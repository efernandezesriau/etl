#-------------------------------------------------------------------------------
# Name:        IncrementalDistributionImport.py
# Purpose:     Identifies the differences from the time the script was last run
#              and then launches the Data Interoperability tool to import Distribution
#              data into the enterprise geodatabase.
#
#              NOTE: This script requires the installation of cx_Oracle,pypyodbc
#
# Author:      mdonnelly
#
# Created:     13/10/2016
# Copyright:   (c) mdonnelly 2016
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import arcpy, os, traceback, sys, shutil, csv, time, math
import smtplib, base64
import cx_Oracle
from collections import defaultdict
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from util import Util
from SAPUtils import SAPUtils

class DistributionImport:

    MOD_STATUS_PENDING = "PENDING"
    MOD_STATUS_PROCESSED = "PROCESSED"
    CSV_HEADER_GTECH_VIEW_NAME = "GTECH_FEATURE_VIEW"
    CSV_HEADER_ARCGIS_FEATURE_NAME = "ARCGIS_FEATURE"
    CSV_FEATURE_ARCGIS_SUBTYPE_NAME = "ARCGIS_SUBTYPE_VALUE"
    CSV_SAP_DESCRIPTION_CODE = "GIS_DESCRIPTION"
    CSV_SAP_OBJECT_TYPE = "GIS_BUSINESS_OBJECT_TYPE"
    CSV_SAP_CLASS_NAME = "SAP_OBJECT_CLASS"
    CSV_SAP_SUBCLASS_NAME = "SAP_SUBCLASS"

    MOD_TYPE_UPDATE = "UPDATE"
    MOD_TYPE_INSERT = "INSERT"
    MOD_TYPE_DELETE = "DELETE"

    STATE_REMOVED = 'Removed'

    NETWORK_DISTRIBUTION = "DISTRIBUTION"

    SPATIAL_REF_WEB_MERCATOR = 3857

    slurpLimit = 100
    addedSAPLookupRecords=[]

    def __init__(self):
        self.cp = Util.loadConfig()
        self.logpath = self.cp.get('logging', 'logroot')
        self.logFilePrefix = "Incremental%s" %(self.cp.get("logging", "distributionLogFilePrefix"))
        self.loggingLevel = self.cp.getint("logging", "logginglevel")

        self.logFile = Util.startLogging(logpath=self.logpath, logFilePrefix=self.logFilePrefix, loggingLevel=self.loggingLevel)

        self.interopLicenseCode = self.cp.get('program', 'interopLicenseCode')
        self.ETLToolbox = self.cp.get('toolbox', 'ETLToolbox')
        self.maxFeaturesToCopy = self.cp.getint('testing', 'maxFeaturesToCopy')

        self.GTECHOracleConnection = self.cp.get('databases', 'distributionOracleConnection')
        self.GTECHUser = self.cp.get('databases', 'GTECHUser')
        self.GTECHPwd = self.cp.get('databases', 'GTECHPwd')
        self.ViewPrefix = self.cp.get('databases', 'GTECHSchema')

        self.interimFGDB = self.cp.get('geodatabases', 'distributioninterimFGDB')
        self.outputEGDB = self.cp.get('geodatabases', 'distributionEGDB')
        self.outputEGDB_web =self.cp.get('geodatabases','distributionEGDB_webmercator')
        self.gdbSchema = self.cp.get('geodatabases', 'distributionSchema')

        self.fgdbFFSFile = os.path.join(self.logpath, self.cp.get('toolbox', 'distributionIncrementalGTECH2FGDB_Failed'))
        self.egdbFFSFile = os.path.join(self.logpath, self.cp.get('toolbox', 'distributionIncrementalFGDB2EGDB_Failed'))

        self.mappingFileName = self.cp.get('toolbox', 'GTechMappingFileName')
        self.featureMappingFileName = self.cp.get('toolbox', 'GTechFeatureMappingFile')
        self.mappingFileSourceFolder = self.cp.get('toolbox', 'mappingFileSourceFolder')
        self.mappingFileFullPath = os.path.join(self.mappingFileSourceFolder, self.mappingFileName)
        self.featureMappingFileFullPath = os.path.join(self.mappingFileSourceFolder, self.featureMappingFileName)
        self.feature_mapping_file = self.cp.get('SAP', 'dist_feature_mapping')


        self.modificationTableName = self.cp.get('program', 'modificationTableName')
        self.modificationNumberField = self.cp.get('program', 'modificationNumberField')
        self.modificationTypeField = self.cp.get('program', 'modificationTableActionField')
        self.modificationStatusField = self.cp.get('program', 'modificationTableStatusField')
        self.modificationSourceField = self.cp.get('program', 'modificationTableSourceViewField')

        self.GTECHLastModificationTable = self.cp.get('program', 'GTECHLastModificationTable')
        self.GTECHLastModificationField = self.cp.get('program', 'GTECHLastModificationField')

        self.GTECHModificationCopyStoreProc = self.cp.get('program', 'GTECHModificationCopyStoreProc')
        self.GTECHUniqueIDField = self.cp.get('program', 'GTECHUniqueIDField')
        self.GTECHStateField = self.cp.get('program', 'GTECHStateField')
        self.OBJECTID = self.cp.get('program', 'OBJECTID')
        # SAP related information
        self.sap_EGDB = self.cp.get('SAP', 'sap_egdb')

        self.sap_asset_master_table = self.cp.get('program', 'SAP_master_table')
        self.sap_equip_prefix = self.cp.get('program', 'SAP_equip_prefix')
        self.sap_floc_prefix = self.cp.get('program', 'SAP_functional_loc_prefix')

        self.field_sap_key = self.cp.get('program', 'SAP_key_field')
        self.field_sap_internal_key = self.cp.get('program', 'SAP_internal_key_field')
        self.field_sap_parent_key = self.cp.get('program', 'SAP_parent_key_field')
        self.field_sap_gis_object_id = self.cp.get('program', 'SAP_gis_object_id_field')
        self.field_sap_gis_geometry_type = self.cp.get('program', 'SAP_geometry_type_field')
        self.field_sap_asset_type = self.cp.get('program', 'SAP_asset_type_field')
        self.field_sap_gis_class_name = self.cp.get('program', 'SAP_gis_class_field')
        self.field_sap_network_type = self.cp.get('program', 'SAP_network_type_field')
        self.field_sap_class_name = self.cp.get('program', 'SAP_class_field')
        self.field_sap_subclass_name = self.cp.get('program', 'SAP_subclass_field')
        self.field_sap_description = self.cp.get('program', 'SAP_description_field')
        self.field_sap_symbol_rotation = self.cp.get('program', 'SAP_symbolrotation_field')
        self.field_sap_gis_description = self.cp.get('program', 'GIS_description_field')
        self.field_sap_gis_state = self.cp.get('program', 'GIS_state_field')
        self.field_sap_gis_subtype = self.cp.get('program', 'GIS_subtype_field')
        self.field_gis_guid = self.cp.get('program', 'GIS_guid_field')


        #assetmaster csv file
        self.max_where_ids = int(self.cp.get('program', 'MAX_WHERE_IDS'))
        self.sap_lookup_csv_path = self.cp.get('SAP', 'csv_file_destination')

        # TODO: remove hardcode
        self.field_sap_src_label_text = "Labeltext"
        self.field_sap_src_state = "State"
        self.field_sap_src_symbol_rotation = "SymbolRotation"

        # SAP integration service related settings, changes per environment
        self.SAP_URL_WM_lookup = self.cp.get('SAP', 'sap_WM_lookup_URL')
        self.SAP_URL_FLOC_internal_key = self.cp.get('SAP', 'sapInternalKeyServiceURL')
        self.SAP_URL_auth_header = self.cp.get('SAP', 'serviceAuthenticationHeader')


        #email related
        self.send_email = self.cp.getboolean('email', 'emailreport')

        if self.send_email :
            self.email_recipients = self.cp.get('email', 'recipients').split(';')
            self.email_sender = self.cp.get('email', 'sender')

            self.email_server = self.cp.get('email', 'server')
            self.email_port = self.cp.get('email', 'port')
            self.email_username = self.cp.get('email', 'name')
            self.email_password = base64.b64decode(self.cp.get('email', 'password'))


        self.gtech_arcgis_fc_map = dict()
        self.has_processing_error = False

    # Checks any requirements of the class
    #
    def checkPrerequisites(self):
        message = ""

        if not os.path.exists(self.ETLToolbox):
            message = message + "\nThe ETLToolbox does not exist. Check the .ini file.\n"

        if not os.path.exists(os.path.dirname(self.interimFGDB)):
            message = message + "\nFolder for InterimFGDB does not exist. Check the .ini file.\n"

        if not arcpy.Exists(self.outputEGDB):
            message = message + "\nThe distributionEGDB (GDA1994) does not exist. Check the .ini file.\n"

        if not arcpy.Exists(self.outputEGDB_web):
            message = message + "\nThe distributionEGDB (Web Mercator) does not exist. Check the .ini file.\n"

        if not arcpy.Exists(os.path.join(self.outputEGDB, self.GTECHLastModificationTable)):
            message = message + "\nThe GTECHLastModificationTable does not exist. Check the .ini file.\n"

        return message

    # Identify the modifications since the last time the tool was run
    #
    def getLastModificationNumber(self):

        fc = os.path.join(self.outputEGDB, self.GTECHLastModificationTable)
        fields = [self.GTECHLastModificationField]
        lastModificationNumber = -1
        has_row = False
        with arcpy.da.SearchCursor(fc, fields) as cursor:
            for row in cursor:
                has_row = True
                lastModificationNumber = row[0]

        if (lastModificationNumber < 0):
            lastModificationNumber = 0
            self.setLastModificationNumber(lastModificationNumber)

        #print lastModificationNumber
        return lastModificationNumber

    # Update the maximum modification number
    #
    def setLastModificationNumber(self, newLastModificationNumber):

        fc = os.path.join(self.outputEGDB, self.GTECHLastModificationTable)
        fields = [self.GTECHLastModificationField]

        rowExists = False
        with arcpy.da.UpdateCursor(fc, fields) as cursor:
            for row in cursor:
                row[0] = newLastModificationNumber
                cursor.updateRow(row)
                rowExists = True
                break
            del cursor

        if not rowExists:
            with arcpy.da.InsertCursor(fc, fields) as  ins_cursor:
                row_values = [newLastModificationNumber]
                ins_cursor.insertRow(row_values)
                del ins_cursor

    # Create an Oracle connection
    #
    def getOracleConnection(self, orcl_service_descriptor, credentials):

        connectionString = credentials + "@" + orcl_service_descriptor
        oracleConnection = cx_Oracle.connect(connectionString)

        return oracleConnection

    # Run the Oracle stored procedure to copy the modifications entries since the last time the tool was run
    # Returns count of records to process
    def copyLatestModified (self, oracleConnection, lastModificationNumber):

        cur = oracleConnection.cursor()
        cur.callproc(self.GTECHModificationCopyStoreProc, [lastModificationNumber])
        # get count of features
        selectStatement = "SELECT Count(" + self.GTECHUniqueIDField + ") FROM " + self.modificationTableName
        count_fids = 0
        cur.execute(selectStatement)
        for result in cur:
            if (not result[0] is None):
                count_fids = int(result[0])
            break
        cur.close()
        return count_fids

    # Get the maximum modification number that currently exists in the MODIFIEDLOG table
    #
    def getMaxModificationNumber(self, oracleConnection):

        cur = oracleConnection.cursor()
        selectStatement = "SELECT MAX(" + self.modificationNumberField + ") FROM " + self.modificationTableName
        maxModificationNumber = -1
        cur.execute(selectStatement)
        for result in cur:
            if (not result[0] is None):
                maxModificationNumber = int(result[0])
            break
        cur.close()

        return maxModificationNumber

    # build G/Tech view to ArcGIS Feature class mapping table
    # GTech view name is unique and is used as key for dict
    def buildViewToFCMapping(self):
        with open(self.featureMappingFileFullPath, 'rb') as csvfilein:
            csvReader = csv.DictReader(csvfilein, delimiter='|')
            for row in csvReader:
                view_name = str(row[self.CSV_HEADER_GTECH_VIEW_NAME]).lower().strip()
                arcgis_fc_name = str(row[self.CSV_HEADER_ARCGIS_FEATURE_NAME]).strip()
                arcgis_subtype = str(row[self.CSV_FEATURE_ARCGIS_SUBTYPE_NAME]).strip()
                sap_description_code = str(row[self.CSV_SAP_DESCRIPTION_CODE]).strip()
                sap_object_type = str(row[self.CSV_SAP_OBJECT_TYPE]).upper().strip()
                sap_object_class = str(row[self.CSV_SAP_CLASS_NAME]).strip()
                sap_object_subclass = str(row[self.CSV_SAP_SUBCLASS_NAME]).strip()
                if (len(arcgis_fc_name) > 0):
                    if (not view_name in self.gtech_arcgis_fc_map.keys()):
                        self.gtech_arcgis_fc_map[view_name] = [arcgis_fc_name, arcgis_subtype, sap_object_type, sap_object_class, sap_object_subclass, sap_description_code]
            del csvReader

    # Gets feature class for view
    #
    def getFcFromView(self, view_name):
        if (view_name.lower() in self.gtech_arcgis_fc_map.keys()):
            return self.gtech_arcgis_fc_map[view_name.lower()]
        else:
            return []



    # Gets unique list of arcgis feature classes from mapping file
    #
    def getFcList(self):

        fcList = []
        fcList_temp = [v for k, v in self.gtech_arcgis_fc_map.iteritems()]
        fcSet = set(fcList_temp)
        return list(fcSet)

    # get a list of features from modification log as dict with arcgis feature class names as keys
    def getModLogFeatures(self, oracleConnection, whereClause):
        try:
            dict_delete_views_gtech = dict()
            dict_delete_views_arcgis = dict()

            select_statement = "Select Distinct %s, %s From %s Where %s Order By %s" % (self.modificationSourceField, self.GTECHUniqueIDField,
                                         self.modificationTableName, whereClause, self.modificationSourceField)
            msg = "Executing %s..." %(select_statement)
            Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)

            ora_cursor = oracleConnection.cursor()
            ora_cursor.execute(select_statement)
            for result_row in ora_cursor:

                g3e_fid = long(result_row[1])
                gtech_view_name = str(result_row[0]).lower()
                if (gtech_view_name in dict_delete_views_gtech.keys()):
                    dict_delete_views_gtech[gtech_view_name].append(g3e_fid)
                else:
                    dict_delete_views_gtech[gtech_view_name] = [g3e_fid]

            # close cursor now
            ora_cursor.close()

            for gtech_view_name in dict_delete_views_gtech.keys():
                list_fc_details = self.getFcFromView(gtech_view_name.lower())
                if (len(list_fc_details) == 0):
                    continue
                arcgis_table = list_fc_details[0]
                arcgis_subtype = list_fc_details[1]

                if (len(arcgis_table) > 0):
                    if (arcgis_table in dict_delete_views_arcgis.keys()):
                        dict_delete_views_arcgis[arcgis_table].extend(dict_delete_views_gtech[gtech_view_name])
                    else:
                        dict_delete_views_arcgis[arcgis_table] = dict_delete_views_gtech[gtech_view_name]


            return dict_delete_views_arcgis

        except Exception as e:
            Util.log("Could not get list of features from %s" %(self.modificationTableName), Util.LOG_LEVEL_ERROR, False, True)
            Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False, True)
            raise


    # get a list of deleted features in mod log as dict with arcgis feature class names as keys
    def getDeletedFeatures(self, oracleConnection):
        where_clause = "%s='%s' And %s='%s'" %(self.modificationTypeField, self.MOD_TYPE_DELETE, self.modificationStatusField, self.MOD_STATUS_PENDING)
        return self.getModLogFeatures(oracleConnection, where_clause)

    # get a list of added or updated features in mod log as dict with arcgis feature class names as keys
    def getAddedUpdatedFeatures(self, oracleConnection, gtechView):
        where_clause = "Upper(%s)=Upper('%s') And (%s='%s' or %s='%s') And %s='%s'" %(self.modificationSourceField, gtechView, self.modificationTypeField, self.MOD_TYPE_DELETE, self.modificationTypeField, self.MOD_TYPE_INSERT, self.modificationStatusField, self.MOD_STATUS_PENDING)
        return self.getModLogFeatures(oracleConnection, where_clause)

    # get unique FID values from ArcGIS table/feature
    def getUniqueFIDs(self, arcgis_table_full):
        with arcpy.da.SearchCursor(arcgis_table_full, [self.GTECHUniqueIDField]) as cursor:
            return sorted({row[0] for row in cursor})

    # add index to ArcGIS table

    def addUniqueIDIndex(self, arcgis_object_path, field_name):
        fields = arcpy.ListFields(dataset=arcgis_object_path, wild_card=field_name)
        if (len(fields) == 1):
            has_index = False
            # field is present, check of this has index
            indexes = arcpy.ListIndexes(dataset=arcgis_object_path)
            for index in indexes:
                if (index.fields[0].name.lower() == field_name.lower()):
                    has_index = True
                    break
            if (has_index == False):
                arcpy.AddIndex_management(in_table=arcgis_object_path,
                                      fields=field_name,
                                      index_name=field_name + "_idx",
                                      unique="NON_UNIQUE",
                                      ascending="ASCENDING")


    # update mod log table FIDs with status
    def updateModLogStatus(self, oracleConnection, list_FIDs, status):

        try:
            num_batches = math.ceil(len(list_FIDs)/float(self.slurpLimit))
            start_pos = 0
            for iter_fids in range(0, int(num_batches)):
                list_batch_fids = list_FIDs[start_pos: start_pos + self.slurpLimit]
                if (len(list_batch_fids) <= 0):
                    break
                start_pos = start_pos + len(list_batch_fids)
                selection_ids_str = ','.join(str(fid) for fid in list_batch_fids)
                fid_clause = "%s in (%s)" %(self.GTECHUniqueIDField, selection_ids_str)
                update_statement = "Update %s Set %s='%s' Where %s in (%s)" %(self.modificationTableName,
                                    self.modificationStatusField, status, self.GTECHUniqueIDField, selection_ids_str)
                msg = "Executing %s..." %(update_statement)
                #Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False )
                cur = oracleConnection.cursor()
                cur.execute(update_statement)
                oracleConnection.commit()
        except Exception as e:
            Util.log("Could not update %s with '%s'" %(self.modificationTableName, status), Util.LOG_LEVEL_ERROR, False, True)
            Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False, True)

    # get sap schema object type, SAP class and SAP subclass
    #sap_object_type, sap_object_class_name, sap_object_subclass = self.get_SAP_properties(arcgis_object_name, curr_subtype)
    def get_SAP_properties(self, src_object_name, subtype):
        ret_sap_object_type = ""
        ret_sap_class_name = ""
        ret_sap_subclass_name = ""
        ret_sap_description_code = ""
        has_csv_header = True
        feature_mapping_file_path = os.path.join(self.mappingFileSourceFolder, self.feature_mapping_file)
        with open(feature_mapping_file_path, mode='r') as file_feature_map:
            csv_reader_feat = csv.reader(file_feature_map, delimiter='|')
            if (has_csv_header):
                next(csv_reader_feat)
            for csv_row in csv_reader_feat:

                if (str(csv_row[0]).strip() == ''):
                    continue
                try:
                    arcgis_object_name = csv_row[8].strip()
                    if (arcgis_object_name is None or arcgis_object_name == ''):
                        continue

                    arcgis_geometry_type = csv_row[4].strip().title()
                    sap_object_class = csv_row[13].strip()
                    sap_object_subclass = csv_row[14].strip()
                    sap_object_type = csv_row[15].strip().upper()
                    sap_gis_object_type = csv_row[16].strip().upper()
                    sap_description_code = csv_row[19].strip()

                    if (csv_row[9].strip() is None or csv_row[9].strip() == ""):
                        arcgis_subtype_filter = -1
                    else:
                        arcgis_subtype_filter = int(csv_row[9].strip())

                    if (arcgis_object_name.upper() == src_object_name.upper() and arcgis_subtype_filter == subtype):
                        ret_sap_object_type = sap_gis_object_type
                        ret_sap_class_name = sap_object_class
                        ret_sap_subclass_name = sap_object_subclass
                        ret_sap_description_code = sap_description_code
                        break
                except Exception as e:
                    Util.log("Could not retrieve SAP object name for '%s'" % (src_object_name),
                             Util.LOG_LEVEL_ERROR, False, True)
                    Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)),
                             Util.LOG_LEVEL_ERROR,
                             False, True)

        return ret_sap_object_type, ret_sap_class_name, ret_sap_subclass_name, ret_sap_description_code

    def has_sap_object(self, src_object_name):
        has_sap_asset = False
        has_csv_header = True
        feature_mapping_file_path = os.path.join(self.mappingFileSourceFolder, self.feature_mapping_file)
        with open(feature_mapping_file_path, mode='r') as file_feature_map:
            csv_reader_feat = csv.reader(file_feature_map, delimiter='|')
            if (has_csv_header):
                next(csv_reader_feat)
            for csv_row in csv_reader_feat:
                if (str(csv_row[0]).strip() == ''):
                    continue
                try:
                    arcgis_object_name = csv_row[8].strip()
                    if (arcgis_object_name is None or arcgis_object_name == ''):
                        continue
                    if (arcgis_object_name.upper() == src_object_name.upper()):
                        has_sap_asset = True
                        break
                except Exception as e:
                        Util.log("Could not retrieve SAP object mapping for '%s'" % (src_object_name),
                                 Util.LOG_LEVEL_ERROR, False, True)
                        Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
                        exc_type, exc_value, exc_traceback = sys.exc_info()
                        Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)),
                                 Util.LOG_LEVEL_ERROR,
                                 False, True)
        return has_sap_asset


    def mapRowsAttributes(self, rowOutput, rowInput, rowFields, srcRowFields):
        count = len(rowFields)
        fieldIndex = -1
        for i in range(count):
            if(rowFields[i] != self.OBJECTID and rowFields[i] != 'Shape'):
                fieldIndex = -1
                fieldIndex = self.indexOf(srcRowFields, rowFields[i])
                if(fieldIndex != -1):
                    rowOutput[i] = rowInput[fieldIndex]
        return True;

    # delete GIS records specified by where_clause
    # typically used to replace records from FGDB
    # Appropriate edit session should be start prior to calling this
    def delete_gis_records(self, in_gdb, arcgis_object_name, arcgis_object_type, where_clause):

        try:

            arcgis_table_output_full = os.path.join(in_gdb, arcgis_object_name)

            with arcpy.da.UpdateCursor(in_table=arcgis_table_output_full, field_names="*", where_clause=where_clause) as cursor_delete:
                for row_delete in cursor_delete:
                    cursor_delete.deleteRow()

        except Exception as e:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False, True)
            #Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
            Util.log(r"Could not delete records in %s/%s" % (in_gdb, arcgis_object_name), Util.LOG_LEVEL_ERROR, False, True)



    # this function will search for GIS records that needs to be updated, and insert the newsly added features to GIS feature classes
    def update_GIS_records(self, in_gdb, out_egdb, arcgis_object_name, list_fids, gis_object_type):

        is_success = True
        edit_started = False
        try:

            Util.log("Processing added/updated records in %s ..." % (arcgis_object_name), Util.LOG_LEVEL_DEBUG, True, True)

            arcgis_table_in_full = os.path.join(in_gdb, arcgis_object_name)
            arcgis_table_output_full = os.path.join(out_egdb, arcgis_object_name)

            edit = arcpy.da.Editor(out_egdb)
            edit.startEditing(False, False)
            # Start an edit operation
            edit.startOperation()
            edit_started = True

            batch_start = 0
            while (batch_start < len(list_fids)):
                batch_fids = list_fids[batch_start:batch_start+self.max_where_ids]
                if (len(batch_fids) == 0):
                    break
                batch_start = batch_start + len(batch_fids)
                fid_values_csv = ','.join(str(fid) for fid in batch_fids)

                Util.log("Add/update FID %s in %s" % (fid_values_csv, arcgis_object_name), Util.LOG_LEVEL_DEBUG, True, False)

                lyr_src = r"IN_MEMORY\LYR_SRC_%s" %(arcgis_object_name)
                if (arcpy.Exists(lyr_src)):
                    arcpy.Delete_management(in_data=lyr_src)

                lyr_dest = r"IN_MEMORY\LYR_DEST_%s" %(arcgis_object_name)
                if (arcpy.Exists(lyr_dest)):
                    arcpy.Delete_management(in_data=lyr_dest)

                fid_clause = "%s in (%s)" % (self.GTECHUniqueIDField, fid_values_csv)
                if (gis_object_type == 'Table'):
                    arcpy.MakeTableView_management(in_table=arcgis_table_in_full, out_view=lyr_src, where_clause=fid_clause)
                    arcpy.MakeTableView_management(in_table=arcgis_table_output_full, out_view=lyr_dest, where_clause=fid_clause)
                    arcpy.SelectLayerByAttribute_management(in_layer_or_view=lyr_dest)
                    arcpy.DeleteRows_management(in_rows=lyr_dest)
                else:
                    arcpy.MakeFeatureLayer_management(in_features=arcgis_table_in_full, out_layer=lyr_src, where_clause=fid_clause)
                    arcpy.MakeFeatureLayer_management(in_features=arcgis_table_output_full, out_layer=lyr_dest, where_clause=fid_clause)
                    arcpy.SelectLayerByAttribute_management(in_layer_or_view=lyr_dest)
                    arcpy.DeleteFeatures_management(in_features=lyr_dest)

                field_mappings = arcpy.FieldMappings()
                field_mappings.addTable(lyr_src)
                arcpy.Append_management(inputs=[lyr_src], target=arcgis_table_output_full,
                            schema_type="NO_TEST", field_mapping=field_mappings)

                if (arcpy.Exists(lyr_src)):
                    arcpy.Delete_management(in_data=lyr_src)
                if (arcpy.Exists(lyr_dest)):
                    arcpy.Delete_management(in_data=lyr_dest)


            Util.log("Processed added/updated records in %s successfully." % (arcgis_object_name), Util.LOG_LEVEL_DEBUG, True, True)

        except Exception as e:
            is_success = False
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False, True)
            #Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
            Util.log("Failed to processing added/updated records in %s." % (arcgis_object_name), Util.LOG_LEVEL_ERROR, False, True)

        finally:
            if (edit_started):
                edit.stopOperation()
                edit.stopEditing(True)
            return is_success

    # delete a decomissioned asset with sap id assigned to different FID
    # Special case as G/Tech is not following decommission and commission asset workflow
    def delete_orphan_sap_ids(self, dict_sap_ids):
        sap_master_table = os.path.join(self.sap_EGDB, self.sap_asset_master_table)
        sap_master_update_fields = ["OID@", self.field_sap_asset_type, self.field_sap_gis_geometry_type ]
        for sap_id in dict_sap_ids:
            unique_fid_clause = "%s='%s' and %s='%s' and (%s<>%d or %s Is NULL)" %(self.field_sap_key, sap_id,
                                                                   self.field_sap_network_type, self.NETWORK_DISTRIBUTION,
                                                                   self.GTECHUniqueIDField, dict_sap_ids[sap_id],
                                                                   self.GTECHUniqueIDField)
            fc_sap_geom = ""
            with arcpy.da.UpdateCursor(sap_master_table, sap_master_update_fields, unique_fid_clause) as delete_master_cursor:
                for row_to_delete in delete_master_cursor:
                    if (not row_to_delete[1] is None and not row_to_delete[2] is None):
                        fc_sap_geom = "%s%s"%(str(row_to_delete[1]).upper(), str(row_to_delete[2]).title())
                        fc_sap_geom = os.path.join(self.sap_EGDB, fc_sap_geom)
                    delete_master_cursor.deleteRow()
            if (fc_sap_geom != "" and arcpy.Exists(fc_sap_geom)):
                with arcpy.da.UpdateCursor(fc_sap_geom, ["OID@"],
                                           unique_fid_clause) as delete_fc_cursor:
                    for row_to_delete in delete_fc_cursor:
                        delete_fc_cursor.deleteRow()


    # add or update into SAP master table and sap feature class
    # this will run very slow as it uses cursors
    def update_SAP_records(self, network_egdb, arcgis_object_name, list_fids, gis_geom_type,
                           sap_object_type, sap_object_class_name, sap_object_subclass):
        try:

            src_object = os.path.join(network_egdb, arcgis_object_name)


            if (arcpy.Exists(src_object) == False or Util.hasField(src_object, self.GTECHUniqueIDField) == False):
                Util.log("Source object '%s' does not exist or does not have '%s' field " %(src_object, self.GTECHUniqueIDField), Util.LOG_LEVEL_ERROR, False, True)
                return

            Util.log("Processing added/updated records in %s%s - class %s  ..." % (sap_object_type, gis_geom_type, sap_object_class_name), Util.LOG_LEVEL_DEBUG, True, True)

            object_type = Util.get_object_type(src_object)

            search_fields = ['OID@', self.GTECHUniqueIDField, self.field_sap_key]
            state_pos = -1
            has_state_field = Util.hasField(src_object, self.GTECHStateField)
            if (has_state_field):
                state_pos = len(search_fields)
                search_fields.append(self.GTECHStateField)

            label_pos = -1
            has_label_field = Util.hasField(src_object, self.field_sap_src_label_text)
            if (has_label_field):
                label_pos = len(search_fields)
                search_fields.append(self.field_sap_src_label_text)

            symbol_rotation_pos = -1
            has_rotation_field = Util.hasField(src_object, self.field_sap_symbol_rotation)
            if (has_rotation_field):
                symbol_rotation_pos = len(search_fields)
                search_fields.append(self.field_sap_symbol_rotation)

            subtype_pos = -1
            has_subtype_field = Util.hasField(src_object, self.field_sap_gis_subtype)
            if (has_subtype_field):
                subtype_pos = len(search_fields)
                search_fields.append(self.field_sap_gis_subtype)

            sap_descrip_position = -1
            has_sap_descrip_field = Util.hasField(src_object, self.field_sap_description)
            if (has_sap_descrip_field):
                sap_descrip_position = len(search_fields)
                search_fields.append(self.field_sap_description)

            src_shape_field_pos = -1
            if (object_type != 'Table'):
                src_shape_field_pos = len(search_fields)
                search_fields.append("SHAPE@")

            sap_master_table = os.path.join(self.sap_EGDB, self.sap_asset_master_table)


            sap_master_update_fields = ['OID@', self.field_sap_key,
                                        self.GTECHUniqueIDField, self.field_sap_gis_object_id,
                                        self.field_sap_gis_class_name, self.field_sap_network_type,
                                        self.field_sap_asset_type, self.field_sap_gis_geometry_type]

            sap_master_insert_fields = [self.field_sap_key, self.field_sap_internal_key,
                                        self.GTECHUniqueIDField, self.field_sap_gis_object_id,
                                        self.field_sap_gis_class_name, self.field_sap_network_type,
                                        self.field_sap_asset_type, self.field_sap_gis_geometry_type]

            sap_feature_update_fields = ['OID@', self.field_sap_key,
                                         self.GTECHUniqueIDField, self.field_sap_gis_object_id,
                                         self.field_sap_gis_class_name, self.field_sap_gis_subtype,
                                         self.field_sap_network_type, self.field_sap_class_name,self.field_sap_subclass_name,
                                         self.field_sap_gis_state, self.field_sap_gis_description, self.field_sap_description,
                                         "SHAPE@"]

            sap_feature_insert_fields = [self.field_sap_key,
                                         self.GTECHUniqueIDField, self.field_sap_gis_object_id,
                                         self.field_sap_gis_class_name, self.field_sap_gis_subtype,
                                         self.field_sap_network_type, self.field_sap_class_name,self.field_sap_subclass_name,
                                         self.field_sap_gis_state, self.field_sap_gis_description, self.field_sap_description,
                                         "SHAPE@"]

            sap_class_rotation_pos_update = -1
            sap_class_rotation_pos_insert = -1
            if (gis_geom_type == "Point"):
                sap_class_rotation_pos_update = len(sap_feature_update_fields)
                sap_feature_update_fields.append(self.field_sap_symbol_rotation)
                sap_class_rotation_pos_insert = len(sap_feature_insert_fields)
                sap_feature_insert_fields.append(self.field_sap_symbol_rotation)

            sap_spatial_ref = arcpy.SpatialReference(self.SPATIAL_REF_WEB_MERCATOR)

            # store all new object IDs in AssetMaster table
            list_new_object_ids = []

            num_batches = math.ceil(len(list_fids) / float(self.slurpLimit))
            count_updated = 0
            count_to_be_updated = len(list_fids)
            start_pos = 0
            for iter_fids in range(0, int(num_batches)):
                list_batch_fids = list_fids[start_pos:start_pos + self.slurpLimit]
                if (len(list_batch_fids) <= 0):
                    break
                start_pos = start_pos + len(list_batch_fids)
                selection_ids_str = ','.join(str(fid) for fid in list_batch_fids)
                fid_clause = "%s in (%s) And %s Is Not Null" % (self.GTECHUniqueIDField, selection_ids_str, self.field_sap_key)
                with arcpy.da.SearchCursor(src_object, search_fields, fid_clause, sap_spatial_ref) as search_cursor:
                    for search_row in search_cursor:
                        curr_oid = int(search_row[0])
                        curr_fid = int(search_row[1])
                        curr_sap_key = str(search_row[2])

                        if (curr_sap_key is None or curr_sap_key == ""):
                            Util.log("Source object '{0}' with G3E_FID #{1} does not have '{2}' field value set. Row skipped.".format(
                              src_object, curr_fid, self.field_sap_key), Util.LOG_LEVEL_WARNING, False, True)
                            continue

                        curr_state = "Operational"
                        curr_label = ""
                        curr_rotation = 0.0
                        curr_subtype = -1
                        curr_sap_descrip = ""

                        if (has_state_field):
                            curr_state = str(search_row[state_pos])
                        if (has_label_field):
                            curr_label = str(search_row[label_pos])
                        if (has_rotation_field):
                            curr_rotation = float(search_row[symbol_rotation_pos])
                        if (has_subtype_field and search_row[subtype_pos] is not None):
                            curr_subtype = int(search_row[subtype_pos])
                        if (has_sap_descrip_field):
                            curr_sap_descrip = str(search_row[sap_descrip_position])

                        #unique_fid_clause = "(%s=%d or %s='%s') AND %s = '%s' "%(self.GTECHUniqueIDField, curr_fid, self.field_sap_key, curr_sap_key, self.field_sap_network_type, self.NETWORK_DISTRIBUTION)
                        unique_fid_clause = "%s=%d  AND %s = '%s' AND %s='%s' "%(self.GTECHUniqueIDField, curr_fid,
                                                                                 self.field_sap_network_type, self.NETWORK_DISTRIBUTION,
                                                                                self.field_sap_gis_class_name, arcgis_object_name)

                        has_master_row = False

                        dict_reassigned_sap_ids = dict()
                        with arcpy.da.UpdateCursor(sap_master_table, sap_master_update_fields, unique_fid_clause) as update_master:
                            for master_row in update_master:
                                has_master_row = True
                                sap_master_oid = int(master_row[0])
                                sap_key = str(master_row[1])
                                g3e_fid = int(master_row[2])
                                if (curr_sap_key != sap_key):
                                    Util.log("Source object '%s' with G3E_FID #%d has different SAP key (%s) than SAP master row SAP ID (%s). SAP ID will be updated."
                                             % (src_object, g3e_fid, curr_sap_key, sap_key),
                                            Util.LOG_LEVEL_INFO, False, True)
                                    # the mapping has changed, master record now points to another SAP ID
                                    # This is because G/Tech has updated SAP ID for the asset
                                    # may be some other asset has been assigned with this SAP ID
                                    master_row[1] = curr_sap_key
                                    dict_reassigned_sap_ids[curr_sap_key] = g3e_fid
                                    list_new_object_ids.append(sap_master_oid)
                                if (g3e_fid != curr_fid):
                                    Util.log("Source object '%s' with sap key '%s' has different G3E_FID than SAP master row. Row skipped."
                                             % (src_object, sap_key),
                                            Util.LOG_LEVEL_WARNING, False, True)
                                    continue


                                master_row[3] = curr_oid
                                #  do not update ArcGIS Object Class Name and Network type
                                #master_row[4] = arcgis_object_name
                                #master_row[5] = self.NETWORK_DISTRIBUTION
                                master_row[6] = sap_object_type
                                master_row[7] = gis_geom_type
                                update_master.updateRow(master_row)

                        if (has_master_row == False):
                            # this is a new record in ArcGIS with valid G3E_FID and SAPKey values. Add to SAP master table
                            with arcpy.da.InsertCursor(sap_master_table, sap_master_insert_fields) as insert_master:
                                sap_internal_key = ""
                                if (sap_object_type == "FLOC"):
                                    sap_internal_key = SAPUtils.getSAPInternalKey(sapKey=curr_sap_key,
                                                                                  serviceURL=self.SAP_URL_FLOC_internal_key,
                                                                                  authenticatioHeaderValue=self.SAP_URL_auth_header)
                                addedObjectID = insert_master.insertRow((curr_sap_key, sap_internal_key,
                                                                         curr_fid, curr_oid,
                                                                         arcgis_object_name, self.NETWORK_DISTRIBUTION,
                                                                         sap_object_type, gis_geom_type))
                                list_new_object_ids.append(addedObjectID)

                        # if we have reasigned any SAP IDs, delete old ones
                        self.delete_orphan_sap_ids(dict_reassigned_sap_ids)

                        # now insert into FLOC or EQUIP table if source is a feature class
                        if (gis_geom_type.title() == "Table" or gis_geom_type == ""):
                            # we dont need table records in SAP feature classes
                            continue

                        sap_fc_class_name = "%s%s" % (sap_object_type, gis_geom_type.title())
                        sap_fc = os.path.join(self.sap_EGDB, sap_fc_class_name)
                        has_class_row = False

                        with arcpy.da.UpdateCursor(sap_fc, sap_feature_update_fields, unique_fid_clause) as update_class:
                            for asset_row in update_class:
                                has_class_row = True
                                sap_key = str(asset_row[1])
                                g3e_fid = int(asset_row[2])
                                if (curr_sap_key != sap_key):
                                    Util.log(
                                        "Source object '%s' with G3E_FID #%d has different SAP key (%s) than SAP geometry row SAP ID (%s). SAP ID will be updated."
                                        % (src_object, g3e_fid, curr_sap_key, sap_key),
                                        Util.LOG_LEVEL_INFO, False, True)
                                    asset_row[1] = curr_sap_key

                                if (g3e_fid != curr_fid):
                                    Util.log(
                                        "Source object '%s' with sap key '%s' has different G3E_FID than SAP master row. Row skipped."
                                        % (src_object, curr_sap_key),
                                        Util.LOG_LEVEL_WARNING, False, True)
                                    continue

                                asset_row[3] = curr_oid
                                asset_row[4] = arcgis_object_name
                                if (curr_subtype >= 0):
                                    asset_row[5] = curr_subtype
                                asset_row[6] = self.NETWORK_DISTRIBUTION
                                asset_row[7] = sap_object_class_name
                                if (len(sap_object_subclass) > 0):
                                    asset_row[8] = sap_object_subclass
                                if (has_state_field):
                                    # update gis state
                                    asset_row[9] = curr_state
                                if (has_label_field):
                                    # update label
                                    asset_row[10] = curr_label
                                if (has_sap_descrip_field):
                                    asset_row[11] = curr_sap_descrip
                                # set shape field
                                if (src_shape_field_pos > -1):
                                    asset_row[12] = search_row[src_shape_field_pos]


                                if (has_rotation_field and sap_class_rotation_pos_update > -1):
                                    asset_row[sap_class_rotation_pos_update] = curr_rotation
                                update_class.updateRow(asset_row)

                        if (has_class_row == False):
                            # this is a new record in ArcGIS with valid G3E_FID and SAPKey values. Add to SAP master table
                            if (curr_subtype < 0):
                                curr_subtype = None

                            with arcpy.da.InsertCursor(sap_fc, sap_feature_insert_fields) as insert_feature:

                                if (has_rotation_field):
                                    insert_feature.insertRow((curr_sap_key,
                                                         curr_fid, curr_oid,
                                                         arcgis_object_name, curr_subtype,
                                                         self.NETWORK_DISTRIBUTION, sap_object_class_name,
                                                         sap_object_subclass, curr_state,
                                                         curr_label, curr_sap_descrip, search_row[src_shape_field_pos], curr_rotation))
                                else:
                                    insert_feature.insertRow((curr_sap_key,
                                                         curr_fid, curr_oid,
                                                         arcgis_object_name, curr_subtype,
                                                         self.NETWORK_DISTRIBUTION, sap_object_class_name,
                                                         sap_object_subclass, curr_state,
                                                         curr_label, curr_sap_descrip, search_row[src_shape_field_pos]))
            if (len(list_new_object_ids) > 0):
                Util.log("Sending %d new objects in %s to SAP..." % (len(list_new_object_ids), arcgis_object_name), Util.LOG_LEVEL_DEBUG, False, True)
                # send new object ids to SAP for Work Manager lookup update
                fields_for_sap = ["OBJECTID", self.field_gis_guid, self.field_sap_key, self.field_sap_asset_type]
                SAPUtils.upload_new_assets_to_SAP(master_oids=list_new_object_ids,
                                                  sap_master_table=sap_master_table,
                                                  fields_to_send=fields_for_sap,
                                                  max_batch_size=self.max_where_ids,
                                                  SAP_service_url=self.SAP_URL_WM_lookup,
                                                  SAP_auth_header=self.SAP_URL_auth_header)


            Util.log("Added/updated records in %s%s - class %s." % (sap_object_type, gis_geom_type, sap_object_class_name), Util.LOG_LEVEL_DEBUG, True, True)

        except Exception as e:
            Util.log("Could not add or update records in %s%s - class %s" % (sap_object_type, gis_geom_type, sap_object_class_name),
                     Util.LOG_LEVEL_ERROR, False, True)
            Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR,
                     False, True)

    def getNewObjectID(self, new_record_where, sap_master_table):
        new_oid=-1
        search_fields = ["OID@"]

        with arcpy.da.SearchCursor(in_table=sap_master_table, field_names=search_fields, where_clause=new_record_where) as cursor:
            for row in cursor:
                new_oid = row[0]
        return new_oid

    # mark assets in SAP schema as removed in sap feature classes
    def delete_SAP_records(self, list_gtech_fids):

        #loop through all objects in SAP schema and delete records by specified G3E_FID value and network type as distribution
        arcpy.env.workspace = self.sap_EGDB
        for ds_name in arcpy.ListDatasets():
            for fc_name in arcpy.ListFeatureClasses(None, None, ds_name):
                self.delete_SAP_fc_records(sap_fc_name=fc_name, list_gtech_fids=list_gtech_fids)

        for fc_name in arcpy.ListFeatureClasses():
            self.delete_SAP_fc_records(sap_fc_name=fc_name, list_gtech_fids=list_gtech_fids)

        for table_name in arcpy.ListTables():
            self.delete_SAP_table_records(sap_table_name=table_name, list_gtech_fids=list_gtech_fids)

    # mark assets in SAP schema as removed
    def delete_SAP_fc_records(self, sap_fc_name, list_gtech_fids):
        sap_fc = os.path.join(self.sap_EGDB, sap_fc_name)
        try:
            select_mode = "NEW_SELECTION"

            if (arcpy.Exists(sap_fc) and
                    Util.hasField(sap_fc, self.GTECHUniqueIDField) and
                    Util.hasField(sap_fc, self.field_sap_gis_state)):

                selection_ids_str = ','.join(str(fid) for fid in list_gtech_fids)
                fid_clause = "%s in (%s) And %s='%s'" % (self.GTECHUniqueIDField, selection_ids_str, self.field_sap_network_type, self.NETWORK_DISTRIBUTION)

                layer_arcgis_fc = r'IN_MEMORY\%s' % (sap_fc_name)
                if (arcpy.Exists(layer_arcgis_fc)):
                    arcpy.Delete_management(layer_arcgis_fc)

                arcpy.MakeTableView_management(in_table=sap_fc, out_view=layer_arcgis_fc)
                arcpy.SelectLayerByAttribute_management(in_layer_or_view=layer_arcgis_fc,
                                                        selection_type=select_mode,
                                                        where_clause=fid_clause)
                arcpy.CalculateField_management(in_table=layer_arcgis_fc, field=self.field_sap_gis_state,
                                                expression="'%s'" % (self.STATE_REMOVED),
                                                expression_type="PYTHON_9.3")

                if (arcpy.Exists(layer_arcgis_fc)):
                    arcpy.Delete_management(layer_arcgis_fc)

        except Exception as e:
            Util.log("Could not mark records as removed in %s using %s" % (sap_fc, self.GTECHUniqueIDField),
                     Util.LOG_LEVEL_ERROR, False, True)
            Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR,
                     False,
                     True)

    # mark assets in SAP schema as removed in sap tables
    def delete_SAP_table_records(self, sap_table_name, list_gtech_fids):
        sap_table = os.path.join(self.sap_EGDB, sap_table_name)
        try:
            select_mode = "NEW_SELECTION"

            if (arcpy.Exists(sap_table) and
                    Util.hasField(sap_table, self.GTECHUniqueIDField) and
                    Util.hasField(sap_table, self.field_sap_gis_state)):

                selection_ids_str = ','.join(str(fid) for fid in list_gtech_fids)
                fid_clause = "%s in (%s) And %s='%s'" % (self.GTECHUniqueIDField, selection_ids_str,
                                                         self.field_sap_network_type, self.NETWORK_DISTRIBUTION)

                layer_sap_table = r'IN_MEMORY\%s' % (sap_table_name)
                if (arcpy.Exists(layer_sap_table)):
                    arcpy.Delete_management(layer_sap_table)

                arcpy.MakeTableView_management(in_table=sap_table, out_view=layer_sap_table)
                arcpy.SelectLayerByAttribute_management(in_layer_or_view=layer_sap_table, selection_type=select_mode,
                                                        where_clause=fid_clause)
                arcpy.CalculateField_management(in_table=layer_sap_table, field=self.field_sap_gis_state,
                                                expression="'%s'" % (self.STATE_REMOVED), expression_type="PYTHON_9.3")


                if (arcpy.Exists(layer_sap_table)):
                    arcpy.Delete_management(layer_sap_table)

        except Exception as e:
            Util.log("Could not mark records as removed in %s using %s" % (sap_table, self.GTECHUniqueIDField),
                     Util.LOG_LEVEL_ERROR, False, True)
            Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False,
                     True)





    # delete features from arcgis feature/table using unique id field from G/Tech
    def deleteArcGISFeatures(self, dest_egdb,arcgis_table_name, list_delete_FIDs, is_FC):
        try:
            arcgis_table_path = os.path.join(self.outputEGDB, arcgis_table_name)
            msg = "Deleting records from %s..." %(arcgis_table_path)
            Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True )
            if (arcpy.Exists(arcgis_table_path) == False):
                msg = "ArcGIS feature/table '%s' does not exist. Cannot delete records using %s" %(arcgis_table_path, self.GTECHUniqueIDField)
                Util.log(msg, Util.LOG_LEVEL_ERROR, False, True)
                return

            if (Util.hasField(arcgis_object_path=arcgis_table_path, field_name=self.GTECHUniqueIDField) == False):
                msg = "ArcGIS feature/table '%s' does not have '%s' field. Cannot mark records as removed." %(arcgis_table_path, self.GTECHUniqueIDField)
                Util.log(msg, Util.LOG_LEVEL_ERROR, False, True)
                return

            if (Util.hasField(arcgis_object_path=arcgis_table_path, field_name=self.GTECHStateField) == False):
                msg = "ArcGIS feature/table '%s' does not have '%s' field. Cannot mark records as removed." %(arcgis_table_path, self.GTECHStateField)
                if (is_FC):
                    Util.log(msg, Util.LOG_LEVEL_WARNING, False, True)
                else:
                    Util.log(msg, Util.LOG_LEVEL_INFO, False, True)
                return

            # make sure field used for selection has an index
            self.addUniqueIDIndex(arcgis_object_path=arcgis_table_path, field_name=self.GTECHUniqueIDField)

            layer_arcgis_table = r'IN_MEMORY\%s' %('layer_for_delete')
            if (arcpy.Exists(layer_arcgis_table)):
                arcpy.Delete_management(layer_arcgis_table)
            if (is_FC):
                arcpy.MakeFeatureLayer_management(in_features = arcgis_table_path, out_layer=layer_arcgis_table)
            else:
                arcpy.MakeTableView_management(in_table = arcgis_table_path, out_view=layer_arcgis_table)

            first_batch = True
            select_mode = "NEW_SELECTION"

            num_batches = math.ceil(len(list_delete_FIDs)/float(self.slurpLimit))
            count_deleted = 0
            count_to_be_deleted = len(list_delete_FIDs)
            start_pos = 0
            for iter_fids in range(0, int(num_batches)):
                list_batch_fids = list_delete_FIDs[start_pos:start_pos+self.slurpLimit]
                if (len(list_batch_fids) <= 0):
                    break
                start_pos = start_pos + len(list_batch_fids)
                selection_ids_str = ','.join(str(fid) for fid in list_batch_fids)
                fid_clause = "%s in (%s)" %(self.GTECHUniqueIDField, selection_ids_str)
                arcpy.SelectLayerByAttribute_management(in_layer_or_view=layer_arcgis_table, selection_type=select_mode, where_clause=fid_clause)
                count_selected = int(arcpy.GetCount_management(layer_arcgis_table).getOutput(0))
                if count_selected > 0:
                    if (is_FC):
                        # we do not delete records in SDE, we simply flag the state as deleted.
                        arcpy.CalculateField_management(in_table=layer_arcgis_table, field=self.GTECHStateField,
                                                        expression="'%s'" %(self.STATE_REMOVED), expression_type="PYTHON_9.3")

                    if (delete_SAP):
                        # now delete SAP records
                       self.delete_SAP_records(list_batch_fids)

                    count_deleted = count_deleted + count_selected
                    # now delete SAP records
                    self.delete_SAP_records(list_batch_fids)

                    count_deleted = count_deleted + count_selected

            if (count_deleted != count_to_be_deleted):
                msg = "%d records out of %d deleted from %s." %(count_selected, count_to_be_deleted, arcgis_table_path)
            else:
                msg = "%d records deleted from %s." %(count_to_be_deleted, arcgis_table_path)
            Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)

            if (arcpy.Exists(layer_arcgis_table)):
                arcpy.Delete_management(layer_arcgis_table)
        except Exception as e:
            Util.log("Could not delete records in %s using %s" %(arcgis_table_path, self.GTECHUniqueIDField), Util.LOG_LEVEL_ERROR, False, True)
            Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False, True)


    # Import features using spatial ETL tool
    #
    def importFeatures(self):

        try:

            Util.log("Starting IncrementalDistributionETLTool FME tool...", Util.LOG_LEVEL_INFO, False, True)
            arcpy.DistributionIncrementalGTECH2FGDB(SourceDataset_ORACLE_SPATIAL_1=self.GTECHOracleConnection,
                                                    DestDataset_GEODATABASE_FILE=self.interimFGDB,
                                                    OracleUser=self.GTECHUser,
                                                    OraclePwd=self.GTECHPwd,
                                                    MAPPING_TABLE_FILE=self.mappingFileFullPath,
                                                    MAX_FEATURES_PER_FEATURE_TYPE=self.maxFeaturesToCopy,
                                                    Oracle_Schema_Owner=self.ViewPrefix,
                                                    FailedFeaturesDumpFile=self.fgdbFFSFile)
            toolMsg = arcpy.GetMessages(0)
            result_summary, time_taken = Util.get_gp_tool_summary(toolMsg, 'Total Features Written')
            Util.log("Finished IncrementalDistributionETLTool FME tool in %s. Total %s features were written" %(time_taken, result_summary), Util.LOG_LEVEL_INFO, False, True)

            featureWritten = Util.get_gp_tool_feature_output(toolMsg, 'Features Written Summary', 'Total Features Written')
            for featureWritten in featureWritten.splitlines():
                Util.log(featureWritten, Util.LOG_LEVEL_INFO, False, True)


            return True

        except Exception as e:
            Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False, True)
            self.has_processing_error = True
            return False

    #send email report
    def send_email_report(self):
        """
        Compose a report to be sent by e-mail
        :return:
        """

        try:

            if (self.has_processing_error):
                status_completion = "completed with errors"
            else:
                status_completion = "completed successfully"

            server_name = Util.getMachineName().upper()

            msg_summary = "Distribution Data Incremental ETL Process %s on %s at %s" %(status_completion, server_name, datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

            with open(self.logFile) as f:
                detail_report_lines = f.readlines()
                report_body = '\n'.join(detail_report_lines)
                self.email(subject=msg_summary, body=report_body)
                Util.log("E-mail sent", Util.LOG_LEVEL_DEBUG, False, False)

            return True
        except Exception as ex:
            Util.log("Error: " + ex.message, Util.LOG_LEVEL_ERROR, False, True)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False, True)
            return False



    #Send email re completion
    def email(self, subject, body):
        """
        an E-mail method hardcoded to log into and and send e-mails.
        :param subject: the subject line in the e-mail
        :param body: the body of the the e-mail
        :return: is e-mail was successful
        """

        # Load Configuration


        # msg = MIMEText(body)
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = self.email_sender
        msg['To'] = ", ".join(self.email_recipients)

        # create html Message to preserve formatting of the report message
        htmlbody = '<html><body><pre>{}</pre><body/></html>'.format(body)

        # Record the MIME types of both parts - text/plain and text/html.
        part1 = MIMEText(body, 'plain')
        part2 = MIMEText(htmlbody, 'html')

        # Attach parts into message container.
        # According to RFC 2046, the last part of a multipart message, in this case
        # the HTML message, is best and preferred.
        msg.attach(part1)
        msg.attach(part2)
        server = smtplib.SMTP(self.email_server, self.email_port)
        server.ehlo()
        #server.starttls() -- Commented by Srini
        #server.login(username, password) -- Commented by Srini
        server.sendmail(self.email_sender, self.email_recipients, msg.as_string())
        server.quit()

        return True


    #return index of item in list
    def indexOf(self, listItems, item):
        try:
            return listItems.index(item)
        except Exception:
            return -1

    #return IDs that do not exist in given FC
    def getAddedFeaturesIDs(self, fc_fullPath, idList):
        fcIDList = self.getUniqueFIDs(fc_fullPath)
        addFeaturesList = []
        for id in idList:
            if(self.indexOf(fcIDList, id) == -1):
                addFeaturesList.append(id)
        return addFeaturesList;

    #select records by atrributes then copy to FC
    def copySelectedRecords(self, idList, fc_toGDB, fc_fromGDB):
        select_mode = "NEW_SELECTION"
        selection_ids_str = ','.join(str(fid) for fid in idList)
        fid_clause = "%s in (%s)" % (self.GTECHUniqueIDField, selection_ids_str)

        selection_layer = r"IN_MEMORY\lyr"
        if (arcpy.Exists(selection_layer)):
            arcpy.Delete_management(selection_layer)

        schemaType = "TEST"

        arcpy.MakeTableView_management(fc_fromGDB, selection_layer)
        arcpy.SelectLayerByAttribute_management(in_layer_or_view=selection_layer,
                                                        selection_type=select_mode,
                                                        where_clause=fid_clause)
        arcpy.Append_management(inputs=[selection_layer], target=fc_toGDB, schema_type=schemaType)
        arcpy.Delete_management(selection_layer)

    #return select features by atrributes then copy to FC
    def copySelectedFeatures(self, idList, fc_toGDB, fc_fromGDB):
        select_mode = "NEW_SELECTION"
        selection_ids_str = ','.join(str(fid) for fid in idList)
        fid_clause = "%s in (%s)" % (self.GTECHUniqueIDField, selection_ids_str)
        selection_layer = "lyr"
        schemaType = "NO_TEST"

        if (arcpy.Exists(selection_layer)):
            arcpy.Delete_management(selection_layer)

        arcpy.MakeFeatureLayer_management(fc_fromGDB, selection_layer)
        arcpy.SelectLayerByAttribute_management(in_layer_or_view=selection_layer,
                                                        selection_type=select_mode,
                                                        where_clause=fid_clause)
        arcpy.Append_management(inputs=[selection_layer], target=fc_toGDB, schema_type=schemaType)
        arcpy.Delete_management(selection_layer)

    # create a CSV file for specified network
    def create_newmaster_lookup_records_csv(self, addedSAPLookupRecords):
        master_table_full_name = os.path.join(self.sap_EGDB, self.sap_asset_master_table)
        fields_to_export = ["OBJECTID", self.field_gis_guid, self.field_sap_key, self.field_sap_asset_type]
        totalIDsLength = len(addedSAPLookupRecords)
        from_index = 0
        to_index = self.max_where_ids
        current_ids = []
        csv_src_array = []
        try:
            while (abs(totalIDsLength - to_index) < self.max_where_ids):
                current_ids = addedSAPLookupRecords[from_index:to_index]
                selection_ids_str = ','.join(str(fid) for fid in current_ids)
                where_clause = "OBJECTID in (%s)" % (selection_ids_str)
                nparr = arcpy.da.FeatureClassToNumPyArray(in_table=master_table_full_name,
                                                          field_names=fields_to_export,
                                                          where_clause=where_clause,
                                                          skip_nulls=True)
                csv_src_array.extend(nparr)
                from_index = to_index
                to_index += self.max_where_ids

            localtime = time.localtime()
            timeString = time.strftime("%d%m%y_%H%M%S", localtime)
            csv_file_name = "%s_Full_Load_%s.csv" %(self.NETWORK_DISTRIBUTION, timeString)
            dest_file = os.path.join(self.sap_lookup_csv_path, csv_file_name)
            if (os.path.exists(dest_file)):
                os.remove(dest_file)
            header_row = ','.join(fields_to_export)
            numpy.savetxt(dest_file, csv_src_array, delimiter=",", fmt="%s", header=header_row)
            return dest_file
        except Exception as e:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False,
                     True)
            returnMessage = "ERROR: " + e.message + ". TRACEBACK: " + repr(
                traceback.format_exception(exc_type, exc_value, exc_traceback))
            return_code = Util.RETURN_CODE_ERROR


if __name__ == "__main__":

    try:
        startTime = time.time()
        script_name = os.path.basename(__file__)


        di = DistributionImport()
        Util.log("Starting %s ###############################################" %(script_name), Util.LOG_LEVEL_INFO, False, True)

        checkMessage = di.checkPrerequisites()

        if checkMessage != "":
            Util.log("Prerequisite error: " + checkMessage, Util.LOG_LEVEL_ERROR, False, True)
            di.has_processing_error = True
            #sys.exit()
        elif not Util.importFMEToolbox(di.ETLToolbox):
            Util.log("The Data Interoperability license is unavailable. Exiting program.", Util.LOG_LEVEL_ERROR, False, True)
            di.has_processing_error = True
            #sys.exit()
        else:
            oracle_conn_open = False
            # start processing now
            try:
                # build a dict of G/Tech to arcgis feature mapping with GTech table/view as key
                di.buildViewToFCMapping()

                # retrieve stored mod number to process changes greater than last processed.
                lastModificationNumber = di.getLastModificationNumber()
                di.GTECHPwd = Util.decode(di.GTECHPwd)
                GTECHCredentials = di.GTECHUser + "/" + di.GTECHPwd

                # open cx_oracle connection
                oracleConnection = di.getOracleConnection(di.GTECHOracleConnection, GTECHCredentials)
                oracle_conn_open = True

                # build simplified mod log table using oracle stored proc
                msg = "Building simplified mod log table with modification number greater than %f..." %(lastModificationNumber)
                Util.log(msg, Util.LOG_LEVEL_INFO, False, True)
                count_fids_to_process = di.copyLatestModified(oracleConnection, lastModificationNumber)

                if (count_fids_to_process <= 0):
                    msg = "No records to process in %s table." % (di.modificationTableName)
                    Util.log(msg, Util.LOG_LEVEL_INFO, False, True)
                else:
                    #get list of deleted features as they will not be present in the FGDB
                    msg = "Processing deleted features..."
                    Util.log(msg, Util.LOG_LEVEL_INFO, False, True)
                    dict_deleted_features = di.getDeletedFeatures(oracleConnection)
                    for arcgis_object_name in dict_deleted_features.keys():
                        # now delete records from arcgis
                        arcgis_table_output_full = os.path.join(di.outputEGDB, arcgis_object_name)
                        is_fc = Util.isFeatureClassObject(arcgis_table_output_full)
                        # first delete from GDA94 Geodatabase and also delete from SAP schema
                        di.deleteArcGISFeatures(di.outputEGDB, arcgis_object_name, dict_deleted_features[arcgis_object_name], is_fc, True)
                        # Now delete from Web Mercator copy of the GDA94 classes, but there is no SAP schema in Web Mercator
                        # 19-Aug-2019 : Added for CR to enable Web Mercator based schema for GeoCortex
                        di.deleteArcGISFeatures(di.outputEGDB_web, arcgis_object_name, dict_deleted_features[arcgis_object_name], is_fc, False)

                        di.updateModLogStatus(oracleConnection, dict_deleted_features[arcgis_object_name], di.MOD_STATUS_PROCESSED)

                    msg = "Creating interim FGDB..."
                    Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
                    Util.createNewFGDB(di.interimFGDB, di.gdbSchema, True)

                    # no need to truncate, new GDB anyway
                    #Util.truncateTables(self.interimFGDB, fcList)
                    # Run the ETL tool and build the FGDB
                    # only features add or updated are added to FGDB
                    if di.importFeatures():
                        msg = "Transferring data from interim geodatabase to enterprise geodatabase..."
                        Util.log(msg, Util.LOG_LEVEL_INFO, False, True)

                        for gtech_view_name in di.gtech_arcgis_fc_map.keys():
                            try:
                                gtech_mapping = di.gtech_arcgis_fc_map[gtech_view_name]
                                arcgis_object_name = gtech_mapping[0]
                                arcgis_subtype = gtech_mapping[1]
                                sap_object_type = gtech_mapping[2]
                                sap_class_name = gtech_mapping[3]
                                sap_subclass_name = gtech_mapping[4]
                                sap_description_code = gtech_mapping[5]

                                msg = "Processing '%s' with subtype %s..." % (arcgis_object_name, arcgis_subtype)
                                Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)

                                arcgis_table_input_full = os.path.join(di.interimFGDB, arcgis_object_name)
                                arcgis_table_output_full = os.path.join(di.outputEGDB, arcgis_object_name)
                                # test if a feature class or table
                                gis_object_type = Util.get_object_type(arcgis_table_input_full)
                                is_table = gis_object_type == 'Table'
                                if (is_table == False):
                                    msg = "Repairing geometries from '%s'..." % (arcgis_object_name)
                                    Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)
                                    arcpy.RepairGeometry_management(in_features=arcgis_table_input_full, delete_null=False)

                                lyr_source_fgdb_object = r'IN_MEMORY\%s' % (arcgis_object_name)
                                if (arcpy.Exists(lyr_source_fgdb_object)):
                                    arcpy.Delete_management(lyr_source_fgdb_object)
                                if (is_table == False):
                                    if (arcgis_subtype is None or arcgis_subtype == ''):
                                        arcpy.MakeFeatureLayer_management(in_features=arcgis_table_input_full,
                                                                          out_layer=lyr_source_fgdb_object)
                                    else:
                                        arcpy.MakeFeatureLayer_management(in_features=arcgis_table_input_full,
                                                                          out_layer=lyr_source_fgdb_object,
                                                                          where_clause="%s=%s" % (
                                                                          di.field_sap_gis_subtype, arcgis_subtype))
                                else:
                                    if (arcgis_subtype is None or arcgis_subtype == ''):
                                        arcpy.MakeTableView_management(in_table=arcgis_table_input_full,
                                                                       out_view=lyr_source_fgdb_object)
                                    else:
                                        arcpy.MakeTableView_management(in_table=arcgis_table_input_full,
                                                                       out_view=lyr_source_fgdb_object,
                                                                          where_clause="%s=%s" % (
                                                                          di.field_sap_gis_subtype, arcgis_subtype))

                                result_count = arcpy.GetCount_management(lyr_source_fgdb_object)
                                count_fgdb = int(result_count.getOutput(0))
                                if (count_fgdb == 0):
                                    msg = "No records to process in '%s' for subtype %s..." % (arcgis_object_name, arcgis_subtype)
                                    Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)
                                else:
                                    # build the SAPDescription
                                    if (sap_description_code is not None and sap_description_code != ''):
                                        try:
                                            msg = "Setting %s to '%s' for '%s'..." % (di.field_sap_description,
                                                                                      sap_description_code,
                                                                                      arcgis_object_name)
                                            Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
                                            arcpy.CalculateField_management(in_table=lyr_source_fgdb_object,
                                                                            field=di.field_sap_description,
                                                                            expression=sap_description_code,
                                                                            expression_type="PYTHON_9.3")
                                        except Exception as e:
                                            exc_type, exc_value, exc_traceback = sys.exc_info()
                                            msg = "Error setting '%s' for %s : %s" % (
                                                     di.field_sap_description, arcgis_object_name, e.message)
                                            Util.log(msg, Util.LOG_LEVEL_ERROR, False, True)
                                            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)),
                                                     Util.LOG_LEVEL_WARNING, False, True)

                                    msg = "Transferring data for %s from FGDB to EGDB..." %(arcgis_object_name)
                                    Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)

                                    # get list of all FID values from FGDB feature class
                                    list_fids = di.getUniqueFIDs(arcgis_table_full=lyr_source_fgdb_object)
                                    if (len(list_fids) > 0):

                                        Util.log("Adding/updating records in GDA94 schema for the distribution objects", Util.LOG_LEVEL_DEBUG, False, False)
                                        #check if feature is already there in the GDA94 EGDB and update, otherwise insert
                                        di.update_GIS_records(in_gdb=di.interimFGDB,out_egdb=di.outputEGDB,
                                                              arcgis_object_name=arcgis_object_name, list_fids=list_fids,
                                                              gis_object_type= gis_object_type)
                                        # now do the same for the Web Mercator schema
                                        Util.log("Adding/updating records in Web Mercator schema for the distribution objects", Util.LOG_LEVEL_DEBUG, False, False)
                                        di.update_GIS_records(in_gdb=di.interimFGDB,out_egdb=di.outputEGDB_web,
                                                              arcgis_object_name=arcgis_object_name, list_fids=list_fids,
                                                              gis_object_type= gis_object_type)

                                        if (sap_object_type != '' and sap_object_type is not None and is_table == False):
                                            # this is SAP asset
                                            msg = "Adding/updating data in SAP schema for SAP Object Type '%s' and SAP Class '%s'..." % (
                                                sap_object_type, sap_class_name)
                                            Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)
                                            di.update_SAP_records(network_egdb=di.outputEGDB,
                                                                  arcgis_object_name=arcgis_object_name,
                                                                  list_fids= list_fids,
                                                                  gis_geom_type=gis_object_type,
                                                                  sap_object_type=sap_object_type,
                                                                  sap_object_class_name=sap_class_name,
                                                                  sap_object_subclass=sap_subclass_name)

                                        di.updateModLogStatus(oracleConnection, list_fids, di.MOD_STATUS_PROCESSED)

                                if (arcpy.Exists(lyr_source_fgdb_object)):
                                    arcpy.Delete_management(lyr_source_fgdb_object)

                            except Exception as e:
                                Util.log("Error loading %s: %s" % (arcgis_object_name, e.message), Util.LOG_LEVEL_ERROR, False, True)
                                exc_type, exc_value, exc_traceback = sys.exc_info()
                                Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False, True)
                                di.has_processing_error = True


                    #Call stored procedure to delete archived records from SAP feature classe
                    load_SAP_objects = di.cp.getboolean("SAP","load_data")
                    if (load_SAP_objects == True):
                        msg_summary = "Deleting archived records from SAP EGDB feature classes"
                        Util.log(msg_summary, Util.LOG_LEVEL_INFO, False, True)
                        SAPUtils.delete_archived_SAP_records(config_parser=di.cp)

                arcpy.CheckInExtension(di.interopLicenseCode)

                # now set the high water mark (modification number) in ArcGIS table for next run
                maxModificationNumber = di.getMaxModificationNumber(oracleConnection)
                if (maxModificationNumber > lastModificationNumber):
                    di.setLastModificationNumber(maxModificationNumber)
                oracleConnection.commit()
                oracleConnection.close()
                oracle_conn_open = False
                msg = "Incremental ETL process completed."
                Util.log(msg, Util.LOG_LEVEL_INFO, False, True)

            except Exception as ex:
                message = ex.message
                Util.log("Error: " + message, Util.LOG_LEVEL_ERROR, False, True)
                exc_type, exc_value, exc_traceback = sys.exc_info()
                Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False, True)
                di.has_processing_error = True
                if (oracle_conn_open):
                    oracleConnection.close()
                    oracle_conn_open = False
        if (di.send_email):
            di.send_email_report()

    except Exception as e:
        message = str(e.message)
        Util.log("Error: " + message, Util.LOG_LEVEL_ERROR, False, True)
        exc_type, exc_value, exc_traceback = sys.exc_info()
        Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False, True)
        returnMessage = "ERROR: " + message + ". TRACEBACK: " + repr(traceback.format_exception(exc_type, exc_value, exc_traceback))

    finally:
        endTime = time.time()
        elapsedTime = Util.getElapsedTime(startTime, endTime)
        Util.log("Elapsed time: " + elapsedTime, Util.LOG_LEVEL_INFO, False, True)
        Util.log("Exiting %s" %(script_name), Util.LOG_LEVEL_INFO, False, True)

    exit(0)