#-------------------------------------------------------------------------------
# Name:        SetModNumber.py
# Purpose:     Helper script for test environment set-up
#
# Author:      vapte
#
# Created:     05/12/2016
# Copyright:   (c) vapte 2016
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import os
import arcpy

def main():

    try:
        mod_number = 0
        if (len(sys.argv) > 2):
            mod_number = float(sys.argv[2])

        src_gdb = str(sys.argv[1])

        src_feature_class = os.path.join(src_gdb, "GTECH_LAST_MODIFIED")
        print "Setting modification number in %s to %f..." %(src_feature_class, mod_number)
        #arcpy.CalculateField_management(in_table="B:/Datasets/TasNetworks/DataConnections/SQL on LEAMEL1M1MTZ1 db TASNET_DIST OS log-in.sde/TASNET_DIST.DBO.GTECH_LAST_MODIFIED", field="LAST_MODIFIED_NUMBER", expression="135200000", expression_type="VB", code_block="")

        arcpy.CalculateField_management(in_table=src_feature_class, field="LAST_MODIFIED_NUMBER", expression=str(mod_number), expression_type="VB", code_block="")
        print 'Set LAST_MODIFIED_NUMBER successfully'
    except Exception as e:
        print 'Failed to set LAST_MODIFIED_NUMBER-' + e.message

if __name__ == '__main__':
    main()
