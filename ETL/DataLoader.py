#-------------------------------------------------------------------------------
# Name: DataLoader.py
# Purpose: Load data from source database to EGDB via staging FGDB
#              Calls appropriate data interop tools
#
# Author: vapte
#
# Created: 25/11/2016
# Copyright: (c) vapte 2016
# Licence: <your licence>
#-------------------------------------------------------------------------------
import arcpy
import os
import traceback
import sys
import shutil
import csv
import time
import smtplib
import base64
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from util import Util
import cx_Oracle

class DataLoader:

    # Static: Name of the column in mapping CSV that denotes arcgis feature
    # class name
    COLUMN_CSV_ARCGIS_FEATURE = "ARCGIS_FEATURE"
    FIELD_ARCGIS_GTECH_ID = "G3E_FID"

    def __init__(self, network_type, config_parser):
        # network_type is TRANSMISSION or DISTRIBUTION or COMMS
        self.network_type = network_type
        self.cp = config_parser
        self.logpath = self.cp.get('logging', 'logroot')

        self.interopLicenseCode = self.cp.get('program', 'interopLicenseCode')
        self.ETLToolbox = self.cp.get('toolbox', 'ETLToolbox')
        self.maxFeaturesToCopy = self.cp.getint('testing', 'maxFeaturesToCopy')

        # asset master lookup fields
        self.field_sap_key = self.cp.get('program', 'SAP_key_field')
        self.field_sap_gis_object_id = self.cp.get('program', 'SAP_gis_object_id_field')

        # set-up network type specific class variables from config file
        if (network_type == 'TRANSMISSION'):

            self.fgdbFFSFile = os.path.join(self.logpath, self.cp.get('toolbox', 'transmissionBulkGeomedia2FGDB_Failed'))
            self.egdbFFSFile = os.path.join(self.logpath, self.cp.get('toolbox', 'transmissionBulkFGDB2EGDB_Failed'))

            self.mappingFileName = self.cp.get('toolbox', 'TransmissionMappingFileName')
            self.interimFGDB = self.cp.get('geodatabases', 'transmissioninterimFGDB')
            self.gdbSchema = self.cp.get('geodatabases', 'transmissionSchema')

            self.outputEGDB = self.cp.get('geodatabases', 'transmissionEGDB')
            self.outputEGDBWeb = self.cp.get('geodatabases', 'transmissionEGDB_webmercator')
            self.SqlServerServer = self.cp.get('databases', 'transmissionSqlServer')
            self.SqlServerUser = self.cp.get('databases', 'transmissionSqlServerUser')
            self.SqlServerPwd = self.cp.get('databases', 'transmissionSqlServerPwd')
            self.SqlServerDatabase = self.cp.get('databases', 'transmissionSqlServerDatabase')

        elif (network_type == 'DISTRIBUTION'):
            self.GTECHOracleConnection = self.cp.get('databases', 'distributionOracleConnection')
            self.GTECHUser = self.cp.get('databases', 'GTECHUser')
            self.GTECHPwd = self.cp.get('databases', 'GTECHPwd')
            self.ViewPrefix = self.cp.get('databases', 'GTECHSchema')

            # For loading OMS features
            # Esri does not have OMS tables but TasNet has
            # When testing in Esri environment, set OMSSchema to incorrect
            # value in CONFIG.INI
            # for tool to exclude the OMS reader and continue with other reader
            # This is to circumvent the limitation of Interop Tool stopping the
            # processing as soon as it encounters invalid table/view
            self.OMSViewPrefix = self.cp.get('databases', 'OMSSchema')

            self.interimFGDB = self.cp.get('geodatabases', 'distributioninterimFGDB')
            self.outputEGDB = self.cp.get('geodatabases', 'distributionEGDB')
            self.outputEGDBWeb = self.cp.get('geodatabases', 'distributionEGDB_webmercator')

            self.gdbSchema = self.cp.get('geodatabases', 'distributionSchema')

            self.fgdbFFSFile = os.path.join(self.logpath, self.cp.get('toolbox', 'distributionBulkGTECH2FGDB_Failed'))
            self.egdbFFSFile = os.path.join(self.logpath, self.cp.get('toolbox', 'distributionBulkFGDB2EGDB_Failed'))
            self.mappingFileName = self.cp.get('toolbox', 'GTechMappingFileName')

            # for getting/setting last mod number
            self.GTECHLastModificationTable = self.cp.get('program', 'GTECHLastModificationTable')
            self.GTECHLastModificationField = self.cp.get('program', 'GTECHLastModificationField')

        elif (network_type == "COMMS"):
            self.GTECHOracleConnection = self.cp.get('databases', 'commsOracleConnection')
            self.GTECHUser = self.cp.get('databases', 'GCOMMUser')
            self.GTECHPwd = self.cp.get('databases', 'GCOMMPwd')
            self.ViewPrefix = self.cp.get('databases', 'GCOMMSchema')

            self.interimFGDB = self.cp.get('geodatabases', 'commsinterimFGDB')
            self.outputEGDB = self.cp.get('geodatabases', 'commsEGDB')
            self.outputEGDBWeb = self.cp.get('geodatabases', 'commsEGDB_webmercator')
            self.gdbSchema = self.cp.get('geodatabases', 'commsSchema')

            self.fgdbFFSFile = os.path.join(self.logpath, self.cp.get('toolbox', 'commsBulkGTECH2FGDB_Failed'))
            self.egdbFFSFile = os.path.join(self.logpath, self.cp.get('toolbox', 'commsBulkFGDB2EGDB_Failed'))
            self.mappingFileName = self.cp.get('toolbox', 'GCommMappingFileName')

        #set mapping file
        self.mappingFileSourceFolder = self.cp.get('toolbox', 'mappingFileSourceFolder')
        self.mappingFileFullPath = os.path.join(self.mappingFileSourceFolder, self.mappingFileName)

        #email related
        self.send_email = self.cp.getboolean('email', 'emailreport')

        if self.send_email :
            self.email_recipients = self.cp.get('email', 'recipients').split(';')
            self.email_sender = self.cp.get('email', 'sender')

            self.email_server = self.cp.get('email', 'server')
            self.email_port = self.cp.get('email', 'port')
            self.email_username = self.cp.get('email', 'name')
            self.email_password = base64.b64decode(self.cp.get('email', 'password'))

    #remove index on specific field
    def removeAttributeIndex(self, in_table, field_name='G3E_FID'):
        indexes = arcpy.ListIndexes(in_table)
        index_names = []

        for index in indexes:
            index_field = list(field.name.lower() for field in index.fields)[0]
            if (index_field == field_name.lower()):
                arcpy.RemoveIndex_management(in_table=in_table, index_name=index.name)


    # add index on G3E_FID field
    def addAttributeIndex(self, in_table, field_name='G3E_FID'):
        fields = arcpy.ListFields(dataset=in_table)
        for field in fields:
            if (field.name.lower() == field_name.lower()):
                arcpy.AddIndex_management(in_table=in_table,
                                      fields=field_name,
                                      index_name=field_name + "_idx",
                                      unique="NON_UNIQUE",
                                      ascending="ASCENDING")
                break


    #Check if all necessary input is provided
    def checkCommonPrerequisites(self):
        message = ""

        if not os.path.exists(self.ETLToolbox):
            message = message + "\nThe ETLToolbox does not exist. Check the .ini file.\n"

        if not os.path.exists(self.gdbSchema):
            message = message + "\nSchema file '%s' does not exist. Check the .ini file.\n" % (self.gdbSchema)

        if not os.path.exists(os.path.dirname(self.interimFGDB)):
            message = message + "\nFolder for InterimFGDB does not exist. Check the .ini file.\n"

        if not arcpy.Exists(self.outputEGDB):
            message = message + "\Enterprise Geodatabase connection file '%s' does not exist. Check the .ini file.\n" % (self.outputEGDB)

        if not arcpy.Exists(self.outputEGDBWeb):
             message = message + "\Enterprise Geodatabase connection file '%s' does not exist. Check the .ini file.\n" % (self.outputEGDBWeb)

        if not os.path.exists(self.mappingFileFullPath):
            message = message + "\n" + self.mappingFileFullPath + " mapping file does not exist.\n"

        return message


    def send_email_report(self, summary_report, detail_report_lines):
        """
        Compose a report to be sent by e-mail
        :param summary_report: the Process summary report
        :param detail_report: the Tile Report
        :return:
        """

        try:
            report_body = '\n'.join(detail_report_lines)
            self.email(subject=summary_report, body=report_body)
            Util.log("E-mail sent",
                 Util.LOG_LEVEL_DEBUG, False, False)
            return True
        except Exception as ex:
            Util.log("Error: " + ex.message, Util.LOG_LEVEL_ERROR, False, True)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False, True)
            return False

            # Identify the modifications since the last time the tool was run
            #


    # Get max modification number from GTECH AE schema
    def get_gtech_max_mod_number(self):
        orcl_password_decrypt = Util.decode(self.GTECHPwd)
        GTECHCredentials = self.GTECHUser + "/" + orcl_password_decrypt

        connectionString = GTECHCredentials + "@" + self.GTECHOracleConnection
        oracleConnection = cx_Oracle.connect(connectionString)

        cur = oracleConnection.cursor()
        selectStatement = "SELECT MAX(MODIFICATIONNUMBER) FROM AE.MODIFICATIONLOG Where LTT_ID=0 Or LTT_ID Is Null"
        maxModificationNumber = -1
        cur.execute(selectStatement)
        for result in cur:
            if (not result[0] is None):
                maxModificationNumber = int(result[0])
                break
        cur.close()
        oracleConnection.commit()
        oracleConnection.close()

        return maxModificationNumber

    # Set last Gtech modification number into ArcGIS
    def set_last_mod_number(self, last_mod_number):
        fc = os.path.join(self.outputEGDB, self.GTECHLastModificationTable)
        fields = [self.GTECHLastModificationField]
        rowExists = False
        with arcpy.da.UpdateCursor(fc, fields) as cursor:
            for row in cursor:
                row[0] = last_mod_number
                cursor.updateRow(row)
                rowExists = True
                break
            del cursor

        if not rowExists:
            with arcpy.da.InsertCursor(fc, fields) as  ins_cursor:
                row_values = [last_mod_number]
                ins_cursor.insertRow(row_values)
                del ins_cursor


    #Send email re completion
    def email(self, subject, body):
        """
        an E-mail method hardcoded to log into and and send e-mails.
        :param subject: the subject line in the e-mail
        :param body: the body of the the e-mail
        :return: is e-mail was successful
        """

        # Load Configuration


        # msg = MIMEText(body)
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = self.email_sender
        msg['To'] = ", ".join(self.email_recipients)

        # create html Message to preserve formatting of the report message
        htmlbody = '<html><body><pre>{}</pre><body/></html>'.format(body)

        # Record the MIME types of both parts - text/plain and text/html.
        part1 = MIMEText(body, 'plain')
        part2 = MIMEText(htmlbody, 'html')

        # Attach parts into message container.
        # According to RFC 2046, the last part of a multipart message, in this
        # case
        # the HTML message, is best and preferred.
        msg.attach(part1)
        msg.attach(part2)

        server = smtplib.SMTP(self.email_server, self.email_port)
        server.ehlo()
        #server.starttls() -- Commented by Srini
        #server.login(username, password) -- Commented by Srini
        server.sendmail(self.email_sender, self.email_recipients, msg.as_string())
        server.quit()

        return True

    # convert FME output into readable report and add it to log file
    def logETLToolOutput(self, tool_name, tool_result):
        result_summary, time_taken = Util.get_gp_tool_summary(tool_result, 'Total Features Written')
        Util.log("Finished %s FME tool in %s. Total %s features were written. Detail report below." % (tool_name, time_taken, result_summary), Util.LOG_LEVEL_INFO, False, True)

        featureWritten = Util.get_gp_tool_feature_output(tool_result, 'Features Written Summary', 'Total Features Written')
        for featureWritten in featureWritten.splitlines():
            Util.log(featureWritten, Util.LOG_LEVEL_INFO, False, True)



    # Import G/Tech distribution features to FGDB
    #
    def importDistributionFeatures(self):

        try:

            GTECHPwd = Util.decode(self.GTECHPwd)

            max_features = 999999999
            if (self.maxFeaturesToCopy > 0):
                max_features = self.maxFeaturesToCopy

            tool_name = "DistributionBulkGTECH2FGDB"
            Util.log("Starting %s FME tool..." % (tool_name), Util.LOG_LEVEL_INFO, False, True)
            arcpy.DistributionBulkGTECH2FGDB(SourceDataset_ORACLE_SPATIAL=self.GTECHOracleConnection,
                                             DestDataset_GEODATABASE_FILE=self.interimFGDB,
                                             Oracle_Schema_Owner=self.ViewPrefix,
                                             OraclePwd=GTECHPwd,
                                             OracleUser=self.GTECHUser,
                                             MAPPING_TABLE_FILE=self.mappingFileFullPath,
                                             MAX_FEATURES_PER_FEATURE_TYPE=max_features,
                                             FailedFeaturesDumpFile=self.fgdbFFSFile,
                                             Oracle_OMS_Schema_Owner=self.OMSViewPrefix)

            toolMsg = arcpy.GetMessages(0)
            # format and log outout of ETL tool
            self.logETLToolOutput(tool_name, toolMsg)

            return True

        except Exception as e:
            Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False, True)
            return False



    # Import GeoMedia transmission features to FGDB
    #
    def importTransmissionFeatures(self):

        try:

            max_features = 999999999
            if (self.maxFeaturesToCopy > 0):
                max_features = self.maxFeaturesToCopy

            GeomediaPwd = Util.decode(self.SqlServerPwd)

            tool_name = "TransmissionBulkGeomedia2FGDB"
            Util.log("Starting %s FME tool..." % (tool_name), Util.LOG_LEVEL_INFO, False, True)
            src_sap_key_field = "SAPExternalKey"
            arcpy.TransmissionBulkGeomedia2FGDB(DestDataset_GEODATABASE_FILE=self.interimFGDB,
                                                SQL_SERVER=self.SqlServerServer,
                                                SourceDataset_FM0_SQL=self.SqlServerDatabase,
                                                SQLServerUser=self.SqlServerUser,
                                                SQLServerPwd=GeomediaPwd,
                                                MAPPING_TABLE_FILE=self.mappingFileFullPath,
                                                MAX_FEATURES_PER_FEATURE_TYPE=max_features,
                                                FailedFeaturesDumpFile=self.fgdbFFSFile,
                                                SrcSAPKeyField=src_sap_key_field)

            toolMsg = arcpy.GetMessages(0)
            # format and log outout of ETL tool
            self.logETLToolOutput(tool_name, toolMsg)

            return True

        except Exception as e:
            Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False, True)
            return False

    #Import G/Tech Comms features to FGDB
    #
    def importCommsFeatures(self):

        try:

            GTECHPwd = Util.decode(self.GTECHPwd)

            tool_name = "CommsBulkGTECH2FGDB"
            Util.log("Starting %s FME tool..." % (tool_name), Util.LOG_LEVEL_INFO, False, True)

            max_features = 999999999
            if (self.maxFeaturesToCopy > 0):
                max_features = self.maxFeaturesToCopy

            arcpy.CommsBulkGTECH2FGDB(SourceDataset_ORACLE_SPATIAL_1=self.GTECHOracleConnection,
                                      DestDataset_GEODATABASE_FILE=self.interimFGDB,
                                      Oracle_Schema_Owner=self.ViewPrefix,
                                      OraclePwd=GTECHPwd,
                                      OracleUser=self.GTECHUser,
                                      MAPPING_TABLE_FILE=self.mappingFileFullPath,
                                      MAX_FEATURES_PER_FEATURE_TYPE=max_features,
                                      FailedFeaturesDumpFile=self.fgdbFFSFile)

            toolMsg = arcpy.GetMessages(0)
            # format and log outout of ETL tool
            self.logETLToolOutput(tool_name, toolMsg)

            return True

        except Exception as e:
            Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False, True)
            return False

    # the uber function to perform end to end ETL for specific network type
    def loadData(self):

        startTime = time.time()
        Util.log("Starting %s Import ###############################################" % (self.network_type), Util.LOG_LEVEL_INFO, False, True)

        return_code = Util.RETURN_CODE_SUCCESS
        try:
            errorMsg = ""
            if (self.network_type == 'TRANSMISSION'):
                errorMsg = self.checkCommonPrerequisites()
                #TODO: confirm connection to source GeoMedia
            elif (self.network_type == 'DISTRIBUTION'):
                errorMsg = self.checkCommonPrerequisites()
                #TODO: confirm connection to source G/Tech
            elif (self.network_type == "COMMS"):
                errorMsg = self.checkCommonPrerequisites()
                #TODO: confirm connection to source G/Tech


            if errorMsg != "":
                Util.log("Process terminated, prerequisite error: " + errorMsg, Util.LOG_LEVEL_ERROR, False, True)
                return_code = Util.RETURN_CODE_ERROR
                return return_code
            elif not Util.importFMEToolbox(self.ETLToolbox):
                Util.log("The Data Interoperability license is unavailable. Exiting program.", Util.LOG_LEVEL_ERROR, False, True)
                return_code = Util.RETURN_CODE_ERROR
                return return_code
            else:


                # create new FGDB using schema file
                Util.createNewFGDB(fgdb_full_path=self.interimFGDB , gdb_schema=self.gdbSchema, drop_existing = True)

                # get a list of arcgis tables/feature classes from mapping file
                fcList = Util.getDistinctCSVColumnValues(DataLoader.COLUMN_CSV_ARCGIS_FEATURE, self.mappingFileFullPath)

                # remove all indexes except OID
                Util.remove_indexes(workspace=self.interimFGDB, sde_prefix="", namelist=fcList, spatial_only=False)
                # perform source db to FGDB import using DI tool

                success_fgdb_import = True
                if (self.network_type == 'TRANSMISSION'):
                    success_fgdb_import = self.importTransmissionFeatures()
                elif (self.network_type == 'DISTRIBUTION'):
                    success_fgdb_import = self.importDistributionFeatures()
                elif (self.network_type == "COMMS"):
                    success_fgdb_import = self.importCommsFeatures()

                if (success_fgdb_import == False):
                    return_code = Util.RETURN_CODE_ERROR
                else:

                    msg = "Transferring data from interim geodatabase to enterprise geodatabase..."
                    Util.log(msg, Util.LOG_LEVEL_INFO, False, True)

                    # FGDB import successful, copy the data from FGDB to EGDB
                    # for each feature class / table in FGDB
                    for table_name in fcList:
                        try:
                            table_input_full = os.path.join(self.interimFGDB, table_name)
                            table_output_full = os.path.join(self.outputEGDB, table_name)
                            table_output_full_Web = os.path.join(self.outputEGDBWeb, table_name)

                            desc_table = arcpy.Describe(table_input_full)
                            is_FC = desc_table.datasetType == 'FeatureClass'
                            spatial_index_removed = False
                            try:

                                if (is_FC):
                                    #Util.add_fc_indexes(in_feature=table_input_full)
                                    msg = "Repairing geometries from feature class '%s'..." % (table_input_full)
                                    Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)
                                    arcpy.RepairGeometry_management(in_features=table_input_full, delete_null=False)
                                    msg = "Remove spatial index from feature class '%s'..." % (table_output_full)
                                    Util.remove_fc_indexes(in_feature_class = table_output_full, spatial_only=True)
                                    spatial_index_removed = True

                                # remove index on G3E_FID
                                self.removeAttributeIndex(in_table=table_output_full, field_name=DataLoader.FIELD_ARCGIS_GTECH_ID)
                            except Exception as ex:
                                Util.log("Error: " + ex.message, Util.LOG_LEVEL_ERROR, False, True)
                                exc_type, exc_value, exc_traceback = sys.exc_info()
                                Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)),
                                         Util.LOG_LEVEL_ERROR, False, True)
                                return_code = Util.LOG_LEVEL_WARNING

                            # Truncate features from EGDB now
                            msg = "Removing existing data from '%s'" % (self.outputEGDB)
                            Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)
                            Util.truncateTables(workspace=self.outputEGDB, tableList=[table_name])
                            # Append features to EGDB now
                            msg = "Loading new data from '%s' to '%s'" % (table_output_full, self.outputEGDB)
                            Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)
                            Util.appendFeatures(fcList=[table_name], fromGDB=self.interimFGDB, toGDB=self.outputEGDB, test=False)
                            if (is_FC and spatial_index_removed == True):
                                arcpy.AddSpatialIndex_management(in_features=table_output_full)
                            # add index on G3E_FID
                            self.addAttributeIndex(in_table=table_output_full, field_name=DataLoader.FIELD_ARCGIS_GTECH_ID)
                            msg = "Loaded data from '%s' to '%s'." % (table_output_full, self.outputEGDB)
                            Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)

                            # Truncate features from EGDB Web Mercator now
                            msg = "Removing existing data from Web Mercator '%s'" % (self.outputEGDBWeb)
                            Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)
                            Util.truncateTables(workspace=self.outputEGDBWeb, tableList=[table_name])
                            # Append features to EGDB Web Mercator now
                            msg = "Loading new web mercator data from '%s' to '%s'" % (table_output_full_Web, self.outputEGDBWeb)
                            Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)
                            Util.appendFeatures(fcList=[table_name], fromGDB=self.interimFGDB, toGDB=self.outputEGDBWeb, test=False)
                            if (is_FC and spatial_index_removed == True):
                                arcpy.AddSpatialIndex_management(in_features=table_output_full_Web)
                            # add index on G3E_FID in Web Mercator feature classes
                            self.addAttributeIndex(in_table=table_output_full_Web, field_name=DataLoader.FIELD_ARCGIS_GTECH_ID)
                            msg = "Loaded web mercator data from '%s' to '%s'." % (table_output_full_Web, self.outputEGDBWeb)
                            Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)

                        except Exception as e:
                            Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
                            exc_type, exc_value, exc_traceback = sys.exc_info()
                            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False, True)
                            return_code = Util.RETURN_CODE_WARNING



                # all done, check in data interop
                arcpy.CheckInExtension(self.interopLicenseCode)


        except Exception as ex:
            Util.log("Error: " + ex.message, Util.LOG_LEVEL_ERROR, False, True)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False, True)
            return_code = Util.LOG_LEVEL_ERROR

        finally:
            return return_code




def main():
    pass

if __name__ == '__main__':
    main()
