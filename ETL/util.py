#-------------------------------------------------------------------------------
# Name:        util.py
# Purpose:     Utilities functions
#
# Author:      mdonnelly
#
# Created:     17/04/2015
# Copyright:   (c) mdonnelly 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import logging, ConfigParser, traceback, datetime, time, os, sys, arcpy, arcgisscripting
import socket
import urllib, urllib2, urlparse, json
import shutil
import csv
import base64
import pypyodbc

class Util:

    LOG_LEVEL_DEBUG = 3
    LOG_LEVEL_INFO = 2
    LOG_LEVEL_WARNING = 1
    LOG_LEVEL_ERROR = 0

    RETURN_CODE_ERROR = 0
    RETURN_CODE_WARNING = 1
    RETURN_CODE_SUCCESS = 2


    # Logs messages to file. Adds a message to tool results console if required.
    #
    @staticmethod
    def log(message, logLevel, addMessage, printMessage):

        message = message.replace("\n", "->")
        logMessage = "%02d %s" % (0, message)
        date = str(datetime.datetime.now())
        timestamp = str(datetime.datetime.now()) #.replace(":","-")
        if (logLevel == Util.LOG_LEVEL_DEBUG):
            logging.debug(logMessage)
        if (logLevel == Util.LOG_LEVEL_INFO):
            logging.info(logMessage)
        if (logLevel == Util.LOG_LEVEL_WARNING):
            logging.warning(logMessage)
        if (logLevel == Util.LOG_LEVEL_ERROR):
            logging.error(logMessage)

        if addMessage:
            if logLevel == Util.LOG_LEVEL_ERROR:
                arcpy.AddError(message)
            elif logLevel == Util.LOG_LEVEL_WARNING:
                arcpy.AddWarning(message)
            else:
                arcpy.AddMessage(message)

        if printMessage:
            print(message)


    # Starts the logging
    #
    @staticmethod
    def startLogging(logpath, logFilePrefix, loggingLevel):
        localtime   = time.localtime()
        timeString  = time.strftime("%d%m%y_%H%M%S", localtime)
        logfile = os.path.join(logpath, logFilePrefix + timeString + ".log")
        print("Log file is %s" %logfile)
        arcpy.AddMessage("Log file is " + logfile)
        logging.getLogger('').handlers = [] # Clear any existing logging handlers
        if os.path.exists(logfile):         # Create a new one for each day
            logging.basicConfig(level=loggingLevel, format='%(asctime)s %(levelname)s %(message)s', filename=logfile, filemode='a')
        else:
            logging.basicConfig(level=loggingLevel, format='%(asctime)s %(levelname)s %(message)s', filename=logfile, filemode='w')

        return logfile

    # Gets time and returns a formatted string
    #
    @staticmethod
    def getTime():
        localtime   = time.localtime()
        timeString  = time.strftime("%d/%m/%Y %H:%M:%S", localtime)
        return timeString

    @staticmethod
    # Gets the difference in time between start and end and returns formatted date string
    #
    def getElapsedTime(startTime, endTime):
        elapsedTime = endTime - startTime
        elapsedTimeObj = time.gmtime(elapsedTime)
        #Need to divide this number by seconds in a day to get any processes that run longer than that

        return time.strftime("%H:%M:%S", elapsedTimeObj)

    # Strip database namespace component
    #
    @staticmethod
    def stripNamespace(table):
        splitStr = table.split('.')
        if splitStr and len(splitStr) == 3: # Database namespace should be DATABASE.USER.TABLE
            return splitStr[2]
        else:
            return table


    # Caseless compare, returns a boolean
    #
    @staticmethod
    def inList(searchFor, inList):
        return str(searchFor).upper() in map(str.upper, inList)


    # Loads the config file
    #
    @staticmethod
    def loadConfig(config_file_name = "Config.ini"):
        INI_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), config_file_name)
        cp = ConfigParser.SafeConfigParser(os.environ)
        cp.read(INI_file)
        return cp

    # Checks out the Data Interoperability license and imports the toolbox
    # Requires a call to arcpy.CheckInExtension from caller when finished
    # with toolbox.
    #
    @staticmethod
    def importFMEToolbox(toolBoxName, toolBoxFolder=None):
        interopLicenseCode = "DataInteroperability"
        if arcpy.CheckExtension(interopLicenseCode) == "Available":
            result = arcpy.CheckOutExtension(interopLicenseCode)
        else:
            return False

        if toolBoxFolder is None:
            #Import tool box from current folder
            toolBoxFolder = os.path.dirname(os.path.realpath(__file__))

        toolBoxPath = os.path.join(toolBoxFolder, toolBoxName)

        result = arcpy.ImportToolbox(toolBoxPath)
        return True

    # Weak password decrypter
    #
    @staticmethod
    def decode(encodeStr):
        return encodeStr.decode('base64','strict')

    # Weak password encrypter
    #
    @staticmethod
    def encode(encodeStr):
        return encodeStr.encode('base64','strict')

    @staticmethod
    def remove_indexes(workspace, sde_prefix, namelist=[], spatial_only=False):
        """
        removes all indexes from featureclasses in the the workspace from the name list
        :param workspace: workspace to look in
        :param sde_prefix: database and user prefix to add to the feature classes
        :param namelist: list of named featureclasses to work on
        :return:
        """
        try:
            Util.log("Removing Indexes from '%s'" % (workspace),
                     Util.LOG_LEVEL_INFO, False, True)
            for name in namelist:
                if not sde_prefix == "":
                    fc = os.path.join(workspace, '{}.{}'.format(sde_prefix,  name))
                else:
                    fc = os.path.join(workspace, name)

                Util.remove_fc_indexes(in_feature_class=fc, spatial_only=spatial_only)
        except Exception as e:
            Util.log("Error: " + e.message,
                     Util.LOG_LEVEL_ERROR, False, True)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log(''.join(traceback.format_exception(exc_type, exc_value, exc_traceback)),
                     Util.LOG_LEVEL_ERROR, False, True)

    # remove all indexes
    # optionally drop index on SDL_FILENAME
    @staticmethod
    def remove_fc_indexes(in_feature_class, spatial_only=False):
        """
        remove all indexes except for the required objectID index from the named feature class
        :param in_feature_class: Feature class to work on
        :return:
        """
        verbose = False
        if not arcpy.Exists(in_feature_class):
            Util.log("Could not find feature class {}, indexes not removed".format(in_feature_class),
                     Util.LOG_LEVEL_ERROR, False, True)
            return

        has_spatial = False

        desc = arcpy.Describe(in_feature_class)
        isFC = desc.datasetType  == 'FeatureClass'
        fieldNameShape = u''
        if (isFC):
            fieldNameShape = desc.shapeFieldName.lower()


        index_names = []

        indexes = arcpy.ListIndexes(dataset=in_feature_class)

        for index in indexes:
            fieldName = list(field.name.lower() for field in index.fields)[0]

            if (fieldName == fieldNameShape):
                has_spatial = True
            elif (not fieldName in [desc.OIDFieldName.lower(), 'globalid'] ):
                index_names.append(index.name)

        try:
            if has_spatial:
                # remove Spatial indexes
                Util.log("Removing spatial index from {}...".format(in_feature_class),
                         Util.LOG_LEVEL_DEBUG, False, False)
                arcpy.RemoveSpatialIndex_management(in_feature_class)

            #remove attribute indexes
            if (spatial_only == False and len(index_names) > 0):
                Util.log("Removing attribute indexes from {}...".format(in_feature_class),
                         Util.LOG_LEVEL_DEBUG, False, False)
                index_names_str = ';'.join(index_names)
                arcpy.RemoveIndex_management(in_table=in_feature_class, index_name=index_names_str)

        except arcgisscripting.ExecuteError as arcpy_e:
            if ('ERROR 000464' in str(arcpy_e) or
               'Lock request conflicts with an established lock' in str(arcpy_e)):
                Util.log("Could not remove spatial index from {} cannot get Exclusive schema lock".format(in_feature_class),
                         Util.LOG_LEVEL_ERROR, False, True)
                return
            else:
                Util.log("Could not remove spatial index from {}. {}".format(in_feature_class, str(arcpy_e)),
                         Util.LOG_LEVEL_ERROR, False, True)

        except Exception as e:
            Util.log("Error: " + e.message,
                     Util.LOG_LEVEL_ERROR, False, True)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log(''.join(traceback.format_exception(exc_type, exc_value, exc_traceback)),
                     Util.LOG_LEVEL_ERROR, False, True)

            Util.log("Could not remove spatial index from {}.".format(in_feature_class),
                         Util.LOG_LEVEL_ERROR, False, True)

        except Exception as e:
            Util.log("Error: " + e.message,
                     Util.LOG_LEVEL_ERROR, False, True)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log(''.join(traceback.format_exception(exc_type, exc_value, exc_traceback)),
                     Util.LOG_LEVEL_ERROR, False, True)

            Util.log("Could not remove index(es) [{}] from {}.".format(';'.join(index_names), in_feature_class),
                         Util.LOG_LEVEL_ERROR, False, True)
        return

    @staticmethod
    def deleteArcPyObjects(objectList):

        for object in objectList:
            if arcpy.Exists(object):
                arcpy.Delete_management(object)

    @staticmethod
    def add_indexes(workspace, sde_prefix, fc_list=[]):

        for name in fc_list:
            if not sde_prefix == "":
                fc = os.path.join(workspace, '{}.{}'.format(sde_prefix,  name))
            else:
                fc = os.path.join(workspace, name)

            if arcpy.Exists(fc):

                desc = arcpy.Describe(fc)
                isFC = desc.datasetType == 'FeatureClass'
                if not isFC:
                    continue

                Util.add_fc_indexes(fc)

    @staticmethod
    def add_fc_indexes(in_feature):
        """
        create an index for a feature class
        :param in_feature: the feature class to apply to
        :param fields: the fields to index
        :param add_spatial: will a spatial index be also applied
        :return:
        """
        try:
            # add Spatial indexes
            # leave these options as 0,0,0 (default)
            # The tool will calculate an optimal grid size by examining all the input features
            Util.log("Adding spatial index in {}...".format(in_feature),
                     Util.LOG_LEVEL_DEBUG, False, False)
            arcpy.AddSpatialIndex_management(in_feature)

        except arcgisscripting.ExecuteError as arcpy_e:
            if ('ERROR 000464' in str(arcpy_e) or
               'Lock request conflicts with an established lock' in str(arcpy_e)):
                Util.log("Could not add spatial index to {}, cannot get Exclusive schema lock".format(in_feature),
                         Util.LOG_LEVEL_ERROR, False, True)
                return
            else:
                Util.log("Could not add index on {}.".format(in_feature),
                         Util.LOG_LEVEL_ERROR, False, True)

        except Exception as e:
            Util.log("Error: " + e.message,
                     Util.LOG_LEVEL_ERROR, False, True)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log(''.join(traceback.format_exception(exc_type, exc_value, exc_traceback)),
                     Util.LOG_LEVEL_ERROR, False, True)

            Util.log("Could not add index(es) on {}.".format(in_feature),
                         Util.LOG_LEVEL_ERROR, False, True)

        return

    # get all tables and feature classes in a workspace
    @staticmethod
    def getAllObjects(workspace):

        arcpy.env.workspace = workspace
        objectList = []
        dsList = arcpy.ListDatasets()
        for ds in dsList:
            fcDsList = arcpy.ListFeatureClasses(feature_dataset=ds)
            objectList.extend(fcDsList)
        fcRootList = arcpy.ListFeatureClasses()
        objectList.extend(fcRootList)
        tabRootList = arcpy.ListTables()
        objectList.extend(tabRootList)
        return objectList

    @staticmethod
    def getFcList(workspace):

        arcpy.env.workspace = workspace
        fcList = []
        dsList = arcpy.ListDatasets()
        for ds in dsList:
            fcDsList = arcpy.ListFeatureClasses(feature_dataset=ds)
            fcList.extend(fcDsList)

        return fcList

    @staticmethod
    def truncateTables(workspace, tableList):

        for table in tableList:
            tablePath = os.path.join(workspace, table)
            if not arcpy.Exists(tablePath):
                Util.log("Table/Feature Class did not exist: {}.".format(tablePath),
                         Util.LOG_LEVEL_ERROR, False, True)
                continue

            Util.log("Truncating {}...".format(tablePath),
                     Util.LOG_LEVEL_DEBUG, False, True)

            try:
                arcpy.TruncateTable_management(in_table=tablePath)
            except Exception as e:
                Util.log("Error: " + e.message,
                         Util.LOG_LEVEL_ERROR, False, True)
                exc_type, exc_value, exc_traceback = sys.exc_info()
                Util.log(''.join(traceback.format_exception(exc_type, exc_value, exc_traceback)),
                         Util.LOG_LEVEL_ERROR, False, True)

                Util.log("Could not truncate table: {}.".format(tablePath),
                             Util.LOG_LEVEL_ERROR, False, True)

    @staticmethod
    def repairGeometries(workspace, featureClassList):

        for fc in featureClassList:
            featureClassPath = os.path.join(workspace, fc)
            if not arcpy.Exists(featureClassPath):
                Util.log("Table/Feature Class did not exist: {}.".format(featureClassPath),
                         Util.LOG_LEVEL_ERROR, False, True)
                continue

            desc = arcpy.Describe(featureClassPath)
            isFC = desc.datasetType == 'FeatureClass'
            if not isFC:
                continue

            Util.log("Repairing geometry for {}...".format(featureClassPath),
                     Util.LOG_LEVEL_DEBUG, False, True)
            try:
                arcpy.RepairGeometry_management(in_features=featureClassPath, delete_null=False)
            except Exception as e:
                Util.log("Error: " + e.message,
                         Util.LOG_LEVEL_ERROR, False, True)
                exc_type, exc_value, exc_traceback = sys.exc_info()
                Util.log(''.join(traceback.format_exception(exc_type, exc_value, exc_traceback)),
                         Util.LOG_LEVEL_ERROR, False, True)

                Util.log("Could not repair geometry: {}.".format(featureClassPath),
                             Util.LOG_LEVEL_ERROR, False, True)

    @staticmethod
    def isInt(s):
        if s is None:
            return False
        try:
            int(s)
            return True
        except Exception as e:
            return False

    @staticmethod
    def isFloat(s):
        if s is None:
            return False
        try:
            float(s)
            return True
        except Exception as e:
            return False

    @staticmethod
    def appendFeatures(fcList, fromGDB, toGDB, test):

        for fc in fcList:
            fromFcPath = os.path.join(fromGDB, fc)
            toFcPath = os.path.join(toGDB, fc)
            Util.log("Appending features to {}...".format(toFcPath),
                     Util.LOG_LEVEL_DEBUG, False, True)
            if test:
                schemaType = "TEST"
            else:
                schemaType = "NO_TEST"
            try:
                # read number of features before append
                result = arcpy.GetCount_management(in_rows = toFcPath)
                start_count = int(result.getOutput(0))
                arcpy.Append_management(inputs=[fromFcPath], target=toFcPath, schema_type=schemaType)
                # read number of features after append
                result = arcpy.GetCount_management(in_rows = toFcPath)
                end_count = int(result.getOutput(0))
                Util.log("--Added %d features to %s" %(end_count-start_count, fc), Util.LOG_LEVEL_INFO, False, True)

            except Exception as e:
                Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
                exc_type, exc_value, exc_traceback = sys.exc_info()
                Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False, True)

    # create new FGDB using schema file
    @staticmethod
    def createNewFGDB(fgdb_full_path, gdb_schema, drop_existing = True):
        # drop existing FGDB if it already exists
        if (os.path.exists(fgdb_full_path) and drop_existing):
            shutil.rmtree(fgdb_full_path)
        if (os.path.exists(fgdb_full_path) == False):
            path_fgdb = os.path.dirname(fgdb_full_path)
            name_fgdb = os.path.basename(fgdb_full_path)

            arcpy.CreateFileGDB_management(out_folder_path=path_fgdb, out_name=name_fgdb)

            arcpy.ImportXMLWorkspaceDocument_management(target_geodatabase=fgdb_full_path, in_file=gdb_schema)

    # Make dictionary of the field names returned from Oracle query
    @staticmethod
    def getOracleFieldDictionary(cursor):

        fieldDict = {}
        count = 0
        for desc in cursor.description:
            fieldDict[desc[0]] = count
            count = count + 1

        return fieldDict

    # Get distinct values from a specified column in a CSV file
    # assumes first row is column headers
    @staticmethod
    def getDistinctCSVColumnValues(column_name, csv_file):
        distinctValues = []

        with open(csv_file, 'rb') as mappingFileHandle:
            csvReader = csv.DictReader(mappingFileHandle, delimiter='|')

            for row in csvReader:
                if (row[column_name] is None):
                    continue
                currentValue = str(row[column_name]).strip()
                if currentValue == "":
                    continue
                if not currentValue in distinctValues:
                    distinctValues.append(currentValue)

        return distinctValues

    # get string getween two
    @staticmethod
    def mid_by_strings(txtcontent, str1, str2):
        str1l = len(str1)
        if str1l < 1:
            return "-2"
        ridx1 = txtcontent.find(str1)

        if ridx1 >= 0:
            if str2 == '':
                ridx2 = None
            else:
                ridx2 = txtcontent.find(str2, ridx1)
            return txtcontent[ridx1 + str1l:ridx2]
        else:
            return "-1"


    # get Data Interop tool feature write summary, gets total features written
    @staticmethod
    def get_gp_tool_summary(message_string, str_start = 'Total Features Written'):
        #str_start is keyword for total feature written, can change depending upon version of ArcMap

        str_end = '\n'
        idx_start = message_string.find(str_start)
        idx_end = 0

        if idx_start > 0:
            idx_end = message_string.find(str_end, idx_start)

        if idx_end > idx_start > 0:
            result = message_string[idx_start + len(str_start):idx_end].strip()
            if result.isdigit():
                result = int(result)
            else:
                Util.log('Value found in result message not an integer ',
                         Util.LOG_LEVEL_WARNING, False, True)
                Util.log('value found is:[{}]'.format(result),
                         Util.LOG_LEVEL_WARNING, False, True)
                result = 0
        else:
            Util.log('String not found [Total Features Written] in message',
                     Util.LOG_LEVEL_WARNING, False, True)
            Util.log('Message string is:\n{}\n ##### end of message string ####'.format(message_string),
                     Util.LOG_LEVEL_WARNING, False, True)
            result = 0

        # (Elapsed Time: 2 minutes 49 seconds)
        # FME Session Duration: 3 minutes 21.9 seconds.
        time_block = Util.mid_by_strings(message_string, 'FME Session Duration:', '.')

        time_taken = time_block.replace(' minutes', ':').replace('seconds', '')
        return result, time_taken

    # pasrse feature write summary and get Feature Name and Count pairs as string
    @staticmethod
    def get_gp_tool_feature_output(message_string, str_start = 'Features Written Summary', str_end = 'Total Features Written'):
        #str_start and str_end are keywords for features written summary, can change depending upon version of ArcMap
        feature_writes = ""

        idx_start = message_string.find(str_start)
        idx_end = 0

        if idx_start > 0:
            idx_end = message_string.find(str_end, idx_start)

        if idx_end > idx_start > 0:
            write_msg = message_string[idx_start + len(str_start):idx_end].strip()
            for resultLine in write_msg.splitlines():
                resultData = resultLine.split('|')
                if (len(resultData) > 0):
                    featureOutput = filter(bool, str(resultData[len(resultData) - 1]).split())
                    if (len(featureOutput) == 2):
                        if (len(feature_writes) > 0):
                            feature_writes = "%s\n---Added %s features to %s" %(feature_writes, featureOutput[1], featureOutput[0])
                        else:
                            feature_writes = "---Added %s features to %s" %(featureOutput[1], featureOutput[0])

        else:
            Util.log('String %s or %s not found in message'%(str_start, str_end) ,
                     Util.LOG_LEVEL_WARNING, False, True)
        return feature_writes

     # delete relationships from a specified dataset
    @staticmethod
    def deleteDatasetRelationships(dataset):
        if (arcpy.Exists(dataset)):
            try:
                arcpy.env.workspace = dataset
                list_rel_classes = [c.name for c in arcpy.Describe(dataset).children if c.datatype == "RelationshipClass"]
                for rel_class_name in list_rel_classes:
                    rel_full_class_name = os.path.join(dataset, rel_class_name)
                    if (arcpy.Exists(rel_full_class_name)):
                        msg = "Deleting relationship class '%s'..." %(rel_class_name)
                        Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)
                        arcpy.Delete_management(rel_full_class_name)
                        msg = "Deleted '%s' relationship class." %(rel_class_name)
                        Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)


            except:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                msg = repr(traceback.format_exception(exc_type, exc_value, exc_traceback))
                Util.log(msg, Util.LOG_LEVEL_WARNING, False, True)

    # delete all relationships from the workspace include any datasets
    @staticmethod
    def deleteAllRelationships(workspace):
        try:

            parDestWorkspace = workspace

            msg = "Deleting relationships from '%s'..." %(parDestWorkspace)
            Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)


            if (arcpy.Exists(parDestWorkspace) == False):
                msg = "Destination workspace/dataset '%s' is invalid." % (parDestWorkspace)
                Util.log(msg, Util.LOG_LEVEL_WARNING, False, True)
                return
            else:
                desc = arcpy.Describe(parDestWorkspace)
                datasetType = desc.dataType

                if (datasetType == "FeatureDataset"):
                    # feature dataset is selected
                    arcpy.env.workspace = parDestWorkspace
                    Util.deleteDatasetRelationships(parDestWorkspace)

                if (datasetType == "Workspace"):
                    # workspace is selected
                    arcpy.env.workspace =  parDestWorkspace

                    datasets = arcpy.ListDatasets()
                    for dataset in datasets:
                        msg = "Deleting relationships in dataset '%s'..." %(dataset)
                        Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)
                        fullDatasetName = os.path.join(parDestWorkspace, dataset)
                        arcpy.env.workspace = fullDatasetName
                        Util.deleteDatasetRelationships(fullDatasetName)
                        msg = "Deleted all relationships in dataset '%s'." %(dataset)
                        Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)

                    # now delete root level feature classes and tables
                    Util.deleteDatasetRelationships(parDestWorkspace)


            msg = "Deleted relationships from '%s'." %(parDestWorkspace)
            Util.log(msg, Util.LOG_LEVEL_INFO, False, True)
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            msg = repr(traceback.format_exception(exc_type, exc_value, exc_traceback))
            Util.log(msg, Util.LOG_LEVEL_WARNING, False, True)
        finally:
            return


    # Find if the object specified is a feature class or table
    # assume input object exists
    # Returns true if a feature class, false if table
    @staticmethod
    def isFeatureClassObject(arcgis_object_path):
        is_FC = False
        desc_table = arcpy.Describe(arcgis_object_path)
        is_FC = desc_table.datasetType == 'FeatureClass'
        return is_FC

    # Get the geometry type of the feature class
    # Returns shapeType if a feature class, otherwise return dataType value
    @staticmethod
    def get_object_type(arcgis_object_path):
        object_type = ""
        desc_table = arcpy.Describe(arcgis_object_path)
        object_type = desc_table.datasetType
        if (object_type == 'FeatureClass'):
            object_type = desc_table.shapeType

        return object_type

    # Find if the object specified as a specified field
    # Returns true if a field is present, false if not present
    @staticmethod
    def hasField(arcgis_object_path, field_name):
        fields = arcpy.ListFields(dataset=arcgis_object_path, wild_card=field_name)
        return len(fields) == 1

    @staticmethod
    # get the name of the machine
    def getMachineName():
        machine_name = ""
        if socket.gethostname().find('.')>=0:
            machine_name = socket.gethostname()
        else:
            machine_name = socket.gethostbyaddr(socket.gethostname())[0]

        return machine_name

    @staticmethod
    #return index of item in list
    def indexOf(listItems, item):
        try:
            return listItems.index(item)
        except Exception:
            return -1


    @staticmethod
    def getSQLServerConnection(server, database, user, password):
        conn = pypyodbc.connect('Driver={SQL Server};Server=' + server + ';Database=' + database + ';uid=' + user + ';pwd=' + password)
        return conn

# class for managing REST calls
class RESTWorker(object):
    # Worker Class with methods to perform tasks with REST service
    def __init__ (self, input_url):
        self.input_url = input_url
        self.layer_url = None
        self.layer_id = None

    def getMapServiceDetails(self, serviceName, params):
        urlMapService = "%s/%s/MapServer" %(self.input_url, serviceName)
        # space causes httblib errors
        urlMapService.replace(' ', '%20')
        return self.get_response(urlMapService, params)

    def cancelJob(self, jobUrl, params):
        # cancel this job
        # space in jobUrl causes httblib errors
        urlCancel = "%s/cancel" %(jobUrl.replace(' ', '%20'))
        return self.get_response(urlCancel, params)

    def check_input_url(self):
        url_parts = self.check_service(self.input_url)
        self.layer_url = url_parts["layer_url"]
        self.layer_id = url_parts["layer_id"]
        if not self.layer_url:
            self.layer_url = "%s%s" % (url_parts["map_url"], "0")
        return url_parts["map_url"]

    def get_root_name(self):
        return time.strftime("%Y_%m_%d_") + self.GDBName

    def get_response(self, url, query='', get_json=True, exit_on_error = False):
        encoded = urllib.urlencode(query)
        request = urllib2.Request(url, encoded)
        if get_json:
            json_response = json.loads(urllib2.urlopen(request).read())
            if 'error' in json_response:
                if (exit_on_error == True):
                    exit()
                else:
                    return json_response
            else:
                return json_response
        return urllib2.urlopen(request).read()

    # check the service URL and return parts as feature service url, url with layer id and layer if
    def check_service(service_url):
        url_parts = {"map_url": None, "layer_url":None, "layer_id":None}
        components = os.path.split(service_url)
        if service_url == None:
            return True
        elif (components[1].isdigit() and os.path.split(components[0])[1] == "MapServer"):
            url_parts["map_url"] = components[0]
            url_parts["layer_url"] = service_url
            url_parts["layer_id"] = str(components[1])
            return url_parts
        elif components[1] == "MapServer":
            url_parts["map_url"] = service_url
            return url_parts
        else:
            return False

