# -------------------------------------------------------------------------------
# Name:        Utilities for SAP services
#
# Purpose:
#
# Author:      vapte
#
# Created:     13/09/2017
# Copyright:   (c) Esri Australia
# Licence:     <your licence>
# -------------------------------------------------------------------------------

import logging, ConfigParser, traceback, datetime, time, os, sys, arcpy, arcgisscripting
import socket
import urllib, urllib2, urlparse, json
import base64
import pypyodbc
from util import Util

class SAPUtils:

    @staticmethod
    # method for updating FLOCs SAPInternalKeys
    def updateSAPInternalKeyAttribute(sap_master_table, master_oids,  max_batch_size,
                                      field_sap_key, field_sap_internal_key,
                                      SAP_service_url, SAP_auth_header):
        search_fields = ["OID@", field_sap_key, field_sap_internal_key]
        batch_start = 0
        while (batch_start < len(master_oids)):
            batch_ids = master_oids[batch_start:batch_start+max_batch_size]
            if (len(batch_ids) == 0):
                break
            batch_start = batch_start + len(batch_ids)
            str_master_ids = ','.join(str(id1) for id1 in batch_ids)

            where_clause = "OBJECTID in (%s) And (%s Is Null Or %s='')" % (str_master_ids, field_sap_internal_key, field_sap_internal_key)
            with arcpy.da.UpdateCursor(in_table=sap_master_table, field_names=search_fields, where_clause=where_clause) as cursor:
                for update_row in cursor:
                    if (update_row[1] is None or update_row[1] == ''):
                        continue
                    sap_internal_key = SAPUtils.getSAPInternalKey(update_row[1], SAP_service_url, SAP_auth_header)
                    if (sap_internal_key != ''):
                        update_row[2] = sap_internal_key
                        cursor.updateRow(update_row)


    @staticmethod
    def getSAPInternalKey(sapKey, serviceURL, authenticatioHeaderValue):
        data = {'SAP_IDS': []}
        data['SAP_IDS'].append({'FunctionalLocationLabel': sapKey})

        headers = {'Authorization': authenticatioHeaderValue}
        values = json.dumps(data)
        req = urllib2.Request(serviceURL, values, headers)
        response = urllib2.urlopen(req).read()
        internalKey = ''
        try:
            json_response = json.loads(response)
            internalKey = json_response['SAP_IDS'][0]['FunctionalLocationKey']
        except Exception:
            internalKey = ''
        return internalKey

    # call SAP service to notify new assets registered in GIS with valid SAP key
    @staticmethod
    def upload_new_assets_to_SAP(master_oids, sap_master_table, fields_to_send, max_batch_size, SAP_service_url, SAP_auth_header):
        # do batch of 20 records
        batch_start = 0
        while (batch_start < len(master_oids)):
            batch_ids = master_oids[batch_start:batch_start+max_batch_size]
            if (len(batch_ids) == 0):
                break
            batch_start = batch_start + len(batch_ids)
            str_master_ids = ','.join(str(id1) for id1 in batch_ids)
            where_clause = "OBJECTID in (%s)" % (str_master_ids)
            tbl_records = arcpy.da.TableToNumPyArray(in_table=sap_master_table,
                                                     field_names=fields_to_send,
                                                     where_clause=where_clause)
            data = {'Records': []}

            for row in tbl_records:
                object_id = row[0]
                guid = row[1]
                sap_key = row[2]
                sap_type = row[3]
                if (object_id is None or guid is None or sap_key is None or sap_type is None):
                    continue
                data['Records'].append(
                    {'GIS_OBJECT_ID': object_id, 'GIS_GUID': guid, 'SAP_KEY': sap_key, 'SAP_ASSET_TYPE': sap_type})

            if (len(data['Records']) > 0):
                json_response = SAPUtils.sap_load_new_assets(data, SAP_service_url, SAP_auth_header)
                # TODO: see if any record failed. Do we care if any failed?

    @staticmethod
    def sap_load_new_assets(dict_new_assets, service_URL, auth_header):
        json_values = json.dumps(dict_new_assets)
        headers = {'Authorization': auth_header}
        req = urllib2.Request(service_URL, json_values, headers)
        response = urllib2.urlopen(req).read()
        json_response = json.loads(response)
        return json_response

    @staticmethod
    def delete_archived_SAP_records(config_parser):
        sap_egdb = config_parser.get('SAP', 'sap_egdb')
        arcpy.env.workspace = sap_egdb
        server=config_parser.get('SAP','sapSqlServer')
        database=config_parser.get('SAP','sapSqlServerDB')
        user=config_parser.get('SAP','sapSqlServerUsername')
        password=config_parser.get('SAP','sapSqlServerPwd')
        dec_password = Util.decode(password)
        #open sql connection
        sqlConnection = Util.getSQLServerConnection(server, database, user, dec_password)
        arcpy.env.workspace = sap_egdb
        fc_names = arcpy.ListFeatureClasses()
        for fc_name in fc_names:
            #fc_name = fc_name.split('.')[-1]
            fc_full_path = os.path.join(sap_egdb, fc_name)
            desc_fc = arcpy.Describe(fc_full_path)
            if (desc_fc.isArchived):
                sqlCur = sqlConnection.cursor()
                cmd = "Delete From {0} Where YEAR(GDB_TO_DATE) < 9999".format(fc_name)
                print "starting {0} ...".format(cmd)
                result = sqlCur.execute(cmd)
                sqlCur.commit()
                sqlCur.close()
                del sqlCur
                print "{0} succeeded.".format(cmd)
		tbl_names = arcpy.ListTables()
        for tbl_name in tbl_names:
            try:
                tbl_full_path = os.path.join(sap_egdb, tbl_name)
                desc_fc = arcpy.Describe(tbl_full_path)
                if (desc_fc.isArchived):
                    sqlCur = sqlConnection.cursor()
                    cmd = "Delete From {0} Where YEAR(GDB_TO_DATE) < 9999".format(tbl_name)
                    print "starting {0} ...".format(cmd)
                    result = sqlCur.execute(cmd)
                    sqlCur.commit()
                    sqlCur.close()
                    del sqlCur
                    print "{0} succeeded.".format(cmd)
            except Exception as ex:
                print ("Failed to delete archived records from '%s'" %(tbl_full_path))

        sqlConnection.close()

    @staticmethod
    def deleteArchivedSAPRecords(sqlConnection, sp_name):
        cur = sqlConnection.cursor()
        cmd = "{0}".format(sp_name)
        result = cur.execute(cmd)
        # result = cur.execute("SELECT 'Hello world'")
        cur.commit()
