#-------------------------------------------------------------------------------
# Name:        SpecialCases
# Purpose:     Functions to deal with special cases when importing data
#
# Author:      mdonnelly
#
# Created:     05/10/2016
# Copyright:   (c) mdonnelly 2016
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import arcpy, os, traceback, sys, shutil, csv
from util import Util

class SpecialCases:


    # Translate fixed phase value to phase designation value
    #
    def FixedPhaseTranslation(self, csvFile, ingdb):

        fixedPhaseArr = []
        #Open the CSV mapping file to find out which feature classes have FixedPhase fields
        with open(csvFile, 'rb') as csvfile:
            csvReader = csv.DictReader(csvfile, delimiter='|')
            for row in csvReader:
                if row["ARCGIS_ATTRIBUTE_NAME"] == "FixedPhase":
                    fixedPhaseArr.append(row["ARCGIS_FEATURE"])

        for fixedPhaseFc in fixedPhaseArr:

            fields = ["FixedPhase"]
            fc = os.path.join(ingdb, fixedPhaseFc)

            workspace = ingdb
            arcpy.env.workspace = workspace

            edit = arcpy.da.Editor(arcpy.env.workspace)
            edit.startEditing(False, True)
            edit.startOperation()

            with arcpy.da.UpdateCursor(fc, fields) as cursor:
                for row in cursor:
                    print str(row[0])

            #TODO: Call function to calculate bitwise value from FixedPhase and write it to Phase Designation

            edit.stopOperation()
            edit.stopEditing(True)

    def getFcNameFromViewName(self, csvFileViews, mappingFile, viewColumn, mappingViewColumn, fcColumn):

         featureClasses = ""
         with open(csvFileViews, 'rb') as csvfile:
            csvReader = csv.DictReader(csvfile, delimiter='|')
            for row in csvReader:
                with open(mappingFile, 'rb') as csvfile2:
                    csvReader2 = csv.DictReader(csvfile2, delimiter='|')
                    for row2 in csvReader2:
                        if row[viewColumn] == row2[mappingViewColumn]:
                            #print row[viewColumn] + " " + row2[fcColumn]
                            featureClasses = featureClasses + row2[fcColumn] + " "
                            break
         print featureClasses

if __name__ == "__main__":

    sc = SpecialCases()
    #sc.FixedPhaseTranslation("E:\\Projects\\TasNetworks\\ETL\\MappingTables\\GTechAttributeMapping.csv",
    #                         "E:\\Projects\\TasNetworks\\DATA\DistributionNetwork.gdb")

    sc.getFcNameFromViewName("E:\\Projects\\TasNetworks\\GCOMM_Views.csv",
                             "E:\\Projects\\TasNetworks\\ETL\\MappingTables\\GCommAttributeMapping.csv",
                             "Views",
                             "GTECH_FEATURE_VIEW",
                             "ARCGIS_FEATURE")