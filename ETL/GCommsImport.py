#-------------------------------------------------------------------------------
# Name:        commsImport
# Purpose:     Launches the Data Interoperability tool to import Distrubtion
#              data into the enterprise geodatabase.
#
# Author:      mdonnelly
#
# Created:     05/10/2016
# Copyright:   (c) mdonnelly 2016
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import arcpy, os, traceback, sys, shutil, csv, time, datetime
from util import Util
from SAPUtils import SAPUtils
from DataLoader import DataLoader
from SAPSchemaLoader import SAPSchemaLoader

NETWORK_TYPE = "COMMS"

if __name__ == "__main__":
    try:
        script_name = os.path.basename(__file__)
        startTime = time.time()

        server_name = Util.getMachineName().upper()

        config_parser = Util.loadConfig(config_file_name='Config.INI')
        logpath = config_parser.get('logging', 'logroot')
        logFilePrefix = config_parser.get("logging", "commsLogFilePrefix")
        loggingLevel = config_parser.getint("logging", "logginglevel")
        load_SAP_objects = config_parser.getboolean("SAP", "load_data")
        load_method = config_parser.get("SAP", "data_load_method")
        is_full_load = (load_method.upper() == "FULL")

        logFile = Util.startLogging(logpath, logFilePrefix, loggingLevel)

        dataLoader = DataLoader(network_type=NETWORK_TYPE, config_parser=config_parser)
        # this is where it all happens
        return_code = dataLoader.loadData()
        #return_code = 1
        if (return_code != Util.RETURN_CODE_ERROR and load_SAP_objects == True):
            # no error, load objects in SAP
            sapLoader = SAPSchemaLoader(network_type=NETWORK_TYPE, config_parser=config_parser)
            return_code_2 = sapLoader.start_SAP_load(is_first_load=is_full_load)
            if (return_code_2 == Util.RETURN_CODE_SUCCESS):
                # create CSV file
                if (is_full_load):
                    sapLoader.create_CSV_file()
            else:
                # can be warning or error
                return_code = return_code_2

        # all done
        if (return_code == Util.RETURN_CODE_ERROR):
            status_completion = "completed with errors"
        elif (return_code == Util.RETURN_CODE_WARNING):
            status_completion = "completed with warnings"
        else:
            status_completion = "completed successfully"

        
        try:
            if (load_SAP_objects == True):
                msg_summary = "Deleting archived records from SAP EGDB feature classes"
                Util.log(msg_summary, Util.LOG_LEVEL_INFO, False, True)
                SAPUtils.delete_archived_SAP_records(config_parser=config_parser)
        except Exception as ex:
            Util.log("Error removing archived records from SAP EGDB: " + ex.message, Util.LOG_LEVEL_ERROR, False, True)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False, True)
            status_completion = "completed with errors"
            
        msg_summary = "ETL Process for %s data load %s on %s at %s" %(NETWORK_TYPE, status_completion, server_name, datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        Util.log(msg_summary, Util.LOG_LEVEL_INFO, False, True)

        send_email = config_parser.getboolean('email', 'emailreport')
        if (send_email):
            with open(logFile) as f:
                report_content = f.readlines()
                dataLoader.send_email_report(msg_summary, report_content)

    except Exception as e:
        Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
        exc_type, exc_value, exc_traceback = sys.exc_info()
        Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False, True)
        returnMessage = "ERROR: " + e.message + ". TRACEBACK: " + repr(traceback.format_exception(exc_type, exc_value, exc_traceback))
    finally:
        endTime = time.time()
        elapsedTime = Util.getElapsedTime(startTime, endTime)
        Util.log("Elapsed time: " + elapsedTime, Util.LOG_LEVEL_INFO, False, True)
        Util.log("Exiting %s" %(script_name), Util.LOG_LEVEL_INFO, False, True)

