# -------------------------------------------------------------------------------
# Name:        Script to load SAP schema from Dist or Comm or Transmission schema
#              Uses feature mapping table to finding mapping between feature and
#
# Purpose:
#
# Author:      vapte
#
# Created:     05/10/2016
# Copyright:   (c) vapte 2016
# Licence:     <your licence>
# -------------------------------------------------------------------------------

import arcpy
import csv
from ConfigParser import SafeConfigParser
# import simplejson as json
import os, sys, math, shutil
import traceback
from os.path import split, exists
import time
import datetime
import imghdr
import re
import tempfile
import zipfile
import codecs
from util import Util
from SAPUtils import SAPUtils
import numpy


# if table/fc has specified field
def hasField(in_table, field_name):
    has_field = False
    fields = arcpy.ListFields(dataset=in_table, wild_card=field_name)
    has_field = len(fields) == 1
    return has_field



def createFieldMap(src_table, src_column, dest_table, dest_column):
    field_map = arcpy.FieldMap()
    field_map.addInputField(src_table, src_column)
    out_field = field_map.outputField
    out_field.name = dest_column
    field_map.outputField = out_field
    return field_map



class SAPSchemaLoader:
    def __init__(self, network_type, config_parser):
        self.network_type = network_type
        self.cp = config_parser
        self.logpath = self.cp.get('logging', 'logroot')
        # set-up network type specific class variables from config file
        self.SAP_EGDB = self.cp.get('SAP', 'sap_egdb')
        self.mapping_source_folder = self.cp.get('SAP', 'mapping_files_folder')
        self.scratch_geodatabase_path = self.cp.get('SAP', 'scratch_workspace_folder')
        self.SAP_schema_xml = self.cp.get('SAP', 'sap_schema')
        self.CSV_file_dest = self.cp.get('SAP', 'csv_file_destination')

        #assetmaster csv file
        self.max_where_ids = int(self.cp.get('program', 'MAX_WHERE_IDS'))
        self.csv_path = self.cp.get('SAP', 'csv_file_destination')


        if (network_type.upper() == 'TRANSMISSION'):

            self.feature_mapping_file = self.cp.get('SAP', 'trans_feature_mapping')
            self.inputEGDB = self.cp.get('geodatabases', 'transmissionEGDB')

        elif (network_type.upper() == 'DISTRIBUTION'):

            self.inputEGDB = self.cp.get('geodatabases', 'distributionEGDB')
            self.feature_mapping_file = self.cp.get('SAP', 'dist_feature_mapping')

        elif (network_type.upper() == "COMMS"):

            self.inputEGDB = self.cp.get('geodatabases', 'commsEGDB')
            self.feature_mapping_file = self.cp.get('SAP', 'comms_feature_mapping')

        # set mapping file
        self.feature_mapping_file = os.path.join(self.mapping_source_folder, self.feature_mapping_file)

        # tables and object names
        self.asset_master_table = self.cp.get('program', 'SAP_master_table')
        self.equip_prefix = self.cp.get('program', 'SAP_equip_prefix')
        self.floc_prefix = self.cp.get('program', 'SAP_functional_loc_prefix')

        # other field names etc
        self.field_g3e_fid = self.cp.get('program', 'GTECHUniqueIDField')
        self.field_sap_key = self.cp.get('program', 'SAP_key_field')
        self.field_sap_internal_key = self.cp.get('program', 'SAP_internal_key_field')
        self.field_sap_parent_key = self.cp.get('program', 'SAP_parent_key_field')
        self.field_gis_object_id = self.cp.get('program', 'SAP_gis_object_id_field')
        self.field_gis_geometry_type = self.cp.get('program', 'SAP_geometry_type_field')
        self.field_sap_asset_type = self.cp.get('program', 'SAP_asset_type_field')
        self.field_gis_class_name = self.cp.get('program', 'SAP_gis_class_field')
        self.field_network_type = self.cp.get('program', 'SAP_network_type_field')
        self.field_sap_class_name = self.cp.get('program', 'SAP_class_field')
        self.field_sap_subclass_name = self.cp.get('program', 'SAP_subclass_field')
        self.field_sap_description = self.cp.get('program', 'SAP_description_field')
        self.field_sap_symbol_rotation = self.cp.get('program', 'SAP_symbolrotation_field')
        self.field_gis_description = self.cp.get('program', 'GIS_description_field')
        self.field_gis_state = self.cp.get('program', 'GIS_state_field')
        self.field_gis_subtype = self.cp.get('program', 'GIS_subtype_field')
        self.field_gis_guid = self.cp.get('program', 'GIS_guid_field')
        self.field_sap_lifeCycleState = self.cp.get('program', 'SAPLifeCycleState')
        self.field_sap_pushed_gtech = self.cp.get('program', 'SAP_pushedtogtech_field')
        self.t_id = self.cp.get('program', 'TID')
        # TODO: remove hardcode
        self.field_src_label_text = "Labeltext"
        self.field_src_state = "State"
        self.field_src_symbol_rotation = "SymbolRotation"
        # SAP integration service related settings, changes per environment
        self.SAP_URL_WM_lookup = self.cp.get('SAP', 'sap_WM_lookup_URL')
        self.SAP_URL_FLOC_internal_key = self.cp.get('SAP', 'sapInternalKeyServiceURL')
        self.SAP_URL_auth_header = self.cp.get('SAP', 'serviceAuthenticationHeader')

    # remove data from sap schema for specified network
    def remove_network_data(self, network_prefix):
        try:
            arcpy.env.workspace = self.SAP_EGDB
            Util.log("Removing data for '%s' network from SAP schema '%s'..." % (network_prefix, self.SAP_EGDB), Util.LOG_LEVEL_DEBUG,
                     True, True)

            ds_list = arcpy.ListDatasets()
            for ds in ds_list:
                fc_list = arcpy.ListFeatureClasses(feature_dataset=ds)
                for fc_name in fc_list:
                    self.remove_data_from_fc(ds, fc_name, network_prefix)
            # get root level FCs
            fc_list = arcpy.ListFeatureClasses()
            for fc_name in fc_list:
                self.remove_data_from_fc("", fc_name, network_prefix)
            table_list = arcpy.ListTables()
            for table_name in table_list:
                self.remove_data_from_table(table_name, network_prefix)
            Util.log("Removed data for '%s' network from SAP schema" % (network_prefix), Util.LOG_LEVEL_INFO,
                     True, True)

        except Exception as ex:
            Util.log("Error removing data for '%s' network: %s " %(network_prefix, ex.message), Util.LOG_LEVEL_ERROR, True, True )

    # remove data from specified Feature Class using filter
    def remove_data_from_fc(self, ds_name, fc_name, network_prefix):

        temp_layer = r"IN_MEMORY\%s" % (fc_name)
        if (arcpy.Exists(temp_layer)):
            arcpy.Delete_management(temp_layer)
        if (ds_name == "" or ds_name is None ):
            fc_full_name = os.path.join(self.SAP_EGDB, fc_name)
        else:
            fc_full_name = os.path.join(self.SAP_EGDB, ds_name, fc_name)

        if (hasField(fc_full_name, self.field_network_type)):
            layer_filter = "%s='%s'" % (self.field_network_type, network_prefix)
            arcpy.MakeFeatureLayer_management(in_features=fc_full_name, out_layer=temp_layer, where_clause=layer_filter)
            arcpy.DeleteFeatures_management(temp_layer)
            arcpy.Delete_management(temp_layer)

    # remove data from specified Table using filter
    def remove_data_from_table(self, table_name, network_prefix):
        temp_layer = r"IN_MEMORY\%s" % (table_name)
        if (arcpy.Exists(temp_layer)):
            arcpy.Delete_management(temp_layer)
        table_full_name = os.path.join(self.SAP_EGDB, table_name)

        if (hasField(table_name, self.field_network_type)):
            layer_filter = "%s='%s'" % (self.field_network_type, network_prefix)
            arcpy.MakeTableView_management(in_table=table_full_name, out_view=temp_layer, where_clause=layer_filter)
            arcpy.DeleteRows_management(in_rows=temp_layer)
            arcpy.Delete_management(temp_layer)

    # create a CSV file for specified network
    def create_CSV_file(self):
        return_code = Util.RETURN_CODE_SUCCESS
        try:
            master_table_full_name = os.path.join(self.SAP_EGDB, self.asset_master_table)
            # only include records that have SAP Key populated
            where_clause = "%s='%s' And %s Is Not Null And %s <> ''" % (self.field_network_type, self.network_type,
                                                                        self.field_sap_key, self.field_sap_key)
            fields_to_export = ["OID@", self.field_gis_guid, self.field_sap_key, self.field_sap_asset_type]
            header_row = ','.join(["GIS_OBJECT_ID", "GIS_GUID", "SAP_KEY", "SAP_ASSET_TYPE"])

            nparr = arcpy.da.FeatureClassToNumPyArray(in_table=master_table_full_name,
                                                      field_names=fields_to_export,
                                                      where_clause=where_clause,
                                                      skip_nulls=True)
            localtime = time.localtime()
            timeString = time.strftime("%d%m%y_%H%M%S", localtime)
            csv_file_name = "%s_Full_Load_%s.csv" %(self.network_type, timeString)
            dest_file = os.path.join(self.CSV_file_dest, csv_file_name)
            if (os.path.exists(dest_file)):
                os.remove(dest_file)
            numpy.savetxt(dest_file, nparr, delimiter=",", fmt="%s", header=header_row)

        except Exception as e:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False,
                     True)
            returnMessage = "ERROR: " + e.message + ". TRACEBACK: " + repr(
                traceback.format_exception(exc_type, exc_value, exc_traceback))
            return_code = Util.RETURN_CODE_ERROR



    #select features by atrributes then copy to FC
    def copySelectedFeatures(self, idValue, idfieldName, fc_toGDB, fc_fromGDB, sap_feature_class_name):
        select_mode = "NEW_SELECTION"
        #selection_ids_str = ','.join(str(fid) for fid in idList)
        #fid_clause = "%s in (%s)" % (fieldName, selection_ids_str)
        fid_clause = "{0} = '{1}' AND {2} = '{3}' AND {4} = {5}".format(self.field_network_type, self.network_type, self.field_gis_class_name,sap_feature_class_name, idfieldName, idValue)
        selection_layer = "lyr"
        schemaType = "TEST"

        if(arcpy.Exists(selection_layer)):
            arcpy.Delete_management(selection_layer)

        arcpy.MakeFeatureLayer_management(fc_fromGDB, selection_layer) 
        arcpy.SelectLayerByAttribute_management(in_layer_or_view=selection_layer,
                                                        selection_type=select_mode,
                                                        where_clause=fid_clause)
        arcpy.Append_management(inputs=[selection_layer], target=fc_toGDB, schema_type=schemaType)

        arcpy.Delete_management(selection_layer)
        return True


    # insert a record in SAP feature class or table
    def insert_sap_fc_record(self, insert_fields, insert_values, sap_fc_name):


        sap_feature_class = os.path.join(self.SAP_EGDB, sap_fc_name)
        insert_cursor = arcpy.da.InsertCursor(in_table=sap_feature_class,
                                              field_names=insert_fields)

        oid = insert_cursor.insertRow(insert_values)
        del insert_cursor
        return oid



    # process data in SAP scratch feature clas, add or update to SAP EGDB
    # it is important to retain ObjectID and GUID in AssetMaster
    def retain_master_oid(self, sap_scratch_workspace, sap_object_type, arcgis_geometry_type, arcgis_object_name, has_rotation_field):

        sap_fc_name = "%s%s" % (sap_object_type, arcgis_geometry_type)
        sap_scratch_feature_class = os.path.join(sap_scratch_workspace, sap_fc_name)
        sap_feature_class = os.path.join(self.SAP_EGDB, sap_fc_name)
        sap_master_table_name = self.asset_master_table
        sap_master_table = os.path.join(self.SAP_EGDB, sap_master_table_name)
        Util.log("Processing '%s' ..." %(sap_feature_class), Util.LOG_LEVEL_DEBUG, False, True)
        list_new_master_oids = []

        uniqueKeyField = self.field_g3e_fid

        search_fields = [self.field_sap_key,
                         self.field_sap_class_name,
                         self.field_sap_subclass_name,
                         self.field_gis_description,
                         self.field_sap_description,
                         self.field_sap_lifeCycleState,
                         self.field_gis_state,
                         self.field_network_type,
                         self.field_gis_class_name,
                         self.field_gis_object_id,
                         self.field_gis_subtype,
                         uniqueKeyField,
                         "SHAPE@"]

        if (has_rotation_field == True):
            search_fields.append(self.field_sap_symbol_rotation)

        # we need only those features which have SAP key popualted
        where_clause = "%s Is Not Null And %s <> '' And %s Is Not Null" %(self.field_sap_key, self.field_sap_key, uniqueKeyField)
        # loop through all features in temp FGDB and find if this is a new asset or existing
        # Existing asset will have Hexagon primary key (G3E_FID) and arcgis feature class combination in SAP EGDB
        da_editor = arcpy.da.Editor(self.SAP_EGDB)
        # for non-versioned and archive enabled EGDB, start edit must be false and true combination 
        da_editor.startEditing(False, True)
        try:
            # search scratch SAP feature class which is built using network feature class in EGDB
            with arcpy.da.SearchCursor(in_table=sap_scratch_feature_class, field_names=search_fields, where_clause=where_clause) as cursor:

                for row in cursor:
                
                    #da_editor.startOperation()

                    # store values that we need to use when inserting (or updating) into SAP EGDB class and table
                    src_sap_key = row[0]
                    src_sap_class_name = row[1]
                    src_sap_subclass_name = row[2]
                    src_gis_description = row[3]
                    src_sap_description = row[4]
                    src_sap_state = row[5]
                    src_gis_state = row[6]
                    src_network_type = row[7]
                    src_arcgis_class_name = row[8]
                    src_arcgis_object_id = row[9]
                    src_gis_subtype = row[10]
                    src_unique_id_value = row[11]
                    src_shape = row[12]
                    src_rotation = 0.0
                    if (has_rotation_field):
                        src_rotation = row[13]

                    
                    # now add/update EGDB
                    try:
                        # search in EGDB with Hexagon system unique id value
                        # if search is successful, update ArcGIS object ID.
                        master_where_clause = "{0}='{1}' AND {2}='{3}' AND {4}={5}".format(
                            self.field_network_type, self.network_type,
                            self.field_gis_class_name, src_arcgis_class_name,
                            uniqueKeyField, src_unique_id_value)

                        master_asset_fields = [self.field_sap_key, self.field_sap_asset_type,
                                               self.field_gis_geometry_type, self.field_gis_object_id]

                        rowFound = False
                        updateable = False
                        
                    
                        with arcpy.da.UpdateCursor(in_table=sap_master_table, field_names=master_asset_fields,
                                                   where_clause=master_where_clause) as update_cursor:
                            for update_row in update_cursor:
                                rowFound = True
                                dest_sap_key = update_row[0]
                                dest_sap_asset_type = update_row[1]
                                dest_geom_type = update_row[2]
                                # save orig objetId to help update SAP feature class
                                orig_gis_object_id = update_row[3]
                                # we need to confirm sap object type, sap key are same

                                if (dest_sap_key == src_sap_key and
                                            dest_sap_asset_type == sap_object_type and
                                            dest_geom_type == arcgis_geometry_type):
                                    updateable = True
                                    update_row[3] = src_arcgis_object_id
                                    update_cursor.updateRow(update_row)
                            
                                    
                                    # master updated. Update feature class now
                                    
                                else:
                                    msg = "Master Object Record for '%s' network '%s' gis feature class and %d G3E_FID points " \
                                          "to different SAP key '%s' or SAP Object Type '%s'" % (
                                          self.network_type, src_arcgis_class_name,
                                          src_unique_id_value, dest_sap_key, dest_sap_asset_type)
                                    Util.log(msg, Util.LOG_LEVEL_WARNING, False, True)

                        if (rowFound and updateable):
                            # update FC
                            fc_where_clause = "{0}='{1}' AND {2}='{3}' AND {4}={5}".format(self.field_network_type,
                                                                                                   self.network_type,
                                                                                                   self.field_gis_class_name,
                                                                                                   src_arcgis_class_name,
                                                                                                   self.field_gis_object_id,
                                                                                                   orig_gis_object_id)
                            fc_search_fields = [self.field_sap_key,
                                                self.field_sap_class_name, self.field_sap_subclass_name,
                                                self.field_gis_object_id,
                                                self.field_sap_description, self.field_gis_description,
                                                self.field_sap_lifeCycleState, self.field_gis_state,
                                                self.field_gis_subtype, uniqueKeyField]
                            row_updated = False
                            with arcpy.da.UpdateCursor(in_table=sap_feature_class,
                                                       field_names=fc_search_fields,
                                                       where_clause=fc_where_clause) as update_fc_cursor:
                                for update_fc_row in update_fc_cursor:
                                    update_fc_row[1] = src_sap_class_name
                                    update_fc_row[2] = src_sap_subclass_name
                                    update_fc_row[3] = src_arcgis_object_id
                                    update_fc_row[4] = src_sap_description
                                    update_fc_row[5] = src_gis_description
                                    update_fc_row[6] = src_sap_state
                                    update_fc_row[7] = src_gis_state
                                    update_fc_row[8] = src_gis_subtype
                                    update_fc_row[9] = src_unique_id_value
                                    update_fc_cursor.updateRow(update_fc_row)
                                    row_updated = True

                            if (row_updated == False and self.network_type.upper() != 'COMMS'):
                                # try with SAP Key if not a COMMS type.
                                fc_where_clause = "{0}='{1}' AND {2}='{3}' AND {4}='{5}'".format(self.field_network_type,
                                                                                               self.network_type,
                                                                                               self.field_gis_class_name,
                                                                                               src_arcgis_class_name,
                                                                                               self.field_sap_key,
                                                                                                 src_sap_key)
                                with arcpy.da.UpdateCursor(in_table=sap_feature_class,
                                                           field_names=fc_search_fields,
                                                           where_clause=fc_where_clause) as update_fc_cursor:
                                    for update_fc_row in update_fc_cursor:
                                        update_fc_row[1] = src_sap_class_name
                                        update_fc_row[2] = src_sap_subclass_name
                                        update_fc_row[3] = src_arcgis_object_id
                                        update_fc_row[4] = src_sap_description
                                        update_fc_row[5] = src_gis_description
                                        update_fc_row[6] = src_sap_state
                                        update_fc_row[7] = src_gis_state
                                        update_fc_row[8] = src_gis_subtype
                                        update_fc_row[9] = src_unique_id_value
                                        update_fc_cursor.updateRow(update_fc_row)
                                        row_updated = True
                            if (row_updated == False):
                                msg = "Failed to update SAP Key '%s' for '%s' network in '%s'. Record only exists in SAP Master Table." % (src_sap_key,
                                                                                                                                           self.network_type,
                                                                                                                                           sap_fc_name)
                                Util.log(msg, Util.LOG_LEVEL_WARNING, False, True)
                                # insert into SAP FC
                                # this is a new asset, add it to SAP FC
                                insert_fields = [self.field_sap_key, self.field_sap_class_name, self.field_sap_subclass_name,
                                                 self.field_gis_description, self.field_sap_description,
                                                 self.field_sap_lifeCycleState, self.field_gis_state,
                                                 self.field_network_type, self.field_gis_class_name, self.field_gis_object_id,
                                                 self.field_gis_subtype, uniqueKeyField, "SHAPE@"]
                                if (has_rotation_field == True):
                                    insert_fields.append(self.field_sap_symbol_rotation)

                                insert_values = [src_sap_key, src_sap_class_name, src_sap_subclass_name,
                                                 src_gis_description, src_sap_description,
                                                 src_sap_state, src_gis_state,
                                                 src_network_type, src_arcgis_class_name, src_arcgis_object_id,
                                                 src_gis_subtype, src_unique_id_value, src_shape]
                                if (has_rotation_field == True):
                                    insert_values.append(src_rotation)

                                self.insert_sap_fc_record(insert_fields=insert_fields, insert_values=insert_values, sap_fc_name=sap_fc_name)

                        elif (rowFound == False):
                            # this is a new asset, add it to SAP FC
                            insert_fields = [self.field_sap_key, self.field_sap_class_name,
                                             self.field_sap_subclass_name,
                                             self.field_gis_description, self.field_sap_description,
                                             self.field_sap_lifeCycleState, self.field_gis_state,
                                             self.field_network_type, self.field_gis_class_name,
                                             self.field_gis_object_id,
                                             self.field_gis_subtype, uniqueKeyField, "SHAPE@"]
                            if (has_rotation_field == True):
                                insert_fields.append(self.field_sap_symbol_rotation)

                            insert_values = [src_sap_key, src_sap_class_name, src_sap_subclass_name,
                                             src_gis_description, src_sap_description,
                                             src_sap_state, src_gis_state,
                                             src_network_type, src_arcgis_class_name, src_arcgis_object_id,
                                             src_gis_subtype, src_unique_id_value, src_shape]
                            if (has_rotation_field == True):
                                insert_values.append(src_rotation)

                            self.insert_sap_fc_record(insert_fields=insert_fields, insert_values=insert_values,
                                                      sap_fc_name=sap_fc_name)

                            # now add it to SAP Master
                            insert_fields = [self.field_sap_key, self.field_sap_asset_type, self.field_gis_geometry_type,
                                             self.field_network_type, self.field_gis_class_name, self.field_gis_object_id,
                                             self.field_sap_pushed_gtech, self.field_sap_internal_key, uniqueKeyField]
                            insert_values = [src_sap_key, sap_object_type, arcgis_geometry_type,
                                             src_network_type, src_arcgis_class_name, src_arcgis_object_id,
                                             "Yes", "", src_unique_id_value]

                            master_oid = self.insert_sap_fc_record(insert_fields=insert_fields, insert_values=insert_values,
                                                      sap_fc_name=sap_master_table)

                            list_new_master_oids.append(master_oid)


                    except Exception as e:
                        exc_type, exc_value, exc_traceback = sys.exc_info()
                        Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
                        Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR,
                                 False, True)
                        msg = "Failed to process %s" % (row[0])
                        Util.log(msg, Util.LOG_LEVEL_ERROR, False, True)
                        return_code = Util.RETURN_CODE_WARNING
                    
                    #da_editor.stopOperation()
        except Exception as e:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR,
                     False, True)
            return_code = Util.RETURN_CODE_WARNING
        
        da_editor.stopEditing(True)
        del da_editor
        return list_new_master_oids

    #select records by atrributes then copy to FC
    def copySelectedRecords(self, idValue, idfieldName, fc_toGDB, fc_fromGDB, sap_feature_class_name):
        select_mode = "NEW_SELECTION"
        #selection_ids_str = ','.join(str(fid) for fid in idList)
        #fid_clause = "%s in (%s)" % (fieldName, selection_ids_str)
        fid_clause = "{0} = '{1}' AND {2} = '{3}' AND {4} = {5}".format(self.field_network_type, self.network_type, self.field_gis_class_name,sap_feature_class_name, idfieldName, idValue)
        selection_layer = r"IN_MEMORY\lyr"
        if (arcpy.Exists(selection_layer)):
            arcpy.Delete_management(selection_layer)

        schemaType = "TEST"

        arcpy.MakeTableView_management(fc_fromGDB, selection_layer) 
        arcpy.SelectLayerByAttribute_management(in_layer_or_view=selection_layer,
                                                        selection_type=select_mode,
                                                        where_clause=fid_clause)
        arcpy.Append_management(inputs=[selection_layer], target=fc_toGDB, schema_type=schemaType)

        arcpy.Delete_management(selection_layer)
        return fid_clause

    def getNewObjectID(self, new_record_where, sap_master_table):
        new_oid=-1
        search_fields = ["OID@"]

        with arcpy.da.SearchCursor(in_table=sap_master_table, field_names=search_fields, where_clause=new_record_where) as cursor:
            for row in cursor:
                new_oid = row[0]
        return new_oid



    # create a CSV file for specified network
    def create_newmaster_lookup_records_csv(self, addedSAPLookupRecords):
        master_table_full_name = os.path.join(self.SAP_EGDB, self.asset_master_table)
        fields_to_export = ["OBJECTID", self.field_gis_guid, self.field_sap_key, self.field_sap_asset_type]
        totalIDsLength = len(addedSAPLookupRecords)
        from_index = 0
        to_index = self.max_where_ids
        current_ids = []
        csv_src_array = []
        try:
            while (abs(totalIDsLength - to_index) < self.max_where_ids):
                current_ids = addedSAPLookupRecords[from_index:to_index]
                selection_ids_str = ','.join(str(fid) for fid in current_ids)
                where_clause = "OBJECTID in (%s)" % (selection_ids_str)
                nparr = arcpy.da.FeatureClassToNumPyArray(in_table=master_table_full_name,
                                                          field_names=fields_to_export,
                                                          where_clause=where_clause,
                                                          skip_nulls=True)
                csv_src_array.extend(nparr)
                from_index = to_index
                to_index += self.max_where_ids

            localtime = time.localtime()
            timeString = time.strftime("%d%m%y_%H%M%S", localtime)
            csv_file_name = "%s_Full_Load_%s.csv" %(self.network_type, timeString)
            dest_file = os.path.join(self.csv_path, csv_file_name)
            if (os.path.exists(dest_file)):
                os.remove(dest_file)
            header_row = ','.join(fields_to_export)
            numpy.savetxt(dest_file, csv_src_array, delimiter=",", fmt="%s", header=header_row)
            return dest_file
        except Exception as e:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False,
                     True)
            returnMessage = "ERROR: " + e.message + ". TRACEBACK: " + repr(
                traceback.format_exception(exc_type, exc_value, exc_traceback))
            return_code = Util.RETURN_CODE_ERROR

    # This method adds data for one feature class into SAP EGDB without checking row-by-row
    # This will be used for initial load of the data
    def fc_full_SAP_load(self, sap_scratch_workspace, sap_object_type, arcgis_geometry_type, arcgis_object_name):
        sap_fc_name = "%s%s" %(sap_object_type, arcgis_geometry_type)
        # this is full load, simply append to SAP EGDB
        sap_scratch_feature_class = os.path.join(sap_scratch_workspace, sap_fc_name)
        sap_scratch_master_table = os.path.join(sap_scratch_workspace, self.asset_master_table)
        sap_egdb_feature_class = os.path.join(self.SAP_EGDB, sap_fc_name)
        sap_master_table = os.path.join(self.SAP_EGDB, self.asset_master_table)
        lyr_src_sap_fc = r"IN_MEMORY\%s" % (sap_fc_name)
        if (arcpy.Exists(lyr_src_sap_fc)):
            arcpy.Delete_management(lyr_src_sap_fc)

        sap_key_filter = "{0} Is Not Null And {0} <> ''".format(self.field_sap_key)
        arcpy.MakeFeatureLayer_management(in_features=sap_scratch_feature_class, out_layer=lyr_src_sap_fc, where_clause=sap_key_filter)

        # transfer data from scratch SAP feature class to EGDB SAP feature class
        field_mappings = arcpy.FieldMappings()
        field_mappings.addTable(lyr_src_sap_fc)
        arcpy.Append_management(inputs=[lyr_src_sap_fc], target=sap_egdb_feature_class,
                                schema_type="NO_TEST", field_mapping=field_mappings)

        arcpy.Delete_management(lyr_src_sap_fc)

        # now load data to SAP asset master table
        # first build scratch sap_egdb_feature_class
        # truncate scratch master table
        arcpy.TruncateTable_management(in_table=sap_scratch_master_table)

        layer_asset_master_table = r"IN_MEMORY\%s" % (self.asset_master_table)
        if (arcpy.Exists(layer_asset_master_table)):
            arcpy.Delete_management(layer_asset_master_table)

        # build a table view from SAP feature class, easy to append
        arcpy.MakeTableView_management(in_table=sap_scratch_feature_class,
                                       out_view=layer_asset_master_table,
                                       where_clause=sap_key_filter)

        field_mappings = arcpy.FieldMappings()
        # map ArcGISObjectID from source to ArcGISObjectID
        if (hasField(layer_asset_master_table, self.field_gis_object_id)):
            field_mappings.addFieldMap(createFieldMap(layer_asset_master_table, self.field_gis_object_id,
                                                      sap_scratch_master_table,
                                                      self.field_gis_object_id))

        if (hasField(layer_asset_master_table, self.field_g3e_fid)):
            field_mappings.addFieldMap(
                createFieldMap(layer_asset_master_table, self.field_g3e_fid,
                               sap_scratch_master_table, self.field_g3e_fid))
        if (hasField(layer_asset_master_table, self.field_sap_key)):
            field_mappings.addFieldMap(
                createFieldMap(layer_asset_master_table, self.field_sap_key,
                               sap_scratch_master_table, self.field_sap_key))
        if (hasField(layer_asset_master_table, self.field_sap_parent_key)):
            field_mappings.addFieldMap(
                createFieldMap(layer_asset_master_table, self.field_sap_parent_key,
                               sap_scratch_master_table, self.field_sap_parent_key))

        arcpy.Append_management(inputs=[layer_asset_master_table],
                                target=sap_scratch_master_table,
                                schema_type="NO_TEST", field_mapping=field_mappings)

        arcpy.CalculateField_management(in_table=sap_scratch_master_table,
                                        field=self.field_sap_asset_type,
                                        expression="'%s'" % (sap_object_type),
                                        expression_type="PYTHON_9.3")

        arcpy.CalculateField_management(in_table=sap_scratch_master_table,
                                        field=self.field_gis_geometry_type,
                                        expression="'%s'" % (arcgis_geometry_type),
                                        expression_type="PYTHON_9.3")

        arcpy.CalculateField_management(in_table=sap_scratch_master_table,
                                        field=self.field_gis_class_name,
                                        expression="'%s'" % (arcgis_object_name),
                                        expression_type="PYTHON_9.3")

        arcpy.CalculateField_management(in_table=sap_scratch_master_table,
                                        field=self.field_network_type,
                                        expression="'%s'" % (self.network_type),
                                        expression_type="PYTHON_9.3")

        field_mappings = arcpy.FieldMappings()
        field_mappings.addTable(sap_scratch_master_table)
        arcpy.Append_management(inputs=[sap_scratch_master_table],
                                target=sap_master_table,
                                schema_type="NO_TEST", field_mapping=field_mappings)

        if (arcpy.Exists(layer_asset_master_table)):
            arcpy.Delete_management(layer_asset_master_table)

    def start_SAP_load(self, is_first_load = False):
        return_code = Util.RETURN_CODE_SUCCESS
        try:
            temp_workspace = ""
            startTime = time.time()
            script_name = os.path.realpath(__file__)

            has_csv_header = True

            exit_now = False
            for input_param in [self.scratch_geodatabase_path, self.feature_mapping_file, self.SAP_EGDB, self.SAP_schema_xml, self.inputEGDB]:
                if (arcpy.Exists(input_param) == False):
                    Util.log('%s not found. Cannot proceed.' % (input_param), Util.LOG_LEVEL_DEBUG, False, True)
                    exit_now = True

            if (is_first_load and arcpy.Exists(self.csv_path) == False):
                Util.log("CSV file path '%s' does not exist. Cannot proceed." % (self.csv_path), Util.LOG_LEVEL_DEBUG, False, True)
                exit_now = True

            if (exit_now):
                return_code = Util.RETURN_CODE_ERROR
                return

            has_m = "DISABLED"
            has_z = "DISABLED"

            sap_master_table_name = self.asset_master_table
            sap_master_table = os.path.join(self.SAP_EGDB, sap_master_table_name)
            dict_features = dict()

            temp_workspace_name = datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
            arcpy.CreateFileGDB_management(out_folder_path=self.scratch_geodatabase_path, out_name=temp_workspace_name)
            temp_workspace = os.path.join(self.scratch_geodatabase_path, "%s.gdb" %(temp_workspace_name))
            arcpy.ImportXMLWorkspaceDocument_management(target_geodatabase=temp_workspace, in_file=self.SAP_schema_xml,
                                                        import_type="SCHEMA_ONLY", config_keyword="DEFAULTS" )

            sap_scratch_master_table = os.path.join(temp_workspace, sap_master_table_name)

            # remove data from destination SAP schema for specific network type, if this is a first full load
            if (is_first_load):
                self.remove_network_data(network_prefix=self.network_type)
            #else we will not remove data, rather we will still build the intermediate GDB then go row by row
            #  and check if the feature of the given G3ID exsit, then update, otherwise we insert. This applies only for Comms
            addedSAPLookupRecords=[]
            with open(self.feature_mapping_file, mode='r') as file_feature_map:
                csv_reader_feat = csv.reader(file_feature_map, delimiter='|')
                if (has_csv_header):
                    next(csv_reader_feat)
                for csv_row in csv_reader_feat:

                    if (str(csv_row[0]).strip() == ''):
                        continue
                    try:
                        arcgis_object_name = csv_row[8].strip()
                        if (arcgis_object_name is None or arcgis_object_name == '' ):
                            # source not mapped to arcgis feature
                            continue

                        arcgis_geometry_type = csv_row[4].strip().title()
                        if (arcgis_geometry_type.lower() == 'table'):
                            continue

                        arcgis_subtype_filter = csv_row[12].strip()
                        sap_object_class = csv_row[13].strip()
                        sap_object_subclass = csv_row[14].strip()
                        sap_object_type = csv_row[15].strip().upper()
                        sap_gis_object_type = csv_row[16].strip().upper()
                        sap_description_code = csv_row[19].strip()

                        msg = "Processing '%s' of type %s..." % (arcgis_object_name, arcgis_geometry_type)
                        Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
                        if (arcgis_geometry_type is None or arcgis_geometry_type == ''
                            or sap_gis_object_type is None or sap_gis_object_type == ''):
                            # no mapping defined, skip
                            continue

                        sap_feature_class_prefix = sap_gis_object_type.upper()

                        src_feature_class = os.path.join(self.inputEGDB, arcgis_object_name)
                        # Set SAPDescription, if defined
                        if (hasField(src_feature_class, self.field_sap_description)):
                            try:
                                if (sap_description_code.strip() != "" ):
                                    msg = "Setting %s to '%s' for '%s'..." % (self.field_sap_description, sap_description_code, arcgis_object_name)
                                    Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
                                    arcpy.CalculateField_management(in_table=src_feature_class,
                                                                    field=self.field_sap_description,
                                                                    expression=sap_description_code,
                                                                    expression_type="PYTHON_9.3")
                            except Exception as e:
                                exc_type, exc_value, exc_traceback = sys.exc_info()
                                msg = "Error setting '%s' for %s : %s" % (self.field_sap_description, arcgis_object_name, e.message)
                                Util.log(msg, Util.LOG_LEVEL_ERROR, False, True)
                                Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)),
                                         Util.LOG_LEVEL_WARNING, False, True)

                        if (sap_gis_object_type is None or sap_gis_object_type == ''):
                            sap_gis_object_type = self.equip_prefix

                        # see if feature class exists
                        if (arcgis_geometry_type is None or arcgis_geometry_type == ''):
                            arcgis_geometry_type = "Point"

                        layer_src_feature = r"IN_MEMORY\%s" % (arcgis_object_name)
                        Util.log("create {0}".format(layer_src_feature), Util.LOG_LEVEL_DEBUG, False, True)
                        if (arcpy.Exists(layer_src_feature)):
                            arcpy.Delete_management(layer_src_feature)

                        if (arcgis_subtype_filter is None or arcgis_subtype_filter == ''):
                            arcpy.MakeFeatureLayer_management(in_features=src_feature_class, out_layer=layer_src_feature)
                        else:
                            arcpy.MakeFeatureLayer_management(in_features=src_feature_class, out_layer=layer_src_feature, where_clause=arcgis_subtype_filter)

                        if (arcpy.Exists(layer_src_feature)):
                            msg = "Loading '%s' in %s Schema with filter '%s'... " % (arcgis_object_name, self.network_type, arcgis_subtype_filter)
                            Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)

                            sap_feature_class_name = "%s%s" %(sap_feature_class_prefix, arcgis_geometry_type)
                            sap_scratch_feature_class = os.path.join(temp_workspace, sap_feature_class_name)
                            sap_feature_class = os.path.join(self.SAP_EGDB, sap_feature_class_name)
                            if (arcpy.Exists(sap_scratch_feature_class)):
                                field_mappings = arcpy.FieldMappings()
                                field_mappings.addFieldMap(createFieldMap(layer_src_feature,
                                                                          "ObjectID",
                                                                          sap_scratch_feature_class,
                                                                          self.field_gis_object_id))
                                hasRotationField=False
                                # add G3E_FID map
                                if (hasField(layer_src_feature, self.field_g3e_fid)):
                                    field_mappings.addFieldMap(createFieldMap(layer_src_feature, self.field_g3e_fid, sap_scratch_feature_class, self.field_g3e_fid))

                                #for TRANSMISSION FC, the unique key field is TID will be mapped to G3E_ID field in SAP FCs
                                if (self.network_type.upper() == "TRANSMISSION" and hasField(layer_src_feature, self.t_id)):
                                    field_mappings.addFieldMap(createFieldMap(layer_src_feature, self.t_id, sap_scratch_feature_class, self.field_g3e_fid))
                                if (hasField(layer_src_feature, self.field_src_state)):
                                    field_mappings.addFieldMap(createFieldMap(layer_src_feature, self.field_src_state, sap_scratch_feature_class, self.field_gis_state))
                                if (hasField(layer_src_feature, self.field_sap_key )):
                                    field_mappings.addFieldMap(createFieldMap(layer_src_feature, self.field_sap_key, sap_scratch_feature_class, self.field_sap_key))
                                if (hasField(layer_src_feature, self.field_gis_subtype)):
                                    field_mappings.addFieldMap(createFieldMap(layer_src_feature, self.field_gis_subtype, sap_scratch_feature_class, self.field_gis_subtype))
                                if (hasField(layer_src_feature, self.field_src_symbol_rotation) and arcgis_geometry_type == "Point"):
                                    field_mappings.addFieldMap(createFieldMap(layer_src_feature, self.field_src_symbol_rotation, sap_scratch_feature_class, self.field_sap_symbol_rotation))
                                    hasRotationField=True
                                if (hasField(layer_src_feature, self.field_src_label_text)):
                                    field_mappings.addFieldMap(createFieldMap(layer_src_feature, self.field_src_label_text, sap_scratch_feature_class, self.field_gis_description))
                                if (hasField(layer_src_feature, self.field_sap_description)):
                                    field_mappings.addFieldMap(createFieldMap(layer_src_feature, self.field_sap_description,sap_scratch_feature_class, self.field_sap_description))

                                arcpy.TruncateTable_management(in_table=sap_scratch_feature_class)

                                arcpy.Append_management(inputs=[layer_src_feature], target=sap_scratch_feature_class,
                                                        schema_type="NO_TEST", field_mapping=field_mappings)

                                arcpy.CalculateField_management(in_table=sap_scratch_feature_class, field=self.field_sap_class_name,
                                                                expression="'%s'" %(sap_object_class), expression_type="PYTHON_9.3" )
                                arcpy.CalculateField_management(in_table=sap_scratch_feature_class, field=self.field_sap_subclass_name,
                                                                expression="'%s'" %(sap_object_subclass), expression_type="PYTHON_9.3")
                                arcpy.CalculateField_management(in_table=sap_scratch_feature_class, field=self.field_gis_class_name,
                                                                expression="'%s'" %(arcgis_object_name), expression_type="PYTHON_9.3")
                                arcpy.CalculateField_management(in_table=sap_scratch_feature_class, field=self.field_network_type,
                                                                expression="'%s'" %(self.network_type), expression_type="PYTHON_9.3")

                                if (is_first_load):
                                    self.fc_full_SAP_load(sap_scratch_workspace=temp_workspace,
                                                          sap_object_type=sap_feature_class_prefix,
                                                          arcgis_geometry_type=arcgis_geometry_type,
                                                          arcgis_object_name=arcgis_object_name)
                                else:
                                    master_oids = self.retain_master_oid(sap_scratch_workspace=temp_workspace,
                                                          sap_object_type=sap_feature_class_prefix,
                                                          arcgis_geometry_type=arcgis_geometry_type,
                                                          arcgis_object_name=arcgis_object_name,
                                                           has_rotation_field=hasRotationField)

                                    if (len(master_oids) > 0):
                                        # new assets encountered
                                        # send the master object ids to SAP
                                        fields_for_sap = ["OBJECTID", self.field_gis_guid, self.field_sap_key, self.field_sap_asset_type]
                                        SAPUtils.upload_new_assets_to_SAP(master_oids=master_oids,
                                                                      sap_master_table=sap_master_table,
                                                                      fields_to_send=fields_for_sap,
                                                                      max_batch_size=self.max_where_ids,
                                                                      SAP_service_url=self.SAP_URL_WM_lookup,
                                                                      SAP_auth_header=self.SAP_URL_auth_header )
                                        if (sap_gis_object_type == "FLOC"):
                                            # new FLOCs, get internal key
                                            SAPUtils.updateSAPInternalKeyAttribute(sap_master_table=sap_master_table,
                                                                                   master_oids=master_oids,
                                                                                   max_batch_size=self.max_where_ids,
                                                                                   field_sap_key=self.field_sap_key,
                                                                                   field_sap_internal_key=self.field_sap_internal_key,
                                                                                   SAP_service_url=self.SAP_URL_FLOC_internal_key,
                                                                                   SAP_auth_header=self.SAP_URL_auth_header)

                        arcpy.Delete_management(layer_src_feature)
                        Util.log("delete {0}".format(layer_src_feature), Util.LOG_LEVEL_DEBUG, False, True)
                    except Exception as e:
                        exc_type, exc_value, exc_traceback = sys.exc_info()
                        Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
                        Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)),
                                 Util.LOG_LEVEL_ERROR, False, True)
                        msg = "Failed to process row %s" % (csv_row[0])
                        Util.log(msg, Util.LOG_LEVEL_ERROR, False, True)
                        return_code = Util.RETURN_CODE_WARNING



        except Exception as e:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
            Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False,
                     True)
            returnMessage = "ERROR: " + e.message + ". TRACEBACK: " + repr(
                traceback.format_exception(exc_type, exc_value, exc_traceback))
            return_code = Util.RETURN_CODE_ERROR

        finally:
            # if (os.path.exists(temp_workspace)):
            #     arcpy.Delete_management(in_data=temp_workspace)
            #
            # #just in case
            # if os.path.isdir(temp_workspace):
            #     shutil.rmtree(temp_workspace)

            endTime = time.time()
            elapsedTime = Util.getElapsedTime(startTime, endTime)
            Util.log("SAP Asset Schema Load Duration: " + elapsedTime, Util.LOG_LEVEL_INFO, False, True)
            return return_code
