#-------------------------------------------------------------------------------
# Name:        SpecialCases
# Purpose:     Functions to deal with special cases when importing data
#
# Author:      mdonnelly
#
# Created:     05/10/2016
# Copyright:   (c) mdonnelly 2016
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import arcpy, os, traceback, sys, shutil, csv, cx_Oracle, pypyodbc
from util import Util

class AutoTest:

    gtechFeatureView = "GTECH_FEATURE_VIEW"
    geomediaFeature = "GEOMEDIA_FEATURE_VIEW"
    geomediaFieldName = "FIELD_NAME"
    gtechFieldName = "FIELD_NAME"
    arcgisFeature = "ARCGIS_FEATURE"
    arcgisAttributeName = "ARCGIS_ATTRIBUTE_NAME"
    gtechValue = "GTECH_VALUE"
    arcgisValue = "ARCGIS_VALUE"
    g3eFID = "G3E_FID"
    comment = "COMMENT"
    viewCountField = "VIEW_COUNT"
    fcCountField = "FEATURE_CLASS_COUNT"

    def __init__(self):
        self.cp = Util.loadConfig()
        self.logpath = self.cp.get('config', 'logroot')
        self.loggingLevel = self.cp.getint("config", "logginglevel")
        self.logFilePrefix = self.cp.get("config", "autoTestLogFilePrefix")
        Util.startLogging(self.logpath, self.logFilePrefix, self.loggingLevel)
        self.GTECHMappingFileName = self.cp.get('config', 'GTECHMappingFileName')
        self.GCommMappingFileName = self.cp.get('config', 'GCommMappingFileName')
        self.mappingFileSourceFolder = self.cp.get('config', 'mappingFileSourceFolder')
        self.distributionOracleConnection = self.cp.get('config', 'distributionOracleConnection')
        self.commsOracleConnection = self.cp.get('config', 'commsOracleConnection')
        self.GTECHUser = self.cp.get('config', 'GTECHUser')
        self.GTECHPwd = self.cp.get('config', 'GTECHPwd')
        self.AEGCOMMUser = self.cp.get('config', 'AEGCOMMUser')
        self.AEGCOMMPwd = self.cp.get('config', 'AEGCOMMPwd')
        self.distributionEGDB = self.cp.get('config', 'distributionEGDB')
        self.distributionInterimFGDB = self.cp.get('config', 'distributionInterimFGDB')
        self.commsEGDB = self.cp.get('config', 'commsEGDB')
        self.commsInterimFGDB = self.cp.get('config', 'commsInterimFGDB')
        self.distributionNameSpace = self.cp.get('config', 'distributionNameSpace')
        self.distributionErrorFile = os.path.join(self.logpath, self.cp.get('config', 'distributionErrorFile'))
        self.distributionCountFile = os.path.join(self.logpath, self.cp.get('config', 'distributionCountFile'))
        self.commsErrorFile = os.path.join(self.logpath, self.cp.get('config', 'commsErrorFile'))
        self.commsCountFile = os.path.join(self.logpath, self.cp.get('config', 'commsCountFile'))
        self.commsNamespace = self.cp.get('config', 'commsNamespace')
        self.distributionNameSpace = self.cp.get('config', 'distributionNameSpace')
        self.TransmissionMappingFileName = self.cp.get('config', 'TransmissionMappingFileName')
        self.transmissionEGDB = self.cp.get('config', 'transmissionEGDB')
        self.transmissionInterimFGDB = self.cp.get('config', 'transmissionInterimFGDB')
        self.transmissionSqlServer = self.cp.get('config', 'transmissionSqlServer')
        self.transmissionSqlServerDatabase = self.cp.get('config', 'transmissionSqlServerDatabase')
        self.transmissionSqlServerUser = self.cp.get('config', 'transmissionSqlServerUser')
        self.transmissionSqlServerPwd = self.cp.get('config', 'transmissionSqlServerPwd')
        self.transmissionErrorFile = os.path.join(self.logpath, self.cp.get('config', 'transmissionErrorFile'))
        self.transmissionCountFile = os.path.join(self.logpath, self.cp.get('config', 'transmissionCountFile'))
        self.distributionBadGeometriesTable = self.cp.get('config', 'distributionBadGeometriesTable')
        self.commsBadGeometriesTable = self.cp.get('config', 'commsBadGeometriesTable')
        self.transmissionBadGeometriesTable = self.cp.get('config', 'transmissionBadGeometriesTable')
        self.testResultsGDB = os.path.join(self.logpath, self.cp.get('config', 'testResultsGDB'))

    # Identify the modifications since the last time the tool was run and write them to a table
    #
    def getOracleConnection(self, connection, credentials):

        connectionString = credentials + "@" + connection
        oracleConnection = cx_Oracle.connect(connectionString)

        return oracleConnection

    def getSQLServerConnection(self, server, database, user, password):
        return pypyodbc.connect('Driver={SQL Server};'
                                'Server=' + server + ';'
                                'Database=' + database + ';'
                                'uid=' + user + ';pwd=' + password)

     # Uses the mapping csv file to compare field values and writes a report to a csv file
    #
    def checkAttributes(self, csvFileIn, csvFileOut, connection, outgdb, namespace, type):

        if type == "GEOMEDIA":
            databaseFeatureView = self.geomediaFeature
            databaseFieldName = self.geomediaFieldName
        else:
            databaseFeatureView = self.gtechFeatureView
            databaseFieldName = self.gtechFieldName

        cur = connection.cursor()
        #Read in the CSV file
        with open(csvFileIn, 'rb') as csvfilein:
            csvReader = csv.DictReader(csvfilein, delimiter='|')

            with open(csvFileOut, 'w') as csvfileout:
                fieldnames = [databaseFeatureView, databaseFieldName, self.arcgisFeature, self.arcgisAttributeName, self.g3eFID, self.gtechValue, self.arcgisValue, self.comment]
                writer = csv.DictWriter(csvfileout, fieldnames=fieldnames)
                writer.writeheader()

                for row in csvReader:
                    if not row[self.arcgisAttributeName] == "":
                        fc = os.path.join(outgdb, row[self.arcgisFeature])
                        if not arcpy.Exists(fc):

                            writer.writerow({databaseFeatureView: row[databaseFeatureView],
                                             databaseFieldName: row[databaseFieldName],
                                             self.arcgisFeature: row[self.arcgisFeature],
                                             self.arcgisAttributeName: row[self.arcgisAttributeName],
                                             self.g3eFID: "",
                                             self.gtechValue: "",
                                             self.arcgisValue: "",
                                             self.comment: "Feature class does not exist. Skipping"},)
                            continue

                        if row[self.arcgisAttributeName] == 'Shape' or row[self.arcgisAttributeName] == 'SHAPE':
                            continue

                        if row[self.arcgisAttributeName] == "MeasuredLength" and row[databaseFieldName] == "LENGTH":
                            continue

                        # TODO Select random row:
                        # Warning: computationally expensive
                        # SELECT * FROM
                        #   ( SELECT * FROM self.gtechFeatureView
                        #  ORDER BY DBMS_RANDOM.VALUE )
                        #  WHERE ROWNUM = 1

                        columnSelectStatment = "Select column_name from user_tab_cols where table_name=upper('" + row[databaseFeatureView] + "')"
                        cur.execute(columnSelectStatment)
                        columnList = ""
                        g3e_fidIndex = 0
                        databaseFieldIndex = 0
                        count = 0
                        for result in cur:
                            if result[0] == "SHAPE":
                                continue
                            if result[0] == "G3E_FID":
                                g3e_fidIndex = count
                            elif result[0] == row[databaseFieldName]:
                                databaseFieldIndex =  count
                            columnList = columnList + result[0] + ","
                            count = count + 1

                        if namespace == None or namespace == "":
                            selectStatement = "SELECT " + columnList[:-1] + " FROM " + row[databaseFeatureView]
                        else:
                            selectStatement = "SELECT " + columnList[:-1] + " FROM " + namespace + "." + row[databaseFeatureView]

                        # if namespace == None or namespace == "":
                        #     selectStatement = "SELECT " + row[databaseFieldName] + "," + self.g3eFID + " FROM " + row[databaseFeatureView]
                        # else:
                        #     selectStatement = "SELECT " + row[databaseFieldName] + "," + self.g3eFID + " FROM " + namespace + "." + row[databaseFeatureView]
                        try:
                            cur.execute(selectStatement)
                        except Exception as e:

                            writer.writerow({databaseFeatureView: row[databaseFeatureView],
                                             databaseFieldName: row[databaseFieldName],
                                             self.arcgisFeature: row[self.arcgisFeature],
                                             self.arcgisAttributeName: row[self.arcgisAttributeName],
                                             self.g3eFID: "",
                                             self.gtechValue: "",
                                             self.arcgisValue: "",
                                             self.comment: "Database select statement error. Skipping"},)
                            continue

                        databaseValue = ""
                        g3e_fid = 0

                        # if databaseType == "oracle":
                        for result in cur:
                            databaseValue = result[databaseFieldIndex]
                            g3e_fid = result[g3e_fidIndex]
                            break
                        # elif databaseType == "sqlserver":
                        #     result = cur.fetchone()
                        #     databaseValue = result[0]

                        fields = [row[self.arcgisAttributeName]]
                        gdbValue = ""

                        try:
                            whereClause = '{} = '.format(arcpy.AddFieldDelimiters(fc, self.g3eFID)) + str(g3e_fid)
                            with arcpy.da.SearchCursor(fc, fields, where_clause=whereClause) as cursor:
                                rowFound = False
                                for fcRow in cursor:
                                    rowFound = True
                                    gdbValue = fcRow[0]
                                    break
                                if not rowFound:
                                    print "Could not find GDE_FID: " + str(g3e_fid) + " in " + fc
                        except Exception as e:

                            writer.writerow({databaseFeatureView: row[databaseFeatureView],
                                             databaseFieldName: row[databaseFieldName],
                                             self.arcgisFeature: row[self.arcgisFeature],
                                             self.arcgisAttributeName: row[self.arcgisAttributeName],
                                             self.g3eFID: str(g3e_fid),
                                             self.gtechValue: databaseValue,
                                             self.arcgisValue: "",
                                             self.comment: "Feature class select error. Skipping."},)
                            continue

                        if databaseValue is None and gdbValue == "":
                            continue

                        if Util.isInt(databaseValue):
                            if not Util.isInt(gdbValue):
                                writer.writerow({databaseFeatureView: row[databaseFeatureView],
                                                 databaseFieldName: row[databaseFieldName],
                                                 self.arcgisFeature: row[self.arcgisFeature],
                                                 self.arcgisAttributeName: row[self.arcgisAttributeName],
                                                 self.g3eFID: str(g3e_fid),
                                                 self.gtechValue: databaseValue,
                                                 self.arcgisValue: gdbValue,
                                                 self.comment: "Value mismatch: int vs non int"},)
                                continue
                            databaseValueInt = int(databaseValue)
                            gdbValueInt = int(gdbValue)

                            if gdbValueInt != databaseValueInt:
                                writer.writerow({databaseFeatureView: row[databaseFeatureView],
                                                 databaseFieldName: row[databaseFieldName],
                                                 self.arcgisFeature: row[self.arcgisFeature],
                                                 self.arcgisAttributeName: row[self.arcgisAttributeName],
                                                 self.g3eFID: str(g3e_fid),
                                                 self.gtechValue: databaseValue,
                                                 self.arcgisValue: gdbValue,
                                                 self.comment: "Value mismatch"},)

                        elif gdbValue != databaseValue:
                            writer.writerow({databaseFeatureView: row[databaseFeatureView],
                                             databaseFieldName: row[databaseFieldName],
                                             self.arcgisFeature: row[self.arcgisFeature],
                                             self.arcgisAttributeName: row[self.arcgisAttributeName],
                                             self.g3eFID: str(g3e_fid),
                                             self.gtechValue: databaseValue,
                                             self.arcgisValue: gdbValue,
                                             self.comment: "Value mismatch"},)

        cur.close()

    # Reads in the mapping csv file and creates a dictionary of feature classes with lists of views
    #
    def getFcViews(self, csvFileIn, type):

        fcList = {}
        with open(csvFileIn, 'rb') as csvfilein:
            csvReader = csv.DictReader(csvfilein, delimiter='|')

            for row in csvReader:
                viewExists = False
                fcName = row[self.arcgisFeature]
                if type == "GEOMEDIA":
                    viewName = row[self.geomediaFeature]
                else:
                    viewName = row[self.gtechFeatureView]
                if fcName == "":
                    continue

                if fcName not in fcList:
                    fcList[fcName] = []
                else:
                    for view in fcList[fcName]:
                        if viewName == view:
                            viewExists = True
                            break

                if not viewExists:
                    fcList[fcName].append(viewName)

        return fcList

    # Compares the row counts between the database and geodatabase tables and writes results to a csv file
    #
    def getCountsDatabaseGDB(self, fcList, csvFileOut, connection, outgdb, namespace):

        cur = connection.cursor()

        with open(csvFileOut, 'w') as csvfileout:
            fieldnames = [self.gtechFeatureView, self.arcgisFeature, self.viewCountField, self.fcCountField, self.comment]
            writer = csv.DictWriter(csvfileout, fieldnames=fieldnames)
            writer.writeheader()

            for fcName, views in fcList.items():

                fc = os.path.join(outgdb, fcName)

                if not arcpy.Exists(fc):
                    writer.writerow({self.gtechFeatureView: "",
                                     self.arcgisFeature: fc,
                                     self.viewCountField: "0",
                                     self.fcCountField: "0",
                                     self.comment: "Feature class does not exist. Skipping"},)
                    continue

                try:
                    result = arcpy.GetCount_management(fc)
                    fcCount = int(result.getOutput(0))
                except Exception as e:
                    writer.writerow({self.gtechFeatureView: view,
                                     self.arcgisFeature: fcName,
                                     self.countField: "0",
                                     self.comment: "Error in getting count from: " + fcName},)
                    continue

                viewsTotal = 0
                for view in views:

                    if namespace == None or namespace == "":
                        selectStatement = "SELECT COUNT(*) FROM " + view
                    else:
                        selectStatement = "SELECT COUNT(*) FROM " + namespace + "." + view
                    try:
                        cur.execute(selectStatement)
                    except Exception as e:
                        writer.writerow({self.gtechFeatureView: view,
                                         self.arcgisFeature: fcName,
                                         self.viewCountField: "0",
                                         self.fcCountField: "0",
                                         self.comment: "Oracle view select statement error. Skipping"},)
                        continue

                    databaseValue = 0
                    for result in cur:
                        databaseValue = result[0]
                        viewsTotal = viewsTotal + databaseValue

                        if len(views) > 1:
                            writer.writerow({self.gtechFeatureView: view,
                                             self.arcgisFeature: fcName,
                                             self.viewCountField: str(databaseValue),
                                             self.fcCountField: "",
                                             self.comment: "View sub total"},)
                        else:
                            if not fcCount == databaseValue:
                                writer.writerow({self.gtechFeatureView: view,
                                                 self.arcgisFeature: fcName,
                                                 self.viewCountField: databaseValue,
                                                 self.fcCountField: fcCount,
                                                 self.comment: "Count mismatch"},)
                            else:
                                writer.writerow({self.gtechFeatureView: view,
                                                 self.arcgisFeature: fcName,
                                                 self.viewCountField: databaseValue,
                                                 self.fcCountField: fcCount,
                                                 self.comment: "Match"},)
                        break

                if len(views) > 1:
                    if not fcCount == viewsTotal:
                        writer.writerow({self.gtechFeatureView: view,
                                         self.arcgisFeature: fcName,
                                         self.viewCountField: str(viewsTotal),
                                         self.fcCountField: fcCount,
                                         self.comment: "View total. Count mismatch"},)
                    else:
                        writer.writerow({self.gtechFeatureView: view,
                                         self.arcgisFeature: fcName,
                                         self.viewCountField: str(viewsTotal),
                                         self.fcCountField: fcCount,
                                         self.comment: "View total. Match"},)


        cur.close()

    # Finds all the bad geometries
    # Assumes the use of a file geodatabase
    #
    def findEmptyGeometries(self, fcList, ingdb, outTable, outgdb):

        if not arcpy.Exists(outgdb):
            gdbfolder = os.path.dirname(outgdb)
            gdbname = os.path.basename(outgdb)
            arcpy.CreateFileGDB_management(out_folder_path=gdbfolder,out_name=gdbname)

        outTablePath = os.path.join(outgdb, outTable)

        try:
            if arcpy.Exists(outTablePath):
                    arcpy.Delete_management(outTablePath)
        except Exception as e:
            Util.log("Could not delete existing bad geometries table: {}.".format(outTablePath),
                     Util.LOG_LEVEL_ERROR, False, True)
            return

        checkList = []
        for fc in fcList:

            featureClassPath = os.path.join(ingdb, fc)
            if not arcpy.Exists(featureClassPath):
                Util.log("Table/Feature Class did not exist: {}.".format(featureClassPath),
                         Util.LOG_LEVEL_ERROR, False, True)
                continue

            desc = arcpy.Describe(featureClassPath)
            isFC = desc.datasetType == 'FeatureClass'
            if not isFC:
                continue

            checkList.append(featureClassPath)

        try:
            arcpy.CheckGeometry_management(in_features=checkList, out_table=outTablePath)
        except Exception as e:
            Util.log("Could not execute CheckGeometry: {}.".format(outTablePath),
                     Util.LOG_LEVEL_ERROR, False, True)

    # Finds empty geometries with the option of nominating an id field
    #
    # def findEmptyGeometries2(self, workspace, fcList, idField, outCsvFile):
    #
    #     for fc in fcList:
    #
    #         featureClassPath = os.path.join(workspace, fc)
    #         if not arcpy.Exists(featureClassPath):
    #             Util.log("Table/Feature Class did not exist: {}.".format(featureClassPath),
    #                      Util.LOG_LEVEL_ERROR, False, True)
    #             continue
    #
    #         desc = arcpy.Describe(featureClassPath)
    #         isFC = desc.datasetType == 'FeatureClass'
    #         if not isFC:
    #             continue
    #
    #         if desc.shapeType == "Point":
    #             continue
    #
    #         try:
    #             fields = [idField, "SHAPE@LENGTH"]
    #             #whereClause = '{} = '.format(arcpy.AddFieldDelimiters(fc, self.g3eFID)) + str(g3e_fid)
    #             with arcpy.da.SearchCursor(featureClassPath, fields) as cursor:
    #                 for fcRow in cursor:
    #                     idValue = fcRow[0]
    #                     lengthValue = fcRow[1]
    #
    #                     if not lengthValue > 0.0:
    #                         print "Bad geometry in: " + fc + ", " + str(idValue)
    #         except Exception as e:
    #             print "Blah"


if __name__ == "__main__":

    at = AutoTest()

    mappingFile = os.path.join(at.mappingFileSourceFolder, at.GTECHMappingFileName)
    fcViews = at.getFcViews(mappingFile, "GTECH")
    at.GTECHPwd = Util.decode(at.GTECHPwd)
    GTECHCredentials = at.GTECHUser + "/" + at.GTECHPwd
    oracleConnection = at.getOracleConnection(at.distributionOracleConnection, GTECHCredentials)
    # outFile = os.path.join(at.logpath, at.distributionCountFile)
    # at.getCountsDatabaseGDB(fcViews, outFile, oracleConnection, at.distributionEGDB, at.distributionNameSpace)
    outFile = os.path.join(at.logpath, "GTECHErrors.csv")
    at.checkAttributes(mappingFile, outFile, oracleConnection, at.distributionEGDB, at.distributionNameSpace, "GTECH")
    at.findEmptyGeometries(fcViews, at.distributionInterimFGDB, at.distributionBadGeometriesTable, at.testResultsGDB)

    # mappingFile = os.path.join(at.mappingFileSourceFolder, at.GCommMappingFileName)
    # fcViews = at.getFcViews(mappingFile, "GTECH")
    # at.AEGCOMMPwd = Util.decode(at.AEGCOMMPwd)
    # GCommCredentials = at.AEGCOMMUser + "/" + at.AEGCOMMPwd
    # oracleConnection = at.getOracleConnection(at.distributionOracleConnection, GCommCredentials)
    # outFile = os.path.join(at.logpath, at.commsCountFile)
    # at.getCountsDatabaseGDB(fcViews, outFile, oracleConnection, at.commsEGDB, at.commsNamespace)
    # outFile = os.path.join(at.logpath, at.commsErrorFile)
    # at.checkAttributes(mappingFile, outFile, oracleConnection, at.commsEGDB, at.commsNamespace, "GTECH")
    # at.findEmptyGeometries(fcViews, at.commsInterimFGDB, at.commsBadGeometriesTable, at.testResultsGDB)

    # mappingFile = os.path.join(at.mappingFileSourceFolder, at.TransmissionMappingFileName)
    # fcViews = at.getFcViews(mappingFile, "GEOMEDIA")
    # at.transmissionSqlServerPwd = Util.decode(at.transmissionSqlServerPwd)
    # sqlServerConnection = at.getSQLServerConnection(at.transmissionSqlServer, at.transmissionSqlServerDatabase, at.transmissionSqlServerUser, at.transmissionSqlServerPwd)
    # outFile = os.path.join(at.logpath, at.transmissionCountFile)
    # at.getCountsDatabaseGDB(fcViews, outFile, sqlServerConnection, at.transmissionEGDB, "")
    # outFile = os.path.join(at.logpath, at.transmissionErrorFile)
    # #at.checkAttributes(mappingFile, outFile, sqlServerConnection, at.transmissionEGDB, "", "GEOMEDIA")
    # at.findEmptyGeometries(fcViews, at.transmissionInterimFGDB, at.transmissionBadGeometriesTable, at.testResultsGDB)
    #at.findEmptyGeometries2(at.transmissionEGDB, fcViews, "OBJECTID", "")