import sys

if __name__ == "__main__":
    try:

        if len(sys.argv) > 1:
            toEncodeStr = str(sys.argv[1])
            encodedStr = toEncodeStr.encode('base64','strict')
            print "\nEncoded String:\n" + encodedStr
        else:
            print "Please enter a string to encode"

    except Exception as e:
        message = str(e.message)
        print message