import arcpy
import os,sys,traceback
from os import path
from arcpy import env



class RegisterGeodatabaseObjects(object):
    def __init__(self, wspacePath, logger):
        self.workspace = wspacePath
        self.logger = logger

    def RegisterAsVersioned(self, datasetSrcFullPath, baseEdits):
        logger = self.logger
        retVal = -1
        try:
            if (arcpy.Exists(datasetSrcFullPath)):
                desc = arcpy.Describe(datasetSrcFullPath)
                if (desc.canVersion == False):
                    logger.log("Dataset '%s' cannot be versioned." %(datasetSrcFullPath), logger.LOG_LEVEL_WARNING)
                elif (desc.isVersioned == True):
                    logger.log("Dataset '%s' is already versioned." %(datasetSrcFullPath), logger.LOG_LEVEL_WARNING)
                else:
                    logger.log("Versioning '%s' dataset ..." %(datasetSrcFullPath), logger.LOG_LEVEL_DEBUG)
                    #arcpy.RegisterAsVersioned_management(in_dataset=datasetSrcFullPath, edit_to_base=baseEdits)
                    arcpy.RegisterAsVersioned_management(in_dataset=datasetSrcFullPath)

                    logger.log("Versioned dataset '%s'." %(datasetSrcFullPath), logger.LOG_LEVEL_INFO)
                    retVal = 0
            else:
                logger.log("Dataset '%s' does not exist or is invalid." %(datasetSrcFullPath))
        except Exception as ex:
            error = traceback.format_exc()
            logger.log("Failed to perform RegisterAsVersioned on '%s': %s." %(datasetSrcFullPath, error), logger.LOG_LEVEL_ERROR)
        return retVal

    def UnRegisterAsVersioned(self, datasetSrcFullPath, keepEdit, compressDefault):
        logger = self.logger
        retVal = -1
        try:
            if (arcpy.Exists(datasetSrcFullPath)):
                desc = arcpy.Describe(datasetSrcFullPath)
                if (desc.canVersion == False):
                    logger.log("Dataset '%s' cannot be versioned." %(datasetSrcFullPath), logger.LOG_LEVEL_WARNING)
                elif (desc.isVersioned == False):
                    logger.log("Dataset '%s' is not versioned." %(datasetSrcFullPath), logger.LOG_LEVEL_WARNING)
                else:
                    logger.log("Unversioning '%s' dataset with '%s' and '%s'..." %(datasetSrcFullPath, keepEdit, compressDefault), logger.LOG_LEVEL_DEBUG)
                    #arcpy.UnregisterAsVersioned_management(in_dataset=datasetSrcFullPath, keep_edit=keepEdit, compress_default=compressDefault)
                    arcpy.UnregisterAsVersioned_management(in_dataset=datasetSrcFullPath)
                    logger.log("Unversioned dataset '%s'." %(datasetSrcFullPath), logger.LOG_LEVEL_INFO)
                    retVal = 0
            else:
                logger.log("Dataset '%s' does not exist or is invalid." %(datasetSrcFullPath))

        except Exception as ex:
            error = traceback.format_exc()
            logger.log("Failed to perform RegisterAsVersioned on '%s': %s." %(datasetSrcFullPath, error), logger.LOG_LEVEL_ERROR)
        return retVal

    def RegisterDataset(self, datasetName, baseEdits):
        datasetSrcFullPath = os.path.join(self.workspace, datasetName)
        return self.RegisterAsVersioned(datasetSrcFullPath, baseEdits)


    def UnRegisterDataset(self, datasetName, keepEdit, compressDefault):
        retVal = -1
        datasetSrcFullPath = os.path.join(self.workspace, datasetName)
        return self.UnRegisterAsVersioned(datasetSrcFullPath, keepEdit, compressDefault)

    def RegisterDatasetFeature(self, datasetName, featureName, baseEdits):
        datasetSrcFullPath = os.path.join(self.workspace, datasetName, featureName)
        return self.RegisterAsVersioned(datasetSrcFullPath, baseEdits)


    def UnRegisterDatasetFeature(self, datasetName, featureName, keepEdit, compressDefault):
        retVal = -1
        datasetSrcFullPath = os.path.join(self.workspace, datasetName, featureName)
        return self.UnRegisterAsVersioned(datasetSrcFullPath, keepEdit, compressDefault)


# class for logging, currently only arcpy gp logging
# Vish Apte, Initial Version, 01-Aug-2013
class Logger(object):

    def __init__(self, logLevel):
        self.LOG_LEVEL_DEBUG = 3
        self.LOG_LEVEL_INFO = 2
        self.LOG_LEVEL_WARNING = 1
        self.LOG_LEVEL_ERROR = 0
        self.LOG_LEVEL = logLevel

    def log(self, message, logLevel=0, step=1):
        message = message.replace("\n", "->")
        logMessage = "%02d %s" % (step, message)
        date = str(datetime.datetime.now())
        timestamp = str(datetime.datetime.now()) #.replace(":","-")
        if (logLevel == self.LOG_LEVEL_DEBUG and self.LOG_LEVEL >= self.LOG_LEVEL_DEBUG ):
            arcpy.AddMessage("%s:*** %s ***"%(timestamp, logMessage))
        if (logLevel == self.LOG_LEVEL_INFO and self.LOG_LEVEL >= self.LOG_LEVEL_INFO ):
            arcpy.AddMessage("%s:*** %s ***"%(timestamp, logMessage))
        if (logLevel == self.LOG_LEVEL_WARNING and self.LOG_LEVEL >= self.LOG_LEVEL_WARNING ):
            arcpy.AddWarning("%s:*** %s ***"%(timestamp, logMessage))
        if (logLevel == self.LOG_LEVEL_ERROR and self.LOG_LEVEL >= self.LOG_LEVEL_ERROR ):
            arcpy.AddError("%s:*** %s ***"%(timestamp, logMessage))

class ParameterError(Exception):
    pass

class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the
        .pyt file)."""
        self.label = "Utility Tools"
        self.alias = "Tools"

        # List of tool classes associated with this toolbox
        self.tools = [DeleteDomains, CreateDomainUsingFile, UpperCaseFieldNames, DeleteAllDatasets, CopyDatasets, RegisterAsVersioned, UnRegisterAsVersioned, SplitFeaturesByField, TruncateGeodatabase, DeleteRelationships]


class Tool(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "TemplateTool"
        self.description = "Tool added by default when new python toolbox is created"
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""
        params = None
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        return

"""
## Tool to delete all domains from the specified workspace
## Vish Apte, 01-Aug-2013, Initial Version
"""
class DeleteDomains(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Delete All Domains from Workspace"
        self.description = "Delete all domains from specified workspace"
        self.canRunInBackground = False
        self.logger = Logger(1) # log level is warning

    def getParameterInfo(self):
        """Define parameter definitions"""
        parWorkspace = arcpy.Parameter(displayName="Workspace name",
                             name="parWorkspace",
                             datatype="DEWorkspace",
                             parameterType="Required",
                             direction="Input")
        params = [parWorkspace]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):

        try:
            self.logger.log("Running Delete All Domains...", self.logger.LOG_LEVEL_DEBUG);
            Input_Workspace = parameters[0].valueAsText
            self.logger.log("Workspace is '%s'." % (Input_Workspace), self.logger.LOG_LEVEL_DEBUG)
            # Local variables:
            arcpy.env.workspace = Input_Workspace
            desc = arcpy.Describe(Input_Workspace)
            # Process: Delete Domain
            domains = desc.domains
            for domainName in domains:
                self.logger.log("Deleting domain %s ..." % (domainName), self.logger.LOG_LEVEL_DEBUG)
                arcpy.DeleteDomain_management(Input_Workspace, domainName)
                self.logger.log("Deleted domain %s." % (domainName), self.logger.LOG_LEVEL_INFO)

            self.logger.log("Completed Delete All Domains.", self.logger.LOG_LEVEL_DEBUG);

        except ParameterError:
            self.logger.log("Invalid usage.", self.logger.LOG_LEVEL_ERROR)
            self.logger.log("Usage: python.exe DeleteDomains.py <""GDB Workspace"">", self.logger.LOG_LEVEL_ERROR)
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            self.logger.log("DeleteDomains failed")
            self.logger.log(exc_type, self.logger.LOG_LEVEL_ERROR)
            self.logger.log(exc_value, self.logger.LOG_LEVEL_ERROR)
            self.logger.log(exc_traceback, self.logger.LOG_LEVEL_ERROR)

        finally:
            sys.exit()



"""
## Tool to create domain using a text file that has dmain value entries.
## Code and description in the domain should be separated by a comma e.g. CI, Cast Iron
## If description is not provided, code and description are same
## If name of the domain is omitted, tool uses name of the file w/o path and extension as domain name
## If domain exists, tool optionally deletes all existing values.
## Vish Apte, 01-Aug-2013, Initial Version
"""
class CreateDomainUsingFile(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Create Coded Domain with Values from a Text File"
        self.description = "If Domain Name is not provided, file name w/o extension is used as domain name"
        self.canRunInBackground = False
        self.logger = Logger(1) # log level is warning

    def getParameterInfo(self):
        """Define parameter definitions"""
        parWorkspace = arcpy.Parameter(displayName="Workspace name",
                             name="parWorkspace",
                             datatype="DEWorkspace",
                             parameterType="Required",
                             direction="Input")
        parListFile = arcpy.Parameter(displayName="Text File with values",
                             name="parListFile",
                             datatype="DEFile",
                             parameterType="Required",
                             direction="Input")
        parDomainName = arcpy.Parameter(displayName="Domain name",
                             name="parDomainName",
                             datatype="GPString",
                             parameterType="Optional",
                             direction="Input")
        parDeleteValues = arcpy.Parameter(displayName="Delete existing values if domain exists?",
                            name="parDeleteValues",
                            datatype="GPBoolean",
                            parameterType="Optional",
                            direction="Input")
        parDeleteValues.value = True

        params = [parWorkspace, parListFile, parDomainName, parDeleteValues]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def DeleteDomainValues(self, workspace, domainName):
        self.logger.log("Deleting all values from existing '%s' domain..." % (domainName), self.logger.LOG_LEVEL_DEBUG)
        arcpy.env.workspace = workspace
        tempTableName = "TEMPDOMAINTABLE"
        if (arcpy.Exists(tempTableName)):
            arcpy.Delete_management(tempTableName)
        codeField = "CODE"
        descripField = "DESCRIPTION"
        arcpy.DomainToTable_management(workspace, domainName, tempTableName, codeField, descripField)
        ##self.logger.log("Created table '%s' from domain '%s'" % (tempTableName, domainName), self.logger.LOG_LEVEL_DEBUG)
        fullTableName = workspace + r"/" + tempTableName
        values = [row[0] for row in arcpy.da.SearchCursor(fullTableName, (codeField))]
        if (len(values) > 0):
            arcpy.DeleteCodedValueFromDomain_management(workspace, domainName, values)

        arcpy.Delete_management(tempTableName)
        self.logger.log("Deleted all values from existing '%s' domain." % (domainName), self.logger.LOG_LEVEL_INFO)

    def execute(self, parameters, messages):
        try:
            if (len(parameters) < 2):
                raise ParameterError

            parDestWorkspace = parameters[0].valueAsText
            parFileForValues = parameters[1].valueAsText
            parDomain = ""
            parDeleteValues = True
            if (not arcpy.Exists(parDestWorkspace)):
                msg = "Specified workspace parameter '%s' is invalid." % (parDestWorkspace)
                self.logger.log(msg, self.logger.LOG_LEVEL_ERROR )
                print msg
                sys.exit(-1)

            if (not os.path.exists(parFileForValues)):
                msg = "Specified domain values file '%s' does not exist." % (parFileForValues)
                self.logger.log(msg, self.logger.LOG_LEVEL_ERROR )
                print msg
                sys.exit(-1)


            if (parameters[2] is None or parameters[2].valueAsText is None):
                parDomain = os.path.splitext(os.path.basename(parFileForValues))[0]
            else:
                parDomain = parameters[2].valueAsText

            if (parDomain == "" or parDomain == "#" or parDomain == "None" or parDomain is None):
                parDomain = os.path.splitext(os.path.basename(parFileForValues))[0]

            if (parameters[3] is not None or parameters[3].valueAsText is not None):
                parDeleteValues = parameters[3].valueAsText == "true"

            self.logger.log("Workspace is '%s'." % (parDestWorkspace), self.logger.LOG_LEVEL_DEBUG)
            self.logger.log("Domain to create is '%s'." % (parDomain), self.logger.LOG_LEVEL_DEBUG)
            self.logger.log("Values file is '%s'." % (parFileForValues), self.logger.LOG_LEVEL_DEBUG)
            self.logger.log("Delete existing values is '%s'." % (parDeleteValues), self.logger.LOG_LEVEL_DEBUG)

            domainExists = False
            desc = arcpy.Describe(parDestWorkspace)
            for currDomain in desc.domains:
                if (parDomain.lower() == currDomain.lower()):
                    domainExists = True
                    break
            if (domainExists == True):
                if (parDeleteValues == True):
                    self.DeleteDomainValues(parDestWorkspace, parDomain)
            else:
                arcpy.CreateDomain_management(parDestWorkspace, parDomain, parDomain, "TEXT","CODED","DEFAULT","DEFAULT")
                self.logger.log("Created domain '%s'." % (parDomain), self.logger.LOG_LEVEL_INFO)

            f = open(parFileForValues, 'r')
            lines = f.readlines()
            f.close()

            ## Add values to domain
            for line in lines:
                codeEntry = line.rstrip('\n') ## strip new line character
                if (codeEntry != ""):
                    codeValues = codeEntry.split(",", 1)
                    codeDescrip = codeValue = codeValues[0]
                    if (len(codeValues) > 1):
                        codeDescrip = codeValues[1]

                    arcpy.AddCodedValueToDomain_management(parDestWorkspace, parDomain, codeValue, codeDescrip)
                    self.logger.log("Added code '%s'." % (codeValue), self.logger.LOG_LEVEL_INFO)

            self.logger.log( "'%s' domain is built sucessfully." % (parDomain), self.logger.LOG_LEVEL_INFO)

            sys.exit(0)

        except ParameterError:
            self.logger.log("Invalid parameter encountered.\nUsage: CreateDomainUsingFile <Input_Workspace> <File Name> [Domain Name]", self.logger.LOG_LEVEL_ERROR)
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            msg = repr(traceback.format_exception(exc_type, exc_value, exc_traceback))
            self.logger.log(msg, self.logger.LOG_LEVEL_ERROR)
            print msg
        finally:
            sys.exit()

"""
## Tool to upper case all fields in the specified feature class or dataset in the workspace
## Also, generates description by remove special characters
## Vish Apte, 01-Aug-2013, Initial Version
"""
class UpperCaseFieldNames(object):

    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Title case field descriptions and optionally upper case field names"
        self.description = "Title case field descriptions and optionally upper case field names in specified feature classes. Omit system fields from any modification."
        self.canRunInBackground = False
        self.logger = Logger(3) # log level is warning
        self.prohibitedFields = ["OBJECTID", "SHAPE", "GLOBALID", "REL_GLOBALID", "LAST_EDITED_USER", "LAST_EDITED_DATE", "CREATED_USER", "CREATED_DATE"]
        self.annotationFields = ["FeatureID", "ZOrder", "AnnotationClassID", "", "Element", "SymbolID", "Status", "TextString", "FontName", "FontSize", "Bold", "Italic", "Underline", "VerticalAlignment", "HorizontalAlignment", "XOffset", "YOffset", "Angle", "FontLeading", "WordSpacing", "CharacterWidth", "CharacterSpacing", "FlipAngle", "Override" ]

    def getParameterInfo(self):
        """Define parameter definitions"""
        parWorkspace = arcpy.Parameter(displayName="Workspace name",
                             name="parWorkspace",
                             datatype="DEWorkspace",
                             parameterType="Required",
                             direction="Input")

        parUpdateFieldNames = arcpy.Parameter(displayName="Upper case field names?",
                             name="parUpdateFieldName",
                             datatype="GPBoolean",
                             parameterType="Required",
                             direction="Input")

        parDataset = arcpy.Parameter(displayName="Dataset name",
                             name="parDataset",
                             datatype="GPString",
                             parameterType="Optional",
                             direction="Input")

        parWildcard = arcpy.Parameter(displayName="Wildcard for features",
                             name="parWildcard",
                             datatype="GPString",
                             parameterType="Optional",
                             direction="Input")

        parFeatureType = arcpy.Parameter(displayName="Feature type",
                             name="parFeatureType",
                             datatype="GPString",
                             parameterType="Optional",
                             direction="Input")

        ##parFeatureType.multiValue = False
        parFeatureType.filter.type = "ValueList"
        parFeatureType.filter.list = ["All", "Arc", "Annotations", "Dimension", "Edge", "Junction",
                                        "Label", "Line", "Multipatch", "Node", "Point", "Polygon",
                                         "Polyline", "Region", "Route", "Tic"]


        parRecursive = arcpy.Parameter(displayName="Retrieve feature classes recursively?",
                             name="parRecursive",
                             datatype="GPBoolean",
                             parameterType="Optional",
                             direction="Input")



        params = [parWorkspace, parUpdateFieldNames, parDataset, parWildcard, parFeatureType, parRecursive]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return


    def updateField(self, parUpdateFieldName, fullFeatureClass):

        stepCounter = 1
        featureFields = arcpy.ListFields(fullFeatureClass, "*", "All")
        for featureField in featureFields:
            if (featureField.name in self.prohibitedFields or featureField.name in self.annotationFields):
                self.logger.log("Cannot modify system field '%s' with alias '%s'." % (featureField.name, featureField.aliasName), self.logger.LOG_LEVEL_DEBUG)
            else:
##            if (featureField.name.upper().startswith("SHAPE") == False and featureField.name.upper().startswith("OBJECTID") == False
##                and featureField.name.upper().startswith("ATTACHMENTID") == False
##                and featureField.name.upper().startswith("GLOBALID") == False and featureField.name.upper().startswith("REL_GLOBALID") == False
##                and featureField.name.upper().startswith("LAST_EDITED_USER") == False and featureField.name.upper().startswith("LAST_EDITED_DATE") == False
##                and featureField.name.upper().startswith("CREATED_USER") == False and featureField.name.upper().startswith("CREATED_DATE") == False) :

                self.logger.log("Processing '%s' field with alias '%s'..." % (featureField.name, featureField.aliasName), self.logger.LOG_LEVEL_DEBUG)
                try:
                    modify = False
                    if (parUpdateFieldName ):
                        if( featureField.name != featureField.name.upper()):
                            featureField.name = featureField.name.upper()
                            modify = True

                    if (featureField.aliasName == featureField.name.upper() or featureField.aliasName.replace(" ", "_") == featureField.name.upper()):
                        modify = True
                        featureField.aliasName = featureField.aliasName.replace("_", " ")
                        featureField.aliasName = featureField.aliasName.title()

                    if (modify):
                        self.logger.log("Modifying '%s' field ..." % (featureField.name), self.logger.LOG_LEVEL_DEBUG)
                        arcpy.DeleteField_management(fullFeatureClass, featureField.name)
                        arcpy.AddField_management(fullFeatureClass, featureField.name, featureField.type, featureField.precision, featureField.scale, featureField.length, featureField.aliasName, "NULLABLE", "NON_REQUIRED", featureField.domain)
                        self.logger.log("Modified '%s' field." % (featureField.name), self.logger.LOG_LEVEL_INFO)

                except arcpy.ExecuteError:
                    strErrMsg = arcpy.GetMessages(2)
                    self.logger.log("Failed to update '%s' field in '%s' feature class: %s" % (featureField.name, fullFeatureClass, strErrMsg), self.logger.LOG_LEVEL_ERROR)
                except:
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    strErrMsg = "Unknown error occurred: %s-%s " % (exc_type, exc_traceback)
                    self.logger.log("Failed to update '%s' field in '%s' feature class: %s" % (featureField.name, fullFeatureClass, strErrMsg), self.logger.LOG_LEVEL_ERROR)

                self.logger.log("Processed '%s' field." % (featureField.name), self.logger.LOG_LEVEL_INFO)

    def UpdateFieldsInDataset(self, parDestWorkspace, parUpdateFieldName, parWildcard, parFeatureType, parDataset):
        stepCounter = 1
        # go thru all matching feature classes
        featureClasses = arcpy.ListFeatureClasses(parWildcard, parFeatureType, parDataset)
        for featureClass in featureClasses:
            #Loop each feature class
            #self.logger.log("Upper casing fields in '%s' feature class with type '%s'..." % (featureClass, desc.featureType), self.logger.LOG_LEVEL_DEBUG)

            featureClass = featureClass.upper()
            className = featureClass.split('.')
            if (len(className) > 1):
                featureClass = className[len(className)-1]

            if (parDataset == ""):
                fullFeatureClass = path.join(parDestWorkspace, featureClass)
            else:
                fullFeatureClass = path.join(parDestWorkspace, parDataset, featureClass)

            desc = arcpy.Describe(fullFeatureClass)
            if (desc.featureType.upper() != "ANNOTATION" or parFeatureType == "All"):
                self.logger.log("Upper casing fields in '%s' feature class with type '%s'..." % (featureClass, desc.featureType), self.logger.LOG_LEVEL_DEBUG)
                self.updateField(parUpdateFieldName, fullFeatureClass)
                self.logger.log("Upper cased fields in '%s' feature class." % (featureClass), self.logger.LOG_LEVEL_INFO)

        # go thru all matching tables
        tables = arcpy.ListTables(parWildcard, "All")
        for table in tables:
            #Loop each feature class
            self.logger.log("Upper casing fields in '%s' table..." % (table), self.logger.LOG_LEVEL_DEBUG)
            table = table.upper()
            tableName = table.split('.')
            if (len(tableName) > 1):
                table = tableName[len(tableName)-1]
            fullTable = path.join(parDestWorkspace, table)
            self.updateField(parUpdateFieldName, fullTable)
            self.logger.log("Upper cased fields in '%s' table." % (table), self.logger.LOG_LEVEL_INFO)

    def execute(self, parameters, messages):
        parDestWorkspace = ""
        parUpdateFieldName = ""
        parWildcard = "*"
        parFeatureType = "All"
        parDataset = ""
        parRecursive = False
        stepCounter = 1

        try:
            parDestWorkspace = parameters[0].valueAsText

            parUpdateFieldName = parameters[1].valueAsText == "true"

            if (parameters[2] is not None and parameters[2].valueAsText is not None):
                parDataset = parameters[2].valueAsText

            if (parameters[3] is not None and parameters[3].valueAsText is not None):
                parWildcard = parameters[3].valueAsText

            if (parameters[4] is not None and parameters[4].valueAsText is not None):
                parFeatureType = parameters[4].valueAsText

            if (parameters[5] is not None and parameters[5].valueAsText is not None):
                parRecursive = parameters[5].valueAsText == "true"

            self.logger.log("Destination workspace is '%s'" % (parDestWorkspace), self.logger.LOG_LEVEL_DEBUG)
            self.logger.log("Update field name is set to '%s'" % (parUpdateFieldName), self.logger.LOG_LEVEL_DEBUG)
            self.logger.log("Dateset is '%s'" % (parDataset), self.logger.LOG_LEVEL_DEBUG)
            self.logger.log("Feature class filter is '%s'" % (parWildcard), self.logger.LOG_LEVEL_DEBUG)
            self.logger.log("Feature class type is '%s'" % (parFeatureType), self.logger.LOG_LEVEL_DEBUG)
            self.logger.log("Recursion is '%s'" % (parRecursive), self.logger.LOG_LEVEL_DEBUG)


            if (arcpy.Exists(parDestWorkspace)):
                self.logger.log("Destination workspace '%s' is valid." % (parDestWorkspace), self.logger.LOG_LEVEL_DEBUG)
            else:
                self.logger.log("Destination workspace parameter '%s' is invalid." % (parDestWorkspace), self.logger.LOG_LEVEL_ERROR)
                raise ParameterError

            env.workspace = parDestWorkspace
            # List all feature classes in Landbase dataset
            #wildcard = "All"

            self.UpdateFieldsInDataset(parDestWorkspace, parUpdateFieldName, parWildcard, parFeatureType, parDataset)

            if (parDataset == "" and parRecursive):
                ## get all datasets
                datasets = arcpy.ListDatasets()
                for dataset in datasets:
                    self.UpdateFieldsInDataset(parDestWorkspace, parUpdateFieldName, parWildcard, parFeatureType, dataset)

            self.logger.log( "Script completed.", self.logger.LOG_LEVEL_INFO)
        except ParameterError:
            self.logger.log("Invalid parameter encountered.\nUsage: UpperCaseFieldNames <Input Workspace> [Dataset] [Wildcard] [Feature Type] [Recursive]", self.logger.LOG_LEVEL_ERROR)
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            msg = repr(traceback.format_exception(exc_type, exc_value, exc_traceback))
            self.logger.log(msg, self.logger.LOG_LEVEL_ERROR)
        finally:
            sys.exit()

"""
## Tool to empty the workspace or feature dataset
## This tool deletes all feature classes and tables from the specified dataset
## Optionally, it deletes all the domains if specified dataset is a workspace
## Vish Apte, 01-Aug-2013, Initial Version
"""
class DeleteAllDatasets(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Delete Datasets"
        self.description = "Delete datasets recursively"
        self.canRunInBackground = False
        self.logger = Logger(1) # log level is warning

    def getParameterInfo(self):
        """Define parameter definitions"""
        parWorkspace = arcpy.Parameter(displayName="Dataset or Workspace",
                             name="parWorkspace",
                             datatype=["DEDatasetType", "DEWorkspace"],
                             parameterType="Required",
                             direction="Input")
##        parDataset = arcpy.Parameter(displayName="Dataset name",
##                             name="parDataset",
##                             datatype="GPString",
##                             parameterType="Optional",
##                             direction="Input")
        parDeleteDomain = arcpy.Parameter(displayName="Delete all domains as well?",
                             name="parDeleteDomain",
                             datatype="GPBoolean",
                             parameterType="Optional",
                             direction="Input")
        parDeleteDomain.value = False
        params = [parWorkspace, parDeleteDomain]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    """
    # delete feature classes and tables in the dataset
    """
    def deleteFeatureAndTables(self, dataset):
        if (arcpy.Exists(dataset)):
            try:
                env.workspace = dataset
                featureClasses = arcpy.ListFeatureClasses()
                for featureClass in featureClasses:
                    #Loop each feature class
                    featureClass = featureClass.upper()
                    className = featureClass.split('.')
                    if (len(className) > 1):
                        featureClass = className[len(className)-1]

                    fullFeatureClass = path.join(dataset, featureClass)

                    self.logger.log("Deleting '%s' feature class..." % (featureClass), self.logger.LOG_LEVEL_DEBUG)
                    arcpy.Delete_management(fullFeatureClass)
                    self.logger.log("Deleted '%s' feature class." % (featureClass), self.logger.LOG_LEVEL_INFO)

                tables = arcpy.ListTables()
                for table in tables:
                    # loop each table
                    self.logger.log("Deleting '%s' table..." % (table), self.logger.LOG_LEVEL_DEBUG)
                    table = table.upper()
                    tableName = table.split('.')
                    if (len(tableName) > 1):
                        table = tableName[len(tableName)-1]
                    fullTable = path.join(dataset, table)
                    arcpy.Delete_management(fullTable)
                    self.logger.log("Deleted '%s' table." % (table), self.logger.LOG_LEVEL_INFO)

            except:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                msg = repr(traceback.format_exception(exc_type, exc_value, exc_traceback))
                self.logger.log(msg, self.logger.LOG_LEVEL_ERROR)

    def execute(self, parameters, messages):
        try:

            parDestWorkspace = ""
            parDataset = ""
            parDeleteDomains = False

            parDestWorkspace = parameters[0].valueAsText

            if (parameters[1] is not None and parameters[1].valueAsText is not None):
                parDeleteDomains = parameters[1].valueAsText == "true"

            self.logger.log("Destimation dataset is '%s'" % (parDestWorkspace), self.logger.LOG_LEVEL_DEBUG)
            self.logger.log("Delete domains is '%s'" % (parDeleteDomains), self.logger.LOG_LEVEL_DEBUG)


            if (arcpy.Exists(parDestWorkspace)):
                self.logger.log("Destination workspace '%s' is valid." % (parDestWorkspace), self.logger.LOG_LEVEL_DEBUG)
            else:
                self.logger.log("Destination workspace parameter '%s' is invalid." % (parDestWorkspace), self.logger.LOG_LEVEL_ERROR)
                raise ParameterError
                sys.exit(-1)

            desc = arcpy.Describe(parDestWorkspace)
            datasetType = desc.dataType
            self.logger.log("Dataset type is '%s'" %(datasetType), self.logger.LOG_LEVEL_DEBUG)

            if (datasetType == "FeatureDataset"):
                # feature dataset is selected
                env.workspace = parDestWorkspace
                self.deleteFeatureAndTables(parDestWorkspace)

            if (datasetType == "Workspace"):
                # workspace is selected
                env.workspace =  parDestWorkspace

                datasets = arcpy.ListDatasets()
                for dataset in datasets:
                    self.logger.log("Deleting dataset '%s'..." % (dataset), self.logger.LOG_LEVEL_DEBUG)
                    fullDatasetName = path.join(parDestWorkspace, dataset)
                    env.workspace = fullDatasetName
                    self.deleteFeatureAndTables(fullDatasetName)
                    arcpy.Delete_management(fullDatasetName)
                    self.logger.log("Deleted dataset '%s'." % (dataset), self.logger.LOG_LEVEL_INFO)

                # now delete root level feature classes and tables
                self.deleteFeatureAndTables(parDestWorkspace)

                # set the workspace back
                env.workspace =  parDestWorkspace
                if (parDeleteDomains == True):
                    # delete domains as well
                    desc = arcpy.Describe(parDestWorkspace)
                    # Process: Delete Domain
                    domains = desc.domains
                    for domainName in domains:
                        self.logger.log("Deleting domain %s ..." % (domainName), self.logger.LOG_LEVEL_DEBUG)
                        arcpy.DeleteDomain_management(parDestWorkspace, domainName)
                        self.logger.log("Deleted domain %s." % (domainName), self.logger.LOG_LEVEL_INFO)

            self.logger.log( "Script completed.", self.logger.LOG_LEVEL_INFO)
        except ParameterError:
            self.logger.log("Invalid parameter encountered.\nUsage: DeleteAllDatasets <Input Workspace | Dataset> [Delete domains]", self.logger.LOG_LEVEL_ERROR)
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            msg = repr(traceback.format_exception(exc_type, exc_value, exc_traceback))
            self.logger.log(msg, self.logger.LOG_LEVEL_ERROR)
            print msg
        finally:
            sys.exit()


"""
## Tool to copy datasets from one GDB to another
## This tool will copy worksapce contents from one workspace to another
## Existing datasets or feature classes from destination worksapce are skipped
## Optionally, it can copy a specific dataset from source workspace
## Vish Apte, 01-Sep-2011, Initial Version
"""
class CopyDatasets(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Copy Datasets"
        self.description = "Copy data classes recursively"
        self.canRunInBackground = False
        self.logger = Logger(3) # log level is debug

    def getParameterInfo(self):
        """Define parameter definitions"""
        parSrcWorkspace = arcpy.Parameter(displayName="Source Workspace",
                             name="parSrcWorkspace",
                             datatype=["DEDatasetType", "DEWorkspace"],
                             parameterType="Required",
                             direction="Input")
        parDestWorkspace = arcpy.Parameter(displayName="Destination Workspace",
                             name="parDestWorkspace",
                             datatype=["DEDatasetType", "DEWorkspace"],
                             parameterType="Required",
                             direction="Input")
        parDataset = arcpy.Parameter(displayName="Dataset name",
                             name="parDataset",
                             datatype="GPString",
                             parameterType="Optional",
                             direction="Input")


        params = [parSrcWorkspace, parDestWorkspace, parDataset]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return



    def execute(self, parameters, messages):
        logger = self.logger
        try:

            if (len(parameters) < 2):
                raise ParameterError

            #source workspace
            wspaceSrc = parameters[0].valueAsText

            # Local variables:
            wspaceDest = parameters[1].valueAsText

            # dataset to copy, optional...
            datasetSrc = ""
            if (len(parameters) > 2):
                datasetSrc = parameters[2].valueAsText

            if (not arcpy.Exists(wspaceSrc)):
                raise ValueError("Workspace '%s' does not exist or is invalid." %(wspaceSrc))

            if (not arcpy.Exists(wspaceDest)):
                raise ValueError("Workspace '%s' does not exist or is invalid." %(wspaceDest))

            arcpy.env.workspace = wspaceSrc

            datasetSrcFullPath = ""
            datasetDestFullPath = ""
            hasDS = False
            if (datasetSrc is not None and len(datasetSrc) > 0):
                hasDS = True
                datasetSrcFullPath = os.path.join(wspaceSrc, datasetSrc)
                datasetDestFullPath = os.path.join(wspaceDest, datasetSrc)
                if (not arcpy.Exists(datasetSrcFullPath)):
                    hasDS = False
                    raise ValueError("Dataset '%s' does not exist or is invalid." %(datasetSrcFullPath))
                if (arcpy.Exists(datasetDestFullPath)):
                    raise ValueError("Dataset '%s' already exists and cannot be copied." %(datasetDestFullPath))


            # do work

            if (hasDS):
                # specific dataset to be copied
                logger.log("Copying '%s' to '%s'..." %(datasetSrcFullPath, datasetDestFullPath), logger.LOG_LEVEL_DEBUG)
                arcpy.Copy_management(in_data=datasetSrcFullPath, out_data=datasetDestFullPath, data_type="Datasets")
                logger.log("Copyied '%s' to '%s'..." %(datasetSrcFullPath, datasetDestFullPath), logger.LOG_LEVEL_INFO)
            else:
                # copy datasets
                for ds in arcpy.ListDatasets("*", "All"):
                    logger.log("Processing dataset '%s' in '%s'..." %(ds, wspaceSrc), logger.LOG_LEVEL_DEBUG)
                    datasetSrcFullPath = os.path.join(wspaceSrc, ds)
                    datasetDestFullPath = os.path.join(wspaceDest, ds)
                    if (arcpy.Exists(datasetDestFullPath)):
                        logger.log("Dataset '%s' already exists and cannot be copied. Skipping this feature dataset." %(datasetSrcFullPath), logger.LOG_LEVEL_ERROR)
                    else:
                        logger.log("Copying '%s' to '%s'..." %(datasetSrcFullPath, datasetDestFullPath), logger.LOG_LEVEL_DEBUG)
                        arcpy.Copy_management(in_data=datasetSrcFullPath, out_data=datasetDestFullPath, data_type="Datasets")
                        logger.log("Copied '%s' to '%s'..." %(datasetSrcFullPath, datasetDestFullPath), logger.LOG_LEVEL_INFO)
                # copy feature classes
                for fc in arcpy.ListFeatureClasses("*", "All"):
                    datasetSrcFullPath = os.path.join(wspaceSrc, fc)
                    datasetDestFullPath = os.path.join(wspaceDest, fc)
                    if (arcpy.Exists(datasetDestFullPath)):
                        logger.log("Feature class '%s' already exists and cannot be copied. Skipping this feature class." %(datasetSrcFullPath), logger.LOG_LEVEL_ERROR)
                    else:
                        logger.log("Copying '%s' to '%s'..." %(datasetSrcFullPath, datasetDestFullPath), logger.LOG_LEVEL_DEBUG)
                        arcpy.Copy_management(in_data=datasetSrcFullPath, out_data=datasetDestFullPath, data_type="Datasets")
                        logger.log("Copied '%s' to '%s'..." %(datasetSrcFullPath, datasetDestFullPath), logger.LOG_LEVEL_INFO)
                # copy tables
                for tab in arcpy.ListTables("*", "All"):
                    datasetSrcFullPath = os.path.join(wspaceSrc, tab)
                    datasetDestFullPath = os.path.join(wspaceDest, tab)
                    if (arcpy.Exists(datasetDestFullPath)):
                        logger.log("Feature table '%s' already exists and cannot be copied. Skipping this table." %(datasetSrcFullPath), logger.LOG_LEVEL_ERROR)
                    else:
                        logger.log("Copying '%s' to '%s'..." %(datasetSrcFullPath, datasetDestFullPath), logger.LOG_LEVEL_DEBUG)
                        arcpy.Copy_management(in_data=datasetSrcFullPath, out_data=datasetDestFullPath, data_type="Datasets")
                        logger.log("Copied '%s' to '%s'..." %(datasetSrcFullPath, datasetDestFullPath), logger.LOG_LEVEL_INFO)

        except ParameterError:
            logger.log("Invalid usage.", logger.LOG_LEVEL_ERROR)
            logger.log("Usage: python.exe CopyDatasets <""Source GDB Workspace""> <""Destincation GDB Workspace""> [""Dataset Name""]", logger.LOG_LEVEL_ERROR)
        except ValueError as e:
            logger.log("Parameter error.", logger.LOG_LEVEL_ERROR)
            logger.log(str(e), logger.LOG_LEVEL_ERROR)
        except Exception as e:
            error = traceback.format_exc()
            logger.log("Failed to perform CopyDatasets: %s." %(error), logger.LOG_LEVEL_ERROR)
        finally:
            sys.exit()


"""
## Tool to Register all datasets in an enterprise geodatabase as versioned
## Datasets, tables and feature classes that are already versioned will be skipped.
## Vish Apte, 01-Sep-2011, Initial Version
"""
class RegisterAsVersioned(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Register workspace as versioned"
        self.description = "Register all datasets in enterprise gdb as versioned"
        self.canRunInBackground = False
        self.logger = Logger(3) # log level is debug

    def getParameterInfo(self):
        """Define parameter definitions"""
        parSrcWorkspace = arcpy.Parameter(displayName="Source Workspace",
                             name="parSrcWorkspace",
                             datatype=["DEDatasetType", "DEWorkspace"],
                             parameterType="Required",
                             direction="Input")
        parDataset = arcpy.Parameter(displayName="Dataset name",
                             name="parDataset",
                             datatype="GPString",
                             parameterType="Optional",
                             direction="Input")


        params = [parSrcWorkspace, parDataset]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return



    def execute(self, parameters, messages):
        logger = self.logger
        try:

            if (len(parameters) < 2):
                raise ParameterError

            #source workspace
            wspaceSrc = parameters[0].valueAsText

            # dataset to copy, optional...
            hasDS = False
            datasetSrc = ""
            datasetSrcFullPath = ""

            if (len(parameters) > 1):
                datasetSrc = parameters[1].valueAsText
                if (datasetSrc != None and datasetSrc != '' and len(datasetSrc) > 0):
                    hasDS = True


            if (not arcpy.Exists(wspaceSrc)):
                raise ValueError("Workspace '%s' does not exist or is invalid." %(wspaceSrc))
            else:
                desc = arcpy.Describe(wspaceSrc)
                logger.log("Workspace workspace type is '%s'"%(desc.workspaceType), logger.LOG_LEVEL_DEBUG)
                #if (desc.workspaceType == None or desc.connectionString == "" or len(desc.connectionString) == 0):
                if (desc.workspaceType != "RemoteDatabase"):
                    raise ValueError("Workspace '%s' is not SDE workspace." %(wspaceSrc))

            if (hasDS):
                datasetSrcFullPath = os.path.join(wspaceSrc, datasetSrc)
                if (not arcpy.Exists(datasetSrcFullPath)):
                    raise ValueError("Dataset '%s' does not exist or is invalid." %(datasetSrcFullPath))

                    desc = arcpy.Describe(datasetSrcFullPath)
                    if (desc.canVersion == False):
                        raise ValueError("Dataset '%s' cannot be versioned." %(datasetSrcFullPath))
                    elif (desc.isVersioned == True):
                        raise ValueError("Dataset '%s' is already versioned." %(datasetSrcFullPath))

            arcpy.env.workspace = wspaceSrc

            baseEdits = "EDITS_TO_BASE"
            # do work
            registerVersion = RegisterGeodatabaseObjects(wspaceSrc, logger)
            if (hasDS):
                # specific dataset to be versioned
                registerVersion.RegisterDataset(datasetSrc, baseEdits)
            else:
                # copy datasets
                for ds in arcpy.ListDatasets("*", "All"):
                    logger.log("Processing dataset '%s' in '%s'..." %(ds, wspaceSrc), logger.LOG_LEVEL_DEBUG)
                    registerVersion.RegisterDataset(ds, baseEdits)

                # copy feature classes
                for fc in arcpy.ListFeatureClasses("*", "All"):
                    logger.log("Processing feature class '%s' in '%s'..." %(fc, wspaceSrc), logger.LOG_LEVEL_DEBUG)
                    registerVersion.RegisterDataset(fc, baseEdits)

                # copy tables
                for tab in arcpy.ListTables("*", "All"):
                    logger.log("Processing feature table '%s' in '%s'..." %(tab, wspaceSrc), logger.LOG_LEVEL_DEBUG)
                    registerVersion.RegisterDataset(tab, baseEdits)

        except ParameterError:
            logger.log("Invalid usage.", logger.LOG_LEVEL_ERROR)
            logger.log("Usage: python.exe CopyDatasets <""Source GDB Workspace""> <""Destincation GDB Workspace""> [""Dataset Name""]", logger.LOG_LEVEL_ERROR)
        except ValueError as e:
            logger.log("Parameter error.", logger.LOG_LEVEL_ERROR)
            logger.log(str(e), logger.LOG_LEVEL_ERROR)
        except Exception as e:
            error = traceback.format_exc()
            logger.log("Failed to perform CopyDatasets: %s." %(error), logger.LOG_LEVEL_ERROR)
        finally:
            sys.exit()


"""
## Tool to Register all datasets in an enterprise geodatabase as versioned
## Datasets, tables and feature classes that are already versioned will be skipped.
## Vish Apte, 01-Sep-2011, Initial Version
"""
class UnRegisterAsVersioned(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Un-register workspace from versioned"
        self.description = "Unregister all datasets in enterprise gdb from versioned"
        self.canRunInBackground = False
        self.logger = Logger(3) # log level is debug

    def getParameterInfo(self):
        """Define parameter definitions"""
        parSrcWorkspace = arcpy.Parameter(displayName="Source Workspace",
                             name="parSrcWorkspace",
                             datatype=["DEDatasetType", "DEWorkspace"],
                             parameterType="Required",
                             direction="Input")
        parDataset = arcpy.Parameter(displayName="Dataset name",
                             name="parDataset",
                             datatype="GPString",
                             parameterType="Optional",
                             direction="Input")


        params = [parSrcWorkspace, parDataset]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return



    def execute(self, parameters, messages):
        logger = self.logger
        try:

            if (len(parameters) < 2):
                raise ParameterError

            #source workspace
            wspaceSrc = parameters[0].valueAsText

            # dataset to copy, optional...
            hasDS = False
            datasetSrc = ""
            datasetSrcFullPath = ""

            if (len(parameters) > 1):
                datasetSrc = parameters[1].valueAsText
                if (datasetSrc != None and datasetSrc != '' and len(datasetSrc) > 0):
                    hasDS = True


            if (not arcpy.Exists(wspaceSrc)):
                raise ValueError("Workspace '%s' does not exist or is invalid." %(wspaceSrc))
            else:
                desc = arcpy.Describe(wspaceSrc)
                logger.log("Workspace workspace type is '%s'"%(desc.workspaceType), logger.LOG_LEVEL_DEBUG)
                #if (desc.workspaceType == None or desc.connectionString == "" or len(desc.connectionString) == 0):
                if (desc.workspaceType != "RemoteDatabase"):
                    raise ValueError("Workspace '%s' is not SDE workspace." %(wspaceSrc))

            if (hasDS):
                datasetSrcFullPath = os.path.join(wspaceSrc, datasetSrc)
                if (not arcpy.Exists(datasetSrcFullPath)):
                    raise ValueError("Dataset '%s' does not exist or is invalid." %(datasetSrcFullPath))

                    desc = arcpy.Describe(datasetSrcFullPath)
                    if (desc.canVersion == False):
                        raise ValueError("Dataset '%s' cannot be versioned." %(datasetSrcFullPath))
                    elif (desc.isVersioned == True):
                        raise ValueError("Dataset '%s' is already versioned." %(datasetSrcFullPath))

            arcpy.env.workspace = wspaceSrc

            keepEdit = "KEEP_EDIT"
            compressDefault = "NO_COMPRESS_DEFAULT"
            # do work
            registerVersion = RegisterGeodatabaseObjects(wspaceSrc, logger)
            if (hasDS):
                # specific dataset to be versioned
                registerVersion.UnRegisterDataset(datasetSrc, keepEdit, compressDefault)
            else:
                # copy datasets
                for ds in arcpy.ListDatasets("*", "All"):
                    logger.log("Processing dataset '%s' in '%s'..." %(ds, wspaceSrc), logger.LOG_LEVEL_DEBUG)
                    registerVersion.UnRegisterDataset(ds, keepEdit, compressDefault)

                # copy feature classes
                for fc in arcpy.ListFeatureClasses("*", "All"):
                    logger.log("Processing feature class '%s' in '%s'..." %(fc, wspaceSrc), logger.LOG_LEVEL_DEBUG)
                    registerVersion.UnRegisterDataset(fc, keepEdit, compressDefault)

                # copy tables
                for tab in arcpy.ListTables("*", "All"):
                    logger.log("Processing feature table '%s' in '%s'..." %(tab, wspaceSrc), logger.LOG_LEVEL_DEBUG)
                    registerVersion.UnRegisterDataset(tab, keepEdit, compressDefault)

        except ParameterError:
            logger.log("Invalid usage.", logger.LOG_LEVEL_ERROR)
            logger.log("Usage: python.exe CopyDatasets <""Source GDB Workspace""> <""Destincation GDB Workspace""> [""Dataset Name""]", logger.LOG_LEVEL_ERROR)
        except ValueError as e:
            logger.log("Parameter error.", logger.LOG_LEVEL_ERROR)
            logger.log(str(e), logger.LOG_LEVEL_ERROR)
        except Exception as e:
            error = traceback.format_exc()
            logger.log("Failed to perform CopyDatasets: %s." %(error), logger.LOG_LEVEL_ERROR)
        finally:
            sys.exit()

class SplitFeaturesByField(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Split features using unique value"
        self.description = "Split features into different classes using unique values from a field. Multiple feature classes can be provided using comma separator."
        self.canRunInBackground = False
        self.logger = Logger(3) # log level is debug

    def getParameterInfo(self):
        """Define parameter definitions"""
        parSrcWorkspace = arcpy.Parameter(displayName="Source Workspace",
                             name="parSrcWorkspace",
                             datatype=["DEDatasetType", "DEWorkspace"],
                             parameterType="Required",
                             direction="Input")
        parFeatures = arcpy.Parameter(displayName="Feature Class(es) to split.",
                             name="parFeatures",
                             datatype="GPString",
                             parameterType="Required",
                             direction="Input")
        parFields = arcpy.Parameter(displayName="Field(s) to split using.",
                             name="parFields",
                             datatype="GPString",
                             parameterType="Required",
                             direction="Input")
        parDestWorkspace = arcpy.Parameter(displayName="Destination worksapce or dataset to save split feature classes into.",
                             name="parDestWorkspace",
                             datatype=["DEDatasetType", "DEWorkspace"],
                             parameterType="Required",
                             direction="Input")
        parOverwrite = arcpy.Parameter(displayName="Overwrite existing feature classes in destination dataset?",
                             name="parOverwrite",
                             datatype="GPBoolean",
                             parameterType="Required",
                             direction="Input")

        params = [parSrcWorkspace, parFeatures, parFields, parDestWorkspace, parOverwrite]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def splitFeatures(self, wspaceSrc, fcToSplit, fldDistinct, datasetDest, overwrite):
        logger = self.logger
        fqfcToSplit = os.path.join(wspaceSrc, fcToSplit)
        if (arcpy.Exists(fqfcToSplit)):
            values = [row[0] for row in arcpy.da.SearchCursor(in_table=fqfcToSplit, field_names=(fldDistinct))]
            uniqueValues = set(values)
            arcpy.env.workspace = wspaceSrc
            for uniqueValue in uniqueValues:

                fcNameUnique = "%s_%s" %(fcToSplit, uniqueValue.replace(" ", ""))
                fcNameUnique = fcNameUnique.upper()
                whereClause = "%s='%s'" %(fldDistinct, uniqueValue)
                fqfcNew = os.path.join(datasetDest, fcNameUnique)
                skip = False
                if (arcpy.Exists(fqfcNew)):
                    if (overwrite):
                        logger.log("Feature class '%s' already exists and will be overwritten." %(fqfcNew), logger.LOG_LEVEL_WARNING)
                        arcpy.Delete_management(fqfcNew)
                    else:
                        skip = True
                        logger.log("Feature class '%s' already exists. Skipping creation of this feature class." %(fqfcNew), logger.LOG_LEVEL_WARNING)

                if (skip == False):
                    arcpy.FeatureClassToFeatureClass_conversion(in_features=fqfcToSplit, out_path=datasetDest, out_name=fcNameUnique, where_clause=whereClause)
                    if (arcpy.Exists(fqfcNew)):
                        logger.log("Feature class '%s' created." %(fqfcNew), logger.LOG_LEVEL_DEBUG)


        else:
            logger.log("Feature class '%s' does not exist." %(fqfcToSplit), logger.LOG_LEVEL_ERROR)




    def execute(self, parameters, messages):
        logger = self.logger
        try:

            wspaceSrc = parameters[0].valueAsText
            featureClasses = (parameters[1].valueAsText).split(',')
            uniqueFields = (parameters[2].valueAsText).split(',')
            wspaceDest = parameters[3].valueAsText
            boolOverwrite = parameters[4].valueAsText

            # validate input
            hasError = False
            if (not arcpy.Exists(wspaceSrc)):
                hasError = True
                logger.log("Workspace '%s' specified in parameter 1 does not exist." %(wspaceSrc), logger.LOG_LEVEL_ERROR)


            if (not arcpy.Exists(wspaceDest)):
                hasError = True
                logger.log("Worspace '%s' specified in parameter 4 does not exist." %(wspaceDest), logger.LOG_LEVEL_ERROR)

            if (len(featureClasses) != len(uniqueFields)):
                hasError = True
                logger.log("Number of feature classes '%s' specified in parameter 2 does not match number of fields '%s' specified in parameter 3." %(featureClasses, uniqueFields), logger.LOG_LEVEL_ERROR)

            if (hasError):
                sys.exit()

            i = 0
            while i < len(featureClasses):
                featureClass = str(featureClasses[i]).strip()
                uniqueField = str(uniqueFields[i]).strip()
                try:
                    self.splitFeatures(wspaceSrc, featureClass, uniqueField, wspaceDest, boolOverwrite)
                except Exception as e:
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    logger.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), logger.LOG_LEVEL_ERROR)
                i += 1
        except Exception as e:
            error = traceback.format_exc()
            logger.log("Failed to perform SplitFeatures: %s." %(error), logger.LOG_LEVEL_ERROR)
        finally:
            sys.exit()


"""
## Tool to truncate all feature classes within a workspace or dataset.
## Vish Apte, 01-Sep-2015, Initial Version
"""
class TruncateGeodatabase(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Truncate all feature classes and tables recursively from the source workspace"
        self.description = "Truncate all feature classes and tables recursively from the source workspace"
        self.canRunInBackground = True
        self.logger = Logger(3) # log level is debug

    def getParameterInfo(self):
        """Define parameter definitions"""
        parSrcWorkspace = arcpy.Parameter(displayName="Source Workspace",
                             name="parSrcWorkspace",
                             datatype=["DEDatasetType", "DEWorkspace"],
                             parameterType="Required",
                             direction="Input")
        parDataset = arcpy.Parameter(displayName="Dataset name",
                             name="parDataset",
                             datatype="GPString",
                             parameterType="Optional",
                             direction="Input")


        params = [parSrcWorkspace, parDataset]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    # truncate feature classes in a dataset
    def truncateDataset(self, datasetPath):
        arcpy.env.workspace = datasetPath
        for fc in arcpy.ListFeatureClasses():
            fqfc = os.path.join(datasetPath, fc)
            arcpy.TruncateTable_management(fqfc)
            logger.log("Truncated feature class '%s'." %(fqfc), logger.LOG_LEVEL_DEBUG)

    def execute(self, parameters, messages):
        global logger
        logger = self.logger
        try:

            if (len(parameters) < 1):
                raise ParameterError

            #source workspace
            wspaceSrc = parameters[0].valueAsText

            # dataset to copy, optional...
            hasDS = False
            datasetSrc = ""
            datasetSrcFullPath = ""

            if (len(parameters) > 1):
                datasetSrc = parameters[1].valueAsText
                if (datasetSrc is not None and datasetSrc != '' and len(datasetSrc) > 0):
                    hasDS = True


            if (not arcpy.Exists(wspaceSrc)):
                raise ValueError("Workspace '%s' does not exist or is invalid." %(wspaceSrc))
            else:
                desc = arcpy.Describe(wspaceSrc)
                logger.log("Workspace workspace type is '%s'"%(desc.workspaceType), logger.LOG_LEVEL_DEBUG)

            if (hasDS):
                datasetSrcFullPath = os.path.join(wspaceSrc, datasetSrc)
                if (not arcpy.Exists(datasetSrcFullPath)):
                    raise ValueError("Dataset '%s' does not exist or is invalid." %(datasetSrcFullPath))

            arcpy.env.workspace = wspaceSrc

            if (hasDS):
                arcpy.env.workspace = datasetSrcFullPath
                logger.log("Processing dataset '%s' ..." %(datasetSrcFullPath), logger.LOG_LEVEL_DEBUG)
                self.truncateDataset(datasetSrcFullPath)
                logger.log("Processed dataset '%s'." %(datasetSrcFullPath), logger.LOG_LEVEL_DEBUG)
            else:
                # Process datasets
                for ds in arcpy.ListDatasets("*", "All"):
                    datasetSrcFullPath = os.path.join(wspaceSrc, ds)
                    logger.log("Truncating feature classes in dataset '%s' ..." %(datasetSrcFullPath), logger.LOG_LEVEL_DEBUG)
                    self.truncateDataset(datasetSrcFullPath)
                    logger.log("Truncated feature classes in '%s'." %(datasetSrcFullPath), logger.LOG_LEVEL_DEBUG)
                arcpy.env.workspace = wspaceSrc
                # Process feature classes
                for fc in arcpy.ListFeatureClasses("*", "All"):
                    datasetSrcFullPath = os.path.join(wspaceSrc, fc)
                    logger.log("Truncating feature class '%s'..." %(datasetSrcFullPath), logger.LOG_LEVEL_DEBUG)
                    arcpy.TruncateTable_management(datasetSrcFullPath)
                    logger.log("Truncated feature class '%s'." %(datasetSrcFullPath), logger.LOG_LEVEL_DEBUG)

                # Process tables
                for tab in arcpy.ListTables("*", "All"):
                    datasetSrcFullPath = os.path.join(wspaceSrc, tab)
                    logger.log("Truncating table '%s'..." %(datasetSrcFullPath), logger.LOG_LEVEL_DEBUG)
                    arcpy.TruncateTable_management(datasetSrcFullPath)
                    logger.log("Truncated table '%s'." %(datasetSrcFullPath), logger.LOG_LEVEL_DEBUG)


        except Exception as e:
            error = traceback.format_exc()
            logger.log("Failed to perform Truncate Feature Classes: %s." %(error), logger.LOG_LEVEL_ERROR)
        finally:
            sys.exit()

"""
## Tool to delete relationship classes in a workspace or feature dataset
## This tool deletes all relationship classes from the specified dataset
## Vish Apte, 24-Nov-2016, Initial Version
"""
class DeleteRelationships(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Delete Relationship Classes"
        self.description = "Delete all relationship classes in a worspace or dataset"
        self.canRunInBackground = False
        self.logger = Logger(1) # log level is warning

    def getParameterInfo(self):
        """Define parameter definitions"""
        parWorkspace = arcpy.Parameter(displayName="Dataset or Workspace",
                             name="parWorkspace",
                             datatype=["DEDatasetType", "DEWorkspace"],
                             parameterType="Required",
                             direction="Input")
        params = [parWorkspace]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    """
    # delete feature classes and tables in the dataset
    """
    def deleteRelationships(self, dataset):
        if (arcpy.Exists(dataset)):
            try:
                env.workspace = dataset
                list_rel_classes = [c.name for c in arcpy.Describe(dataset).children if c.datatype == "RelationshipClass"]
                for rel_class_name in list_rel_classes:
                    rel_full_class_name = os.path.join(dataset, rel_class_name)
                    if (arcpy.Exists(rel_full_class_name)):
                        msg = "Deleting relationship class '%s'..." %(rel_class_name)
                        self.logger.log(msg, self.logger.LOG_LEVEL_DEBUG)
                        arcpy.Delete_management(rel_full_class_name)
                        msg = "Deleted '%s' relationship class." %(rel_class_name)
                        self.logger.log(msg, self.logger.LOG_LEVEL_INFO)


            except:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                msg = repr(traceback.format_exception(exc_type, exc_value, exc_traceback))
                self.logger.log(msg, self.logger.LOG_LEVEL_ERROR)

    def execute(self, parameters, messages):
        try:

            parDestWorkspace = ""
            parDataset = ""
            parDeleteDomains = False

            parDestWorkspace = parameters[0].valueAsText

            self.logger.log("Destination dataset is '%s'" % (parDestWorkspace), self.logger.LOG_LEVEL_DEBUG)


            if (arcpy.Exists(parDestWorkspace)):
                self.logger.log("Destination workspace/dataset '%s' is valid." % (parDestWorkspace), self.logger.LOG_LEVEL_DEBUG)
            else:
                self.logger.log("Destination workspace/dataset parameter '%s' is invalid." % (parDestWorkspace), self.logger.LOG_LEVEL_ERROR)
                raise ParameterError
                sys.exit(-1)

            desc = arcpy.Describe(parDestWorkspace)
            datasetType = desc.dataType
            self.logger.log("Dataset type is '%s'" %(datasetType), self.logger.LOG_LEVEL_DEBUG)

            if (datasetType == "FeatureDataset"):
                # feature dataset is selected
                env.workspace = parDestWorkspace
                self.deleteRelationships(parDestWorkspace)

            if (datasetType == "Workspace"):
                # workspace is selected
                env.workspace =  parDestWorkspace

                datasets = arcpy.ListDatasets()
                for dataset in datasets:
                    msg = "Deleting relationships in dataset '%s'..." %(dataset)
                    self.logger.log(msg, self.logger.LOG_LEVEL_DEBUG)
                    fullDatasetName = path.join(parDestWorkspace, dataset)
                    env.workspace = fullDatasetName
                    self.deleteRelationships(fullDatasetName)
                    msg = "Deleted all relationships in dataset '%s'." %(dataset)
                    self.logger.log(msg, self.logger.LOG_LEVEL_INFO)

                # now delete root level feature classes and tables
                self.deleteRelationships(parDestWorkspace)


            self.logger.log( "Script completed.", self.logger.LOG_LEVEL_INFO)
        except ParameterError:
            self.logger.log("Invalid parameter encountered.\nUsage: DeleteRelationships <Input Workspace | Dataset>", self.logger.LOG_LEVEL_ERROR)
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            msg = repr(traceback.format_exception(exc_type, exc_value, exc_traceback))
            self.logger.log(msg, self.logger.LOG_LEVEL_ERROR)
            print msg
        finally:
            sys.exit()

