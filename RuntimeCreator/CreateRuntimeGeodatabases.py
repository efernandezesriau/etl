# -------------------------------------------------------------------------------
# Name:        CreateRuntimeGeodatabases.py
#
# Purpose:     Script to create runtime geodatabase for each file
#
# Author:      vapte
#
# Created:     29/11/2017
# Copyright:   (c) vapte 2016
# Licence:     <your licence>
# -------------------------------------------------------------------------------
import arcpy, os, traceback, sys, shutil, csv, time, datetime
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from util import Util

path_mxd = ''
path_output = ''
path_scratch = ''
email_sender = ''
email_server = ''
email_port = ''
email_recipients = ''

# end email report after core processing completes
def send_email_report(config_parser, subject, body):
    """
    an E-mail method hardcoded to log into and and send e-mails.
    :param config_parser: config file
    :param subject: the subject line in the e-mail
    :param body: the body of the the e-mail
    :return: is e-mail was successful
    """

    # Load Configuration
    email_server = config_parser.get('email', 'server')
    email_port = config_parser.get('email', 'port')
    email_sender = config_parser.get('email', 'sender')
    email_recipients = config_parser.get('email', 'recipients')

    # msg = MIMEText(body)
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = email_sender
    msg['To'] = ','.join(email_recipients.split(';'))

    report_body = '\n'.join(body)

    # create html Message to preserve formatting of the report message
    htmlbody = '<html><body><pre>{}</pre><body/></html>'.format(report_body)

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(report_body, 'plain')
    part2 = MIMEText(htmlbody, 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this
    # case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)

    server = smtplib.SMTP(email_server, email_port)
    server.ehlo()
    #server.starttls() -- Commented by Srini
    #server.login(username, password) -- Commented by Srini
    server.sendmail(email_sender, email_recipients, msg.as_string())
    server.quit()

    return True

# validate inputs in config file before starting to process
def validate_inputs(config_parser):
    global path_mxd, path_output, path_scratch
    message = ''

    path_mxd = config_parser.get('map_documents', 'document_folder')
    if not os.path.exists(path_mxd):
        message = message + "\nThe [map_documents]document_folder does not exist. Check the .ini file.\n"

    path_output = config_parser.get('output', 'output_folder')
    if not os.path.exists(path_output):
        message = message + "\nThe [output]output_folder does not exist. Check the .ini file.\n"

    path_scratch = config_parser.get('output', 'scratch_folder')
    if not os.path.exists(path_scratch):
        message = message + "\nThe [output]scratch_folder does not exist. Check the .ini file.\n"

    return message

# Change the source for this layer to a FGDB
# Feature class is created for each layer with data source and definition query applied to it.
# This is to circumvent limitation on ArcGIS Create Runtime content tool which does not definition query
def export_layer_to_fgdb(layer_to_export, fgdb_path):
    ds_name = layer_to_export.datasetName.split('.')[-1]
    layer_name = "".join(x for x in layer_to_export.name if x.isalnum() or x == '_')

    fgdb_path_name = os.path.join(fgdb_path, 'Runtime.gdb')
    if (os.path.exists(fgdb_path_name) == False):
        Util.log("Creating fgdb '%s'" % (fgdb_path_name), Util.LOG_LEVEL_DEBUG, False,
                 True)
        arcpy.CreateFileGDB_management(fgdb_path, 'Runtime')

    layer_in_memory = r"in_memory\%s" %(layer_name)
    if (arcpy.Exists(layer_in_memory)):
        arcpy.Delete_management(layer_in_memory)

    Util.log("Making feature layer for '%s' in '%s' with definition query '%s'" % (layer_to_export.name, layer_to_export.dataSource,layer_to_export.definitionQuery),
             Util.LOG_LEVEL_DEBUG, False, True)

    arcpy.MakeFeatureLayer_management(in_features=layer_to_export.dataSource, out_layer=layer_in_memory,
                                      where_clause=layer_to_export.definitionQuery)
    if (arcpy.Exists(layer_in_memory)):
        fgdb_fc = os.path.join(fgdb_path_name, layer_name)
        if (arcpy.Exists(fgdb_fc)):
            arcpy.Delete_management(fgdb_fc)
        Util.log("Copying '%s' to '%s'..." % (layer_in_memory, fgdb_fc), Util.LOG_LEVEL_DEBUG, False,
                 True)
        arcpy.CopyFeatures_management(in_features=layer_in_memory, out_feature_class=fgdb_fc)
        layer_to_export.replaceDataSource(fgdb_path_name, "FILEGDB_WORKSPACE", layer_name)
        arcpy.Delete_management(layer_in_memory)



# Save layer as a separate MXD for runtime tool
def save_one_layer(mxd_path_name, layer_name, new_path, mxd_suffix):
    mxd_main = arcpy.mapping.MapDocument(mxd_path_name)
    df_active = mxd_main.activeDataFrame
    layers = arcpy.mapping.ListLayers(mxd_main, "", df_active)
    for layer in layers:
        if (layer.isFeatureLayer == False or layer.name.lower() != layer_name.lower()):
            arcpy.mapping.RemoveLayer(df_active, layer)
        else:
            export_layer_to_fgdb(layer, new_path)
    new_mxd_name = "%s_%s.mxd" %(layer_name.replace(" ", "_"), mxd_suffix)
    new_mxd_path_name = os.path.join(new_path, new_mxd_name)
    mxd_main.saveACopy(new_mxd_path_name)
    return new_mxd_name


# get all feature layers in XMD as list
def get_mxd_feature_layers(mxd_path_name):
    list_layers = []
    mxd_main = arcpy.mapping.MapDocument(mxd_path_name)
    df_active = mxd_main.activeDataFrame
    layers = arcpy.mapping.ListLayers(mxd_main, "", df_active)
    for layer in layers:
        if (layer.isFeatureLayer):
            list_layers.append(layer.name)
    return list_layers


# this is where it happens
def main():
    try:
        # store start time to find process time
        start_time = time.time()
        end_time = time.time()
        # assume successful processing
        return_code = Util.RETURN_CODE_SUCCESS
        status_completion = "completed successfully"
        script_name = os.path.splitext(os.path.basename(__file__))[0]
        ini_name = "%s.ini" %(script_name)
        server_name = Util.getMachineName().upper()
        print "Loading INI file %s..." %(ini_name)
        config_parser = Util.loadConfig(config_file_name=ini_name)
        log_path = config_parser.get('logging', 'logroot')
        log_filename_prefix = config_parser.get('logging', 'logfile_prefix')
        logging_level = config_parser.getint("logging", "logginglevel")
        # start the logger
        log_file = Util.startLogging(log_path, log_filename_prefix, logging_level)
        log_path = config_parser.get('program', 'suffix_runtime_name')
        suffix_runtime = config_parser.get('program', 'suffix_runtime_name')
        extn_runtime = config_parser.get('program', 'extn_runtime_name')

        # make sure config params are valid
        status_msg = validate_inputs(config_parser)

        if status_msg != '':
            Util.log("Process terminated, prerequisite error: %s" %( status_msg), Util.LOG_LEVEL_ERROR, False, True)
            return_code = Util.RETURN_CODE_ERROR

        else:
            # create a scratch directory
            scratch_dir_name = time.strftime("%d%m%y_%H%M%S", time.localtime())
            Util.log("Creating scratch directory %s..." % (scratch_dir_name), Util.LOG_LEVEL_DEBUG, False, True)
            # get rid of all files and subfolders in the scratch workspace, runtime tool creates subfolders
            for root, dirs, files in os.walk(path_scratch):
                for f in files:
                    os.unlink(os.path.join(root, f))
                for d in dirs:
                    shutil.rmtree(os.path.join(root, d))

            scratch_dir_path = os.path.join(path_scratch, scratch_dir_name)
            os.mkdir(scratch_dir_path)

            mxd_name_prefix = config_parser.get('map_documents', 'mxd_name_prefix')
            mxd_files = os.listdir(path_mxd)
            for mxd_file in [x.lower() for x in mxd_files]:
                if (mxd_file.startswith(mxd_name_prefix.lower())):
                    try:
                        mxd_file_path = os.path.join(path_mxd, mxd_file)
                        Util.log("Processing '%s' map document..." % (mxd_file_path), Util.LOG_LEVEL_DEBUG, False, True)
                        list_layer_names = get_mxd_feature_layers(mxd_file_path)
                        for layer_name in list_layer_names:
                            new_mxd_name = save_one_layer(mxd_file_path, layer_name, scratch_dir_path, suffix_runtime)
                            src_mxd_path_name = os.path.join(scratch_dir_path, new_mxd_name)
                            mxd_name_no_extn = os.path.splitext(new_mxd_name)[0]
                            scratch_dir_mxd = os.path.join(scratch_dir_path, mxd_name_no_extn)
                            Util.log("Creating runtime data for '%s'..." % (src_mxd_path_name), Util.LOG_LEVEL_DEBUG, False, True)
                            arcpy.CreateRuntimeContent_management(in_map=src_mxd_path_name, output_folder=scratch_dir_mxd,
                                                                  extent='MAXOF', options='FEATURE_AND_TABULAR_DATA')

                            # now move the runtime file to final destination folder
                            dest_geodatabase_path_name = os.path.join(path_output, "%s.%s" %(mxd_name_no_extn, extn_runtime))

                            for dir, dirnames, filenames in os.walk(scratch_dir_mxd):
                                for filename in filenames:
                                    # runtime file has extension of .geodatabase
                                    if (filename.lower().endswith('.%s' %(extn_runtime.lower()))):
                                        src_file_name_path = os.path.join(dir, filename)
                                        shutil.copy2(src_file_name_path, dest_geodatabase_path_name)
                                        Util.log("Copied runtime geodatabase to '%s'." % (dest_geodatabase_path_name),
                                                 Util.LOG_LEVEL_DEBUG, False, True)
                    except Exception as ex:
                        Util.log("Error: " + ex.message, Util.LOG_LEVEL_ERROR, False, True)
                        exc_type, exc_value, exc_traceback = sys.exc_info()
                        Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR,
                                 False, True)
                        return_code =  Util.RETURN_CODE_WARNING

        # all done
        if (return_code == Util.RETURN_CODE_ERROR):
            status_completion = "completed with errors"
        elif (return_code == Util.RETURN_CODE_WARNING):
            status_completion = "completed with warnings"
        else:
            status_completion = "completed successfully"

        end_time = time.time()
        msg_summary = "Runtime geodatabase creation %s on %s at %s " % (status_completion, server_name, time.strftime("%d-%m-%y %H:%M:%S", time.localtime()))
        Util.log(msg_summary, Util.LOG_LEVEL_INFO, False, True)

        send_email = config_parser.getboolean('email', 'emailreport')
        if (send_email):
            with open(log_file) as f:
                report_content = f.readlines()
                send_email_report(config_parser, msg_summary, report_content)

    except Exception as e:
        Util.log("Error: " + e.message, Util.LOG_LEVEL_ERROR, False, True)
        exc_type, exc_value, exc_traceback = sys.exc_info()
        Util.log(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)), Util.LOG_LEVEL_ERROR, False,
                 True)
        returnMessage = "ERROR: " + e.message + ". TRACEBACK: " + repr(
            traceback.format_exception(exc_type, exc_value, exc_traceback))
    finally:
        elapsedTime = Util.getElapsedTime(start_time, end_time)
        Util.log("Elapsed time: " + elapsedTime, Util.LOG_LEVEL_INFO, False, True)
        Util.log("Exiting %s... " %(script_name), Util.LOG_LEVEL_INFO, False, True)


if __name__ == '__main__':
    main()
