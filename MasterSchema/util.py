# -------------------------------------------------------------------------------
# Name:        util.py
# Purpose:     Utilities functions
#
# Author:      mdonnelly
#
# Created:     17/04/2015
# Copyright:   (c) mdonnelly 2015
# Licence:     <your licence>
# -------------------------------------------------------------------------------
# standard
import logging
import ConfigParser
import traceback
import smtplib
import string
import time
import os
import sys
from zipfile import ZipFile as zip

#3rd party
import arcpy

class Util:
    LOG_LEVEL_DEBUG = 3
    LOG_LEVEL_INFO = 2
    LOG_LEVEL_WARNING = 1
    LOG_LEVEL_ERROR = 0
    # Set once during start_logging
    logLevel = 0

    def __init__(self):
        pass

    # Logs messages to file. Adds a message to tool results console if required.
    #
    @staticmethod
    def log(message, log_level, add_message=False, print_message=False):

        # log only if log level is higher or same
        if log_level <= Util.logLevel:

            one_line_message = message.replace("\n", "->")
            log_message = "%02d %s" % (0, one_line_message)

            if log_level == Util.LOG_LEVEL_ERROR:
                logging.error(log_message)
            elif log_level == Util.LOG_LEVEL_WARNING:
                logging.warning(log_message)
            elif log_level == Util.LOG_LEVEL_INFO:
                logging.info(log_message)
            else:  # log_level == Util.LOG_LEVEL_DEBUG:
                logging.debug(log_message)

            if add_message:
                if log_level == Util.LOG_LEVEL_ERROR:
                    arcpy.AddError(one_line_message)
                elif log_level == Util.LOG_LEVEL_WARNING:
                    arcpy.AddWarning(one_line_message)
                else:
                    arcpy.AddMessage(one_line_message)

        if print_message:
            print message


    # Base method to set python logging
    #
    @staticmethod
    def GetPythonLogger(logging_level, logfile):

        # Python logging levels are 50 for Severe, 40 for Error, 30 for Warning, 20 for Info and 10 for Debug
        # Esri Australia logging levels are 3 for debug, 2 for info, 1 for warning and 0 for error
        Util.logLevel = 40 - (logging_level * 10)

        # print "Log file is %s" % logfile
        # arcpy.AddMessage("Log file is " + logfile)
        logging.getLogger('').handlers = []  # Clear any existing logging handlers
        if os.path.exists(logfile):  # Create a new one for each day
            logger = logging.basicConfig(level=Util.logLevel, format='%(asctime)s %(levelname)s %(message)s',
                                         filename=logfile, filemode='a')
        else:
            logger = logging.basicConfig(level=Util.logLevel, format='%(asctime)s %(levelname)s %(message)s',
                                         filename=logfile, filemode='w')
        return logger


    # Starts the logging
    #
    @staticmethod
    def start_logging(log_path, logging_level, log_prefix):
        time_string = Util.get_time()
        logfile = os.path.join(log_path, log_prefix + "_" + time_string + ".log")
        return Util.GetPythonLogger(logging_level, logfile)

    # Starts the logging
    #
    @staticmethod
    def start_logging_2(log_path, logging_level, logfile_prefix):
        time_string = Util.get_time()
        logfile = os.path.join(log_path, logfile_prefix + "_" + time_string + ".log")
        return Util.GetPythonLogger(logging_level, logfile)

    # Starts the logging
    #
    @staticmethod
    def start_logging_3(log_path, logging_level, logfile_prefix, log_entry_prefix=''):
        time_string = Util.get_time()
        if not os.path.isdir(log_path):
            os.makedirs(log_path)
        if log_entry_prefix == '':
            logfile = os.path.join(log_path, logfile_prefix + '_' + time_string + ".log")
        else:
            logfile = os.path.join(log_path, logfile_prefix + '_' + time_string + "_" + log_entry_prefix + ".log")
        return Util.GetPythonLogger(logging_level, logfile)

    # Gets time and returns a formatted string
    #
    @staticmethod
    def get_time():
        localtime = time.localtime()
        time_string = time.strftime("%d_%m_%Y_%H_%M", localtime)
        return time_string

    @staticmethod
    def get_date():
        localtime = time.localtime()
        time_string = time.strftime("%Y-%m-%Y_%H_%M", localtime)
        return time_string

    # Strip database namespace component
    #
    @staticmethod
    def strip_namespace(table):
        splitstr = table.split('.')
        if splitstr and len(splitstr) == 3:  # Database namespace should be DATABASE.USER.TABLE
            return splitstr[2]
        else:
            return table

    # Caseless compare, returns a boolean
    #
    @staticmethod
    def in_list(search_for, in_list):
        return str(search_for).upper() in map(str.upper, in_list)

    # Loads the config file
    #
    @staticmethod
    def load_config(config_file):
        # ini_file = os.path.join(os.path.split(sys.argv[0])[0], config_file)
        cp = ConfigParser.SafeConfigParser()
        cp.read(config_file)
        return cp


    @staticmethod
    def write_config(config_file, section, key_value):
        """
        write a config file
        :param config_file: the path file name of the config file
        :param section: the section to the the config under
        :param key_value: a dictonary of key and values to write into the config file.
        :return:
        """
        config = ConfigParser.ConfigParser()
        cfgfile = open(config_file, 'w')
        config.add_section(section)
        for key in key_value.keys():
            config.set(section, key, key_value[key])

        config.write(cfgfile)
        cfgfile.close()

    # extractAll: unzips a file
    #
    @staticmethod
    def extract_all(zip_name):
        z = zip(zip_name)
        for f in z.namelist():
            if f.endswith('/'):
                os.makedirs(f)
            else:
                z.extract(f)

    # Truncate feature classes in a specified dataset
    @staticmethod
    def truncate_dataset(dataset_path, namelist=[]):
        arcpy.env.workspace = dataset_path
        for fc in arcpy.ListFeatureClasses():
            fqfc = os.path.join(dataset_path, fc)
            Util.truncate_feature_class(fqfc, namelist)
            # arcpy.TruncateTable_management(fqfc)
            # Util.log("Truncated feature class '%s'." % (fqfc), Util.LOG_LEVEL_DEBUG)

    # Truncate all feature classes and tables in a specified workspace recursively
    @staticmethod
    def truncate_workspace(workspace_source, namelist=[]):

        try:

            if not arcpy.Exists(workspace_source):
                raise ValueError("Workspace '%s' does not exist or is invalid." % workspace_source)
            else:
                desc = arcpy.Describe(workspace_source)
                Util.log("Workspace type is '%s'" % desc.workspaceType, Util.LOG_LEVEL_DEBUG)

            arcpy.env.workspace = workspace_source

            # Process datasets
            for ds in arcpy.ListDatasets("*", "All"):
                dataset_src_full_path = os.path.join(workspace_source, ds)
                Util.log("Truncating feature classes in dataset '%s' ..." % dataset_src_full_path, Util.LOG_LEVEL_DEBUG)
                Util.truncate_dataset(dataset_src_full_path, namelist)
                Util.log("Truncated feature classes in '%s'." % dataset_src_full_path, Util.LOG_LEVEL_DEBUG)
            arcpy.env.workspace = workspace_source
            # Process feature classes
            for fc in arcpy.ListFeatureClasses("*", "All"):
                dataset_src_full_path = os.path.join(workspace_source, fc)
                Util.truncate_feature_class(dataset_src_full_path, namelist)
                # Util.log("Truncating feature class '%s'..." % datasetSrcFullPath, Util.LOG_LEVEL_DEBUG)
                # arcpy.TruncateTable_management(datasetSrcFullPath)
                # Util.log("Truncated feature class '%s'." % datasetSrcFullPath, Util.LOG_LEVEL_DEBUG)

            # Process tables
            for tab in arcpy.ListTables("*", "All"):
                dataset_src_full_path = os.path.join(workspace_source, tab)
                Util.truncate_table(dataset_src_full_path, namelist)
                # Util.log("Truncating table '%s'..." % datasetSrcFullPath, Util.LOG_LEVEL_DEBUG)
                # arcpy.TruncateTable_management(datasetSrcFullPath)
                # Util.log("Truncated table '%s'." % datasetSrcFullPath, Util.LOG_LEVEL_DEBUG)

        except Exception as e:
            error = traceback.format_exc()
            Util.log("Failed to perform Truncate Feature Classes: %s." % error, Util.LOG_LEVEL_ERROR)
            raise

    @staticmethod
    def truncate_table(dataset_src_full_path, namelist=[]):
        found = False
        for name in namelist:
            if name in dataset_src_full_path:
                found = True
                break
        if found:
            Util.log("Truncating table '%s'..." % dataset_src_full_path, Util.LOG_LEVEL_DEBUG)
            arcpy.TruncateTable_management(dataset_src_full_path)
            Util.log("Truncated table '%s'." % dataset_src_full_path, Util.LOG_LEVEL_DEBUG)

    @staticmethod
    def truncate_feature_class(dataset_src_full_path, namelist=[]):
        found = False
        for name in namelist:
            if name in dataset_src_full_path:
                found = True
                break
        if found:
            Util.log("Truncating feature class '%s'..." % dataset_src_full_path, Util.LOG_LEVEL_DEBUG)
            arcpy.TruncateTable_management(dataset_src_full_path)
            Util.log("Truncated feature class '%s'." % dataset_src_full_path, Util.LOG_LEVEL_DEBUG)


    # delete relationships from a specified dataset
    @staticmethod
    def deleteDatasetRelationships(dataset):
        if (arcpy.Exists(dataset)):
            try:
                arcpy.env.workspace = dataset
                list_rel_classes = [c.name for c in arcpy.Describe(dataset).children if c.datatype == "RelationshipClass"]
                for rel_class_name in list_rel_classes:
                    rel_full_class_name = os.path.join(dataset, rel_class_name)
                    if (arcpy.Exists(rel_full_class_name)):
                        msg = "Deleting relationship class '%s'..." %(rel_class_name)
                        Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)
                        arcpy.Delete_management(rel_full_class_name)
                        msg = "Deleted '%s' relationship class." %(rel_class_name)
                        Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)


            except:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                msg = repr(traceback.format_exception(exc_type, exc_value, exc_traceback))
                Util.log(msg, Util.LOG_LEVEL_WARNING, False, True)

    # delete all relationships from the workspace include any datasets
    @staticmethod
    def deleteAllRelationships(workspace):
        try:

            parDestWorkspace = workspace

            msg = "Deleting relationships from '%s'..." %(parDestWorkspace)
            Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)


            if (arcpy.Exists(parDestWorkspace) == False):
                msg = "Destination workspace/dataset '%s' is invalid." % (parDestWorkspace)
                Util.log(msg, Util.LOG_LEVEL_WARNING, False, True)
                return
            else:
                desc = arcpy.Describe(parDestWorkspace)
                datasetType = desc.dataType

                if (datasetType == "FeatureDataset"):
                    # feature dataset is selected
                    arcpy.env.workspace = parDestWorkspace
                    Util.deleteDatasetRelationships(parDestWorkspace)

                if (datasetType == "Workspace"):
                    # workspace is selected
                    arcpy.env.workspace =  parDestWorkspace

                    datasets = arcpy.ListDatasets()
                    for dataset in datasets:
                        msg = "Deleting relationships in dataset '%s'..." %(dataset)
                        Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)
                        fullDatasetName = os.path.join(parDestWorkspace, dataset)
                        arcpy.env.workspace = fullDatasetName
                        Util.deleteDatasetRelationships(fullDatasetName)
                        msg = "Deleted all relationships in dataset '%s'." %(dataset)
                        Util.log(msg, Util.LOG_LEVEL_DEBUG, False, False)

                    # now delete root level feature classes and tables
                    Util.deleteDatasetRelationships(parDestWorkspace)


            msg = "Deleted relationships from '%s'." %(parDestWorkspace)
            Util.log(msg, Util.LOG_LEVEL_INFO, False, True)
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            msg = repr(traceback.format_exception(exc_type, exc_value, exc_traceback))
            Util.log(msg, Util.LOG_LEVEL_WARNING, False, True)
        finally:
            return