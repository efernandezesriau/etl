@echo off
SET CURRPATH=%~dp0
SET CURRDRIVE=%CD:~0,3%


SET PYTHONPATH="C:\Python27\ArcGIS10.4;C:\Python27\ArcGIS10.4\LIB"
SET PATH=C:\Python27\ArcGIS10.4;C:\Python27\ArcGIS10.4\LIB;%PATH%
SET PYTHONEXE="C:\Python27\ArcGIS10.4\Python.exe"

set curTimestamp=%date:~4,2%_%date:~7,2%_%date:~10,4%_%time:~0,2%_%time:~3,2%
echo %curTimestamp%

SET SCRIPTNAME="%CURRPATH%BuildMappingTables.py"
SET INIFILE="%CURRPATH%BuildMappingTables.ini"

%PYTHONEXE% %SCRIPTNAME% %INIFILE%

Pause

set curTimestamp=%date:~4,2%_%date:~7,2%_%date:~10,4%_%time:~0,2%_%time:~3,2%
echo %curTimestamp%

