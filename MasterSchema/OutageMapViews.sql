Drop View OutageEquipmentView
GO

Create  View OutageEquipmentView
As
Select EQ.ObjectID OBJECTID, EQ.Shape SHAPE, EQ.SAPObjectKey EquipmentKey, EQ.SAPObjectClass EquipmentType, EQ.SymbolRotation SymbolRotation,
       OUTAGEEQ.SAPSwitchNot SwitchNotification, OUTAGEEQ.SAPEquipDesc EquipmentDescription,  OUTAGEEQ.SAPWorkOrderKey SAPWorkOrderKey, 
	   OUTAGE.SAPOrderStatus OrderStatus, OUTAGE.SAPOrdStDate OrderStartDate, OUTAGE.SAPOrdEnDate OrderEndDate
From EquipPoint EQ, OutageEquipment OUTAGEEQ, OutageWorkOrder OUTAGE 
Where EQ.SAPObjectKey=OUTAGEEQ.SAPEquipmentKey And OUTAGEEQ.SAPWorkOrderKey=OUTAGE.SAPObjectKey
GO

Drop View OutageSwitchingOperationsView
GO

Create  View OutageSwitchingOperationsView As
Select EQ.ObjectID OBJECTID, EQ.Shape SHAPE, EQ.SAPObjectKey EquipmentKey, EQ.SAPObjectClass EquipmentType, EQ.SymbolRotation SymbolRotation, 
    OUTAGESW.SAPOperNum OperationNumber, OUTAGESW.SAPOperDes OperationDescription, OUTAGESW.SAPOperEqDesc EquipmentDescription,  
	OUTAGESW.SAPWorkOrderKey SAPWorkOrderKey, 
	OUTAGE.SAPOrderStatus OrderStatus, OUTAGE.SAPOrdStDate OrderStartDate, OUTAGE.SAPOrdEnDate OrderEndDate
From EQUIPPOINT EQ, OUTAGESWITCHING OUTAGESW, OUTAGEWORKORDER OUTAGE 
Where EQ.SAPObjectKey=OUTAGESW.SAPEquipmentKey And OUTAGESW.SAPWorkOrderKey=OUTAGE.SAPObjectKey
GO

