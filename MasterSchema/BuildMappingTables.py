#-------------------------------------------------------------------------------
# Name:        BuildMappingTables.py
#
# Purpose:     Script to build ETL mapping tables
#              Mapping Tables must exist in Oracle database
#
# Author:      vapte
#
# Created:     11/10/2016
# Copyright:   (c) vapte 2016
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import arcpy
import csv
from ConfigParser import SafeConfigParser
#import simplejson as json
import json
import os, sys, math, shutil
import traceback
from os import mkdir
from os.path import split, exists
import time
import datetime
import imghdr
import re
import tempfile
import zipfile
import codecs
from util import Util
import pyodbc

PRINTLOG_ERROR = True

def main():
    # read config file
    global logger
    has_csv_header = True
    script_dir = os.path.dirname(os.path.realpath(__file__))
    script_file = os.path.basename(__file__).split('.')[0].lower()
    INI_file = "%s.INI" %(script_file)
    log_file = "%s.log" %(script_file)
    log_file_path = os.path.join(script_dir, log_file )
    INI_file_path = os.path.join(script_dir, INI_file)

    print "log file = %s, INI file is %s" %(log_file_path, INI_file_path)

    if (not os.path.exists(INI_file)):
        print '%s not found. Cannot proceed.' %(INI_file)
        sys.exit(-1)


    config = SafeConfigParser()
    config.read(INI_file_path)
    # get oracle connection parameters
    par_orcl_server = config.get('workspace', 'oracle_server')
    par_orcl_service = config.get('workspace', 'oracle_service')
    par_orcl_port = config.getint('workspace', 'oracle_port')
    par_user_gtech = config.get('workspace', 'gtech_user')
    par_pwd_gtech = config.get('workspace', 'gtech_password')
    par_user_gcomm = config.get('workspace', 'gcomm_user')
    par_pwd_gcomm = config.get('workspace', 'gcomm_password')
    par_orcl_pwd_encrypted = config.getboolean('workspace', 'password_encrypted')
    par_DSN_gtech = config.get('workspace', 'DSN_gtech')
    par_DSN_gcomm = config.get('workspace', 'DSN_gcomm')

    has_csv_header = config.getboolean('workspace', 'hasheader' )
    par_gtech_feature_map_CSV = config.get('workspace', 'gtech_featuremap' ) # feature mapping
    par_gtech_attribute_map_CSV = config.get('workspace', 'gtech_attributemap' ) # attribute mapping
    par_gcomm_feature_map_CSV = config.get('workspace', 'gcomm_featuremap' ) # feature mapping
    par_gcomm_attribute_map_CSV = config.get('workspace', 'gcomm_attributemap' ) # attribute mapping

    par_gtech_feature_map_table = config.get('workspace', 'featuremap_gtech')
    par_gtech_attr_map_table = config.get('workspace', 'attrmap_gtech')

    par_gcomm_feature_map_table = config.get('workspace', 'featuremap_gcomm')
    par_gcomm_attr_map_table = config.get('workspace', 'attrmap_gcomm')

    par_log_path = config.get('logging', 'logroot' )
    par_log_prefix = config.get('logging', 'loggingFilePrefix' )
    par_log_level = config.getint('logging', 'logginglevel' )

    logger = Util.start_logging_2(log_path=par_log_path, logging_level=par_log_level, logfile_prefix=par_log_prefix)

    #orcl_connection = "Driver={Microdsoft ODBC for Oracle};Server=%s:%d/%s;uid=%s;pwd=%s" % (par_orcl_server, par_orcl_port, par_orcl_service, par_orcl_user, par_orcl_pwd)
    orcl_conn_gtech = "DSN=%s;PWD=%s" %(par_DSN_gtech, par_pwd_gtech)
    orcl_conn_gcomm = "DSN=%s;PWD=%s" %(par_DSN_gtech, par_pwd_gcomm)
    exit_now = False
    for input_param in [par_gtech_feature_map_CSV, par_gtech_attribute_map_CSV, par_gcomm_feature_map_CSV,  par_gcomm_attribute_map_CSV]:
        if (arcpy.Exists(input_param) == False):
            Util.log('%s not found. Cannot proceed.' %(input_param), Util.LOG_LEVEL_DEBUG, False, True)
            exit_now = True


    try:
        orcl_cnxn = pyodbc.connect(orcl_conn_gtech)
        orcl_cnxn.close()
    except Exception as ex:
        exc_type, exc_value, exc_traceback = sys.exc_info()

        Util.log("Error opening connection to DSN '%s': %s" %(orcl_conn_gtech, ex.message),
                 Util.LOG_LEVEL_ERROR, False, PRINTLOG_ERROR)

        Util.log(''.join(traceback.format_exception(exc_type, exc_value, exc_traceback)),
                 Util.LOG_LEVEL_ERROR, False, PRINTLOG_ERROR)
        exit_now = True

    try:
        orcl_cnxn = pyodbc.connect(orcl_conn_gcomm)
        orcl_cnxn.close()
    except Exception as ex:
        exc_type, exc_value, exc_traceback = sys.exc_info()

        Util.log("Error opening connection to DSN '%s': %s" %(orcl_conn_gcomm, ex.message),
                 Util.LOG_LEVEL_ERROR, False, PRINTLOG_ERROR)

        Util.log(''.join(traceback.format_exception(exc_type, exc_value, exc_traceback)),
                 Util.LOG_LEVEL_ERROR, False, PRINTLOG_ERROR)
        exit_now = True

    if (exit_now):
        sys.exit(-1)

    # process Gtech mapping tables
    table_name = par_gtech_feature_map_table
    has_odbc_conn = False
    try:
        orcl_cnxn = pyodbc.connect(orcl_conn_gtech)
        has_odbc_conn = True
        msg = "Processing '%s' ..." %(table_name)
        Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
        orcl_cursor = orcl_cnxn.cursor()
        # process feature map
        sql_stmt = "Delete From %s" %(table_name)
        orcl_cursor.execute(sql_stmt)
        msg = "%d rows deleted from %s" %(orcl_cursor.rowcount, table_name)
        Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
        with open(par_gtech_feature_map_CSV, mode='r') as file_csv:
            csv_reader_feat = csv.reader(file_csv)
            if (has_csv_header):
                next(csv_reader_feat)
            row_num = 0
            for csv_row in csv_reader_feat:
                sql_stmt = "Insert Into %s (RECORD_ID,SOURCE_MODEL,GTECH_FEATURE_ID,GTECH_FEATURE_NAME,GTECH_FEATURE_TYPE,GTECH_FEATURE_VIEW,GTECH_DATA_FILTER,ARCGIS_MODEL,ARCGIS_FEATURE,ARCGIS_SUBTYPE_VALUE,ARCGIS_SUBTYPE_TEXT, NOTES) Values (" %(table_name)
                for col in range (0, 12):
                    if (col > 0):
                        sql_stmt = sql_stmt + ','
                    sql_stmt = "%s'%s'" %(sql_stmt, csv_row[col])
                sql_stmt = sql_stmt + ');'
                #print sql_stmt
                Util.log(sql_stmt, Util.LOG_LEVEL_DEBUG, False, False)
                orcl_cursor.execute(sql_stmt)
                row_num += 1
            msg = "%d rows added to %s" %(row_num, table_name)
            Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
        # process attribute map
        table_name = par_gtech_attr_map_table
        msg = "Processing '%s' ..." %(table_name)
        Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
        sql_stmt = "Delete From %s" %(table_name)
        orcl_cursor.execute(sql_stmt)
        msg = "%d rows deleted from %s" %(orcl_cursor.rowcount, table_name)
        Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
        with open(par_gtech_attribute_map_CSV, mode='r') as file_csv:
            csv_reader_feat = csv.reader(file_csv)
            if (has_csv_header):
                next(csv_reader_feat)
            row_num = 0
            for csv_row in csv_reader_feat:
                sql_stmt = "Insert Into %s (RECORD_ID,FEATURE_MAP_ID,GTECH_FEATURE_ID,GTECH_FEATURE_NAME,GTECH_FEATURE_TYPE,FME_FEATURE_TYPE,GTECH_FEATURE_VIEW,GTECH_COMPONENT_TYPE,GTECH_COMPONENT_TABLE,GTECH_FIELD_NAME,GTECH_FIELD_TYPE,GTECH_FIELD_LENGTH,GTECH_FIELD_ALIAS,GTECH_PICKLIST_NAME,GTECH_PICKLIST_TABLE,ARCGIS_FEATURE,ARCGIS_SUBTYPE_VALUE,ARCGIS_ATTRIBUTE_NAME,ARCGIS_ATTRIBUTE_ALIAS) Values (" %(table_name)
                for col in range (0, 19):
                    if (col > 0):
                        sql_stmt = sql_stmt + ','
                    sql_stmt = "%s'%s'" %(sql_stmt, csv_row[col])
                sql_stmt = sql_stmt + ');'
                Util.log(sql_stmt, Util.LOG_LEVEL_DEBUG, False, False)
                orcl_cursor.execute(sql_stmt)
                row_num += 1
            print "%d rows added to %s" %(row_num, table_name)

        orcl_cnxn.commit()

    except Exception as ex:
        # some error happened in getting an individual logger_folder
        # cannot log, but return the result as dummy
        exc_type, exc_value, exc_traceback = sys.exc_info()

        Util.log("Error processing '%s'. Transaction rolled back: %s" %(table_name, ex.message),
                 Util.LOG_LEVEL_ERROR, False, PRINTLOG_ERROR)

        Util.log(''.join(traceback.format_exception(exc_type, exc_value, exc_traceback)),
                 Util.LOG_LEVEL_ERROR, False, PRINTLOG_ERROR)
        features_deleted = -1
        if (has_odbc_conn):
            orcl_cnxn.rollback()
    if (has_odbc_conn):
        orcl_cnxn.close()
        has_odbc_conn = False

    # process GComm mapping tables
    table_name = par_gcomm_feature_map_table
    has_odbc_conn = False
    try:
        orcl_cnxn = pyodbc.connect(orcl_conn_gcomm)
        has_odbc_conn = True
        msg = "Processing '%s' ..." %(table_name)
        Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
        orcl_cursor = orcl_cnxn.cursor()
        # process feature map
        sql_stmt = "Delete From %s" %(table_name)
        orcl_cursor.execute(sql_stmt)
        msg = "%d rows deleted from %s" %(orcl_cursor.rowcount, table_name)
        Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
        with open(par_gcomm_feature_map_CSV, mode='r') as file_csv:
            csv_reader_feat = csv.reader(file_csv)
            if (has_csv_header):
                next(csv_reader_feat)
            row_num = 0
            for csv_row in csv_reader_feat:
                sql_stmt = "Insert Into %s (RECORD_ID,SOURCE_MODEL,GTECH_FEATURE_ID,GTECH_FEATURE_NAME,GTECH_FEATURE_TYPE,GTECH_FEATURE_VIEW,GTECH_DATA_FILTER,ARCGIS_MODEL,ARCGIS_FEATURE,ARCGIS_SUBTYPE_VALUE,ARCGIS_SUBTYPE_TEXT, NOTES) Values (" %(table_name)
                for col in range (0, 12):
                    if (col > 0):
                        sql_stmt = sql_stmt + ','
                    sql_stmt = "%s'%s'" %(sql_stmt, csv_row[col])
                sql_stmt = sql_stmt + ');'
                Util.log(sql_stmt, Util.LOG_LEVEL_DEBUG, False, False)
                orcl_cursor.execute(sql_stmt)
                row_num += 1
            msg = "%d rows added to %s" %(row_num, table_name)
            Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
        # process attribute map
        table_name = par_gcomm_attr_map_table

        msg = "Processing '%s' ..." %(table_name)
        Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
        sql_stmt = "Delete From %s" %(table_name)
        orcl_cursor.execute(sql_stmt)
        msg = "%d rows deleted from %s" %(orcl_cursor.rowcount, table_name)
        Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
        with open(par_gcomm_attribute_map_CSV, mode='r') as file_csv:
            csv_reader_feat = csv.reader(file_csv)
            if (has_csv_header):
                next(csv_reader_feat)
            row_num = 0
            for csv_row in csv_reader_feat:
                sql_stmt = "Insert Into %s (RECORD_ID,FEATURE_MAP_ID,GTECH_FEATURE_ID,GTECH_FEATURE_NAME,GTECH_FEATURE_TYPE,FME_FEATURE_TYPE,GTECH_FEATURE_VIEW,GTECH_COMPONENT_TYPE,GTECH_COMPONENT_TABLE,GTECH_FIELD_NAME,GTECH_FIELD_TYPE,GTECH_FIELD_LENGTH,GTECH_FIELD_ALIAS,GTECH_PICKLIST_NAME,GTECH_PICKLIST_TABLE,ARCGIS_FEATURE,ARCGIS_SUBTYPE_VALUE,ARCGIS_ATTRIBUTE_NAME,NOTES) Values (" %(table_name)
                for col in range (0, 19):
                    if (col > 0):
                        sql_stmt = sql_stmt + ','
                    sql_stmt = "%s'%s'" %(sql_stmt, csv_row[col])
                sql_stmt = sql_stmt + ');'
                Util.log(sql_stmt, Util.LOG_LEVEL_DEBUG, False, False)
                orcl_cursor.execute(sql_stmt)
                row_num += 1
            print "%d rows added to %s" %(row_num, table_name)

        orcl_cnxn.commit()

    except Exception as ex:
        # some error happened in getting an individual logger_folder
        # cannot log, but return the result as dummy
        exc_type, exc_value, exc_traceback = sys.exc_info()

        Util.log("Error processing '%s'. Transaction rolled back: %s" %(table_name, ex.message),
                 Util.LOG_LEVEL_ERROR, False, PRINTLOG_ERROR)

        Util.log(''.join(traceback.format_exception(exc_type, exc_value, exc_traceback)),
                 Util.LOG_LEVEL_ERROR, False, PRINTLOG_ERROR)
        features_deleted = -1
        if (has_odbc_conn):
            orcl_cnxn.rollback()
    if (has_odbc_conn):
        orcl_cnxn.close()

if __name__ == '__main__':
    main()
