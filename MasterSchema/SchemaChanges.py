#-------------------------------------------------------------------------------
# Name:        Script to modify Esri geodatabase schema to match GTech schema
#              Uses feature mapping table and attribue mapping table
# Purpose:
#
# Author:      vapte
#
# Created:     05/10/2016
# Copyright:   (c) vapte 2016
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import arcpy
import csv
from ConfigParser import SafeConfigParser
#import simplejson as json
import json
import urllib, urllib2, urlparse
import os, sys, math, shutil
import traceback
from os import mkdir
from os.path import split, exists
import time
import datetime
import imghdr
import re
import tempfile
import zipfile
import codecs
from util import Util

# Get if this is a system field
def isSystemField(src_type, in_table, field_name):
    arcgis_system_field = False
    has_field = False
    is_system_field = False
    desc_table = arcpy.Describe(in_table)
    if (field_name.upper() == desc_table.OIDFieldName.upper()):
        arcgis_system_field = True
    else:
        for table_field in desc_table.fields:
            if (table_field.name.upper() == field_name.upper()):
                if (table_field.type.upper() in ["GEOMETRY", "BLOB", "OID", "RASTER", "SDO_GEOMETRY"]):
                    arcgis_system_field = True
                elif (field_name.upper() in ["ENABLED", "PHASEDESIGNATION", "SHAPE", "SUBTYPECD", "OBJECTID", "CREATIONUSER", "DATECREATED", "DATEMODIFIED", "LASTUSER", "ELECTRICTRACEWEIGHT", "FEEDERINFO", "SYMBOLROTATION"]):
                    arcgis_system_field = True
                elif (field_name.upper() in ["NOMINALVOLTAGE","OPERATINGVOLTAGE","LOWSIDEVOLTAGE","PRIMARYTAPVOLTAGE","SECONDARYTAPVOLTAGE"]):
                    arcgis_system_field = True
                elif (field_name.upper() in ["STATIONID"] and src_type.lower()[:5] =="trans"):
                    arcgis_system_field = True
                break

    return arcgis_system_field



# convert arcgis field type to standard GTech type
def convertFieldTypeToGTech(field_type):
    new_field_type = field_type
    if (field_type == "DOUBLE"):
        new_field_type = "FLOAT"
    elif (field_type == "SINGLE"):
        new_field_type = "FLOAT"
    elif (field_type == "SMALLINTEGER"):
        new_field_type = "SHORT"
    elif (field_type == "INTEGER"):
        new_field_type = "LONG"
    elif (field_type == "STRING"):
        new_field_type = "TEXT"
    elif (field_type == "SDO_GEOMETRY"):
        new_field_type = "GEOMETRY"
    return new_field_type

# Get a name of the subtype field from a table or feature class
def getSubtypeFieldName(table_name):
    sub_type_field = ""

    has_subtype_field = False

    subtypes = arcpy.da.ListSubtypes(table_name)
    for stcode, stdict in subtypes.iteritems():
        for stkey in stdict.iterkeys():
            if (stkey.upper() == "SUBTYPEFIELD" and len(stdict[stkey].strip()) > 0):
                has_subtype_field = True
                sub_type_field = stdict[stkey].strip()
                break
        if (has_subtype_field):
            break

    return sub_type_field


# check if feature class has a subtype code
def hasSubtypeCode(table_name_full, arcgis_subtype_code):
    has_subtype_code = False


    if (arcgis_subtype_code > 0):
        has_subtype_field = False
        sub_type_field = ""

        subtypes = arcpy.da.ListSubtypes(table_name_full)
        for stcode, stdict in subtypes.iteritems():
            if (stcode == arcgis_subtype_code):
                has_subtype_code = True
                break

    return has_subtype_code

# add a subtype code
def addSubtype(table_name_full, arcgis_subtype_code, arcgis_subtype_text):
    if (arcgis_subtype_code > 0):
        has_subtype_field = False
        has_subtype_code = False
        sub_type_field = ""

        subtypes = arcpy.da.ListSubtypes(table_name_full)
        for stcode, stdict in subtypes.iteritems():
            if (has_subtype_field == False):
                for stkey in stdict.iterkeys():
                    if (stkey.upper() == "SUBTYPEFIELD" and len(stdict[stkey]) > 0):
                        has_subtype_field = True
                        sub_type_field = stdict[stkey]

                        break

            if (has_subtype_field):
                if (stcode == arcgis_subtype_code):
                    has_subtype_code = True
                    break
        if (has_subtype_field == True and has_subtype_code == False):
            arcpy.AddSubtype_management(in_table=table_name_full, subtype_code=arcgis_subtype_code, subtype_description=arcgis_subtype_text)


# add a field to geodatabase table/fc
def addField(table_name, field_name, field_alias, field_type, field_length='', field_precision='', domain_name=''):

    if (field_alias == '' or field_alias is None):
        field_alias = field_name

    field_alias = field_alias.replace('_', ' ')
    arcpy.AddField_management(in_table=table_name, field_name=field_name, field_type=field_type, field_precision=field_precision, field_scale="", field_length=field_length, field_alias=field_alias, field_is_nullable="NULLABLE", field_is_required="NON_REQUIRED", field_domain="")


    if (len(domain_name.strip()) > 0):
        arcpy.AssignDomainToField_management(in_table=table_name, field_name=field_name, domain_name=domain_name)

# get field from a table
def getField(in_table, field_name):
    arcgis_field = None
    has_field = False
    desc_table = arcpy.Describe(in_table)
    for table_field in desc_table.fields:
        if (table_field.name.lower() == field_name.lower()):
            has_field = True
            arcgis_field = table_field
            break
    return arcgis_field

# if table/fc has specified field
def hasField(in_table, field_name):
    has_field = False
    fields = arcpy.ListFields(dataset=in_table, wild_card=field_name)
    has_field = len(fields) == 1
    return has_field

def createTable(in_workspace, table_name):
    table_name_full = os.path.join(in_workspace, table_name)
    if (arcpy.Exists(table_name_full) == False):
        arcpy.CreateTable_management(out_path=in_workspace, out_name=table_name)


INI_FILE_NAME = "SchemaChanges.INI"
FIELD_SUBTYPE = "SubtypeCD"
FIELD_LAST_MODIFIED_NUMBER = "LAST_MODIFIED_NUMBER"
TABLE_GTECH_LAST_MODIFIED = "GTECH_LAST_MODIFIED"

def main():
    global logger
    param_count = arcpy.GetArgumentCount()
    print "Param count = %s" %(param_count)
    has_csv_header = True
    if (param_count == 3):
        par_local_workspace = arcpy.GetParameterAsText(0)
        par_feature_map_CSV = arcpy.GetParameterAsText(1)
        par_attribute_map_CSV = arcpy.GetParameterAsText(2)
        par_geodatabase_schema = "MasterSchema.xml"
        logger = Util.start_logging_2(log_path=os.path.dirname(os.path.realpath(__file__)), logging_level=Util.LOG_LEVEL_DEBUG, logfile_prefix="SchemaChanges.log")

    else:
        config = SafeConfigParser()
        INI_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), INI_FILE_NAME)
        log_file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "SchemaChanges.log")
        if (not os.path.exists(INI_file)):
            print '%s not found. Cannot proceed.' %(INI_file)
            sys.exit(-1)

        config.read(INI_file)

        network_type = config.get('processing', 'network' ).lower()
        if (network_type not in ["dist", "trans", "comms"]):
            print "Invalid processing.network value of '%s' in INI file. Cannot proceed." %(network_type)
            sys.exit(-1)

        has_csv_header = config.getboolean('workspace', 'hasheader' )
	
        par_base_CSV_path = config.get('workspace', 'base_csv_path' ) # mapping CSV folder
	
        if (network_type == "dist" ):
            par_local_workspace = config.get('workspace', 'dist_geodatabase' ) # master geodatabase (schema) to update
            par_geodatabase_schema = config.get('workspace', 'dist_geodatabase_schema' ) # XML schema export of updated master geodatabase
            par_feature_map_CSV = config.get('workspace', 'dist_feature_map' ) # feature mapping
            par_attribute_map_CSV = config.get('workspace', 'dist_attribute_map' ) # attribute mapping
            FIELD_SUBTYPE = config.get('workspace','dist_field_subtype')
        elif (network_type == "comms"):
            par_local_workspace = config.get('workspace', 'comms_geodatabase' ) # master geodatabase (schema) to update
            par_geodatabase_schema = config.get('workspace', 'comms_geodatabase_schema' ) # XML schema export of updated master geodatabase
            par_feature_map_CSV = config.get('workspace', 'comms_feature_map' ) # feature mapping
            par_attribute_map_CSV = config.get('workspace', 'comms_attribute_map' ) # attribute mapping
            FIELD_SUBTYPE = config.get('workspace','comms_field_subtype')
        else:
            par_local_workspace = config.get('workspace', 'trans_geodatabase' ) # master geodatabase (schema) to update
            par_geodatabase_schema = config.get('workspace', 'trans_geodatabase_schema' ) # XML schema export of updated master geodatabase
            
            par_feature_map_CSV = config.get('workspace', 'trans_feature_map' ) # feature mapping
            par_attribute_map_CSV = config.get('workspace', 'trans_attribute_map' ) # attribute mapping
            FIELD_SUBTYPE = config.get('workspace','trans_field_subtype')

        par_log_path = config.get('logging', 'logroot' )
        par_log_prefix = config.get('logging', 'loggingFilePrefix' )
        par_log_level = config.getint('logging', 'logginglevel' )
        par_feature_map_CSV = os.path.join(par_base_CSV_path, par_feature_map_CSV)
        par_attribute_map_CSV = os.path.join(par_base_CSV_path, par_attribute_map_CSV)

        logger = Util.start_logging_2(log_path=par_log_path, logging_level=par_log_level, logfile_prefix=par_log_prefix)

    exit_now = False
    for input_param in [par_local_workspace, par_feature_map_CSV, par_attribute_map_CSV]:
        if (arcpy.Exists(input_param) == False):
            Util.log('%s not found. Cannot proceed.' %(input_param), Util.LOG_LEVEL_DEBUG, False, True)
            exit_now = True

    if (exit_now):
        sys.exit(-1)

    has_m = "DISABLED"
    has_z = "DISABLED"

    dict_features = dict()


    with open(par_feature_map_CSV, mode='r') as file_feature_map:
        csv_reader_feat = csv.reader(file_feature_map, delimiter='|')
        if (has_csv_header):
            next(csv_reader_feat)
        for csv_row in csv_reader_feat:
            if (str(csv_row[0]).strip() == ''):
                continue

            feature_map_id = int(csv_row[0])
            src_model = csv_row[1].strip().upper()
            gtech_feature_id = int(csv_row[2])
            gtech_feature_name = csv_row[3].strip()
            gtech_feature_type = csv_row[4].strip().upper()
            gtech_feature_view = csv_row[5].strip().upper()
            gtech_data_filter = csv_row[6].strip()
            arcgis_model = csv_row[7].strip()
            arcgis_table = csv_row[8].strip()
            arcgis_subtype_id = csv_row[9].strip()
            arcgis_subtype_text = csv_row[10].strip()
            notes = csv_row[11].strip()

            msg = "Processing '%s' of type %s..." %(gtech_feature_name, gtech_feature_type)
            Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
            #dict_features[feature_map_id] = [src_model, gtech_feature_id, gtech_feature_name, gtech_feature_type, gtech_feature_view, gtech_data_filter, arcgis_model, arcgis_table, arcgis_subtype_id, arcgis_subtype_text, notes]
            if (arcgis_table == '' or arcgis_table is None):
                # no mapping defined, skip
                continue

            # see if feature class exists
            arcgis_feature_type = gtech_feature_type
            # use Esri object types
            if (gtech_feature_type == "JUNCTION"):
                arcgis_feature_type = "POINT"
            elif (gtech_feature_type == "EDGE"):
                arcgis_feature_type = "POLYLINE"

            dataset_name = ""
            if (gtech_feature_type != "TABLE"):

                # is is a feature class, determine which dataset it resides in
                if (src_model == "GTECH"):
                    dataset_name = "Electric"
                elif (src_model == "GCOMMS"):
                    dataset_name = "TelecomDataset"
                elif (src_model == "GEOMEDIA"):
                    dataset_name = "TransmissionDataset"

            dataset_name_full = os.path.join(par_local_workspace, dataset_name)
            arcgis_table_nospace = arcgis_table.replace(' ','')
            table_name_full = os.path.join(dataset_name_full, arcgis_table_nospace)


            if (arcpy.Exists(table_name_full) == False):
                msg = "%s '%s' does not exist in ArcGIS Schema. Creating it..." %(gtech_feature_type, arcgis_table)
                Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
                if (gtech_feature_type == "TABLE"):
                    arcpy.CreateTable_management(out_path=dataset_name_full, out_name=arcgis_table_nospace)
                else:
                    arcpy.CreateFeatureclass_management(out_path=dataset_name_full, out_name=arcgis_table_nospace, geometry_type=arcgis_feature_type, has_m=has_m, has_z=has_z)
                    # add ArcFM network fields if a junction or edge feature
                    if (gtech_feature_type == "JUNCTION" or gtech_feature_type == "EDGE"):
                        addField(table_name_full, "AncillaryRole", "Ancillary Role", "SHORT")
                        addField(table_name_full, "Enabled", "Enabled", "SHORT")
                        if (network_type == "dist" ):
                            addField(table_name_full, "ElectricTraceWeight", "Electric Trace Weight", "LONG")
                            addField(table_name_full, "FeederInfo", "Feeder Information", "LONG")
                            addField(table_name_full, "PhaseDesignation", "Phase Designation", "LONG")
                            addField(table_name_full, "FeederID", "Feeder ID", "TEXT", 20)
                            addField(table_name_full, "FeederID2", "Feeder ID 2", "TEXT", 20)
                        if (gtech_feature_type == "JUNCTION"):
                            addField(table_name_full, "SymbolRotation", "Symbol Rotation", "DOUBLE")



                msg = "Created %s %s in ArcGIS Schema." %(arcgis_feature_type, arcgis_table)
                Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)

            if (arcpy.Exists(table_name_full) == False):
                continue
            if (len(arcgis_subtype_id) > 0 and arcgis_subtype_id.isdigit() and len(arcgis_subtype_text) > 0):
                arcgis_subtype_code = int(arcgis_subtype_id)
                field_name_subtype = getSubtypeFieldName(table_name_full)
                if (field_name_subtype == ""):
                    msg = "%s does not have subtype field set. Setting sybtype field." %(arcgis_table)
                    Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)

                    # subtype field does not exist
                    if (hasField(table_name_full, FIELD_SUBTYPE) == False):
                        msg = "%s does not have %s field for subtype. Adding it..." %(arcgis_table, FIELD_SUBTYPE)
                        Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
                        addField(table_name_full, FIELD_SUBTYPE, "Subtype", "SHORT")


                    arcpy.SetSubtypeField_management (table_name_full, FIELD_SUBTYPE)


                if (hasSubtypeCode(table_name_full, arcgis_subtype_code) == False):
                    msg = "Adding subtype '%d-%s' to %s..." %(arcgis_subtype_code, arcgis_subtype_text, arcgis_table)
                    Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
                    arcpy.AddSubtype_management(in_table=table_name_full, subtype_code=arcgis_subtype_code, subtype_description=arcgis_subtype_text)

                if (field_name_subtype == ""):
                    # subtype is new, set default value
                    arcpy.SetDefaultSubtype_management(table_name_full, arcgis_subtype_code)

            # process attributes
            with open(par_attribute_map_CSV, mode='r') as file_attr_map:
                csv_reader_attr = csv.reader(file_attr_map, delimiter='|')
                filtered_reader = filter(lambda p: str(feature_map_id) == p[1], csv_reader_attr)
                for row_attr in filtered_reader:

                    gtech_attr_name = row_attr[10]
                    gtech_attr_type = row_attr[11]
                    gtech_attr_length = row_attr[12].strip()
                    arcgis_attr = row_attr[18]
                    arcgis_attr_alias = row_attr[19]
                    arcgis_table = row_attr[16]

                    if (arcgis_attr.strip() == ''):
                        continue

                    arcgis_attr_name = arcgis_attr
                    # arcgis attribute name is already set correctly in CSV, no need to convert
##                    if (network_type == "comms" ):
##                        # in comms, all field names are upper case
##                        # however, mapping table shows the alias
##                        new_attr_names = []
##                        if (' ' in arcgis_attr_name):
##                            for name in arcgis_attr.split(' '):
##                                if (len(name) > 0):
##                                    new_attr_names.append(name.upper())
##                            arcgis_attr_name = ''.join(new_attr_names)
##                    else:
##                        new_attr_names = []
##                        if (' ' in arcgis_attr_name):
##                            for name in arcgis_attr.split(' '):
##                                if (len(name) > 0):
##                                    if (name != name.upper()):
##                                        new_attr_names.append(name.title())
##                                    else:
##                                        new_attr_names.append(name)
##                            arcgis_attr_name = ''.join(new_attr_names)

                    attr_length = 0
                    if (gtech_attr_length.isdigit()):
                        attr_length = int(gtech_attr_length)

                    field_arcgis_attr = getField(in_table=table_name_full, field_name=arcgis_attr_name)
                    if (field_arcgis_attr is None):
                        # field does not exist, add it
                        msg = "Adding field %s of type %s to %s..." %(arcgis_attr_name, gtech_attr_type, arcgis_table)
                        Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
                        if (arcgis_attr_name.upper() in ["NOMINALVOLTAGE","OPERATINGVOLTAGE","LOWSIDEVOLTAGE","HIGHSIDEVOLTAGE","PRIMARYTAPVOLTAGE","SECONDARYTAPVOLTAGE", "SYMBOLROTATION"]):
                            field_type = "DOUBLE"
                        addField(table_name=table_name_full, field_name=arcgis_attr_name, field_alias = arcgis_attr_alias, field_type=gtech_attr_type, field_length=gtech_attr_length)
                    else:
                        # field exists, check alias, data type and length

                        # Standardize field types between Gtech and arcgis
                        arcgis_field_type = convertFieldTypeToGTech(field_arcgis_attr.type.upper())
                        gtech_field_type = convertFieldTypeToGTech(gtech_attr_type.upper())
                        field_domain = field_arcgis_attr.domain.strip()
                        field_alias = field_arcgis_attr.aliasName
                        if (field_alias != arcgis_attr_alias and arcgis_attr_name.upper() not in ["SHAPE", "OBJECTID","GLOBALID"]):
                            # field alias does not match spreadsheet, fix it
                            msg = "Changing field alias for '%s' from '%s' to '%s'." %(arcgis_attr_name, field_alias, arcgis_attr_alias)
                            Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
                            arcpy.AlterField_management (in_table=table_name_full, field=arcgis_attr_name, new_field_alias = arcgis_attr_alias)
                        # special processing for voltage fields, they should be numeric
                        if (arcgis_attr_name.upper() in ["NOMINALVOLTAGE","OPERATINGVOLTAGE","LOWSIDEVOLTAGE","HIGHSIDEVOLTAGE","PRIMARYTAPVOLTAGE","SECONDARYTAPVOLTAGE"]):
                            if (len(field_domain) > 0 or field_arcgis_attr.defaultValue is not None):
                                msg = "Changing data type of %s in %s from %s to DOUBLE and dropping domain %s." %(arcgis_attr_name, arcgis_table, arcgis_field_type, field_domain)
                                Util.log(msg, Util.LOG_LEVEL_WARNING, False, True)
                                # no provision to drop the domain set against the field. Drop and readd it
                                arcpy.DeleteField_management(in_table=table_name_full, drop_field=arcgis_attr_name)
                                addField(table_name=table_name_full, field_name=arcgis_attr_name, field_alias = arcgis_attr_alias, field_type="DOUBLE")
                            elif (arcgis_field_type != 'FLOAT' and arcgis_field_type != 'DOUBLE'):
                                # make sure all voltage fields do not have domain assigned to them
                                msg = "Changing data type of %s in %s from %s to DOUBLE." %(arcgis_attr_name, arcgis_table, arcgis_field_type)
                                Util.log(msg, Util.LOG_LEVEL_WARNING, False, True)
                                arcpy.AlterField_management (in_table=table_name_full, field=arcgis_attr_name, field_type="DOUBLE")
                        elif (arcgis_field_type != gtech_field_type):
                            # field type does not match
                            alter_field = True
                            # check if field in arcgis is high precision
                            if (arcgis_field_type == "LONG" and gtech_field_type == "SHORT"):
                                alter_field = False
                            elif (arcgis_field_type == "FLOAT" and gtech_attr_type in ["SHORT", "LONG", "DOUBLE"]):
                                alter_field = False
                            if (alter_field):
                                if (len(field_domain) == 0):
                                    # No domain assigned, alter this field with new data type
                                    if (isSystemField(src_type=network_type, in_table=table_name_full, field_name=arcgis_attr_name) == False):
                                        msg = "Changing data type of %s in %s from %s to %s." %(arcgis_attr_name, arcgis_table, arcgis_field_type, gtech_attr_type)
                                        Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
                                        arcpy.AlterField_management (in_table=table_name_full, field=arcgis_attr_name, field_type=gtech_attr_type, field_length=gtech_attr_length)
                                    else:
                                        msg = "Specified field is a system field. Cannot change data type of %s in %s from %s to %s." %(arcgis_attr_name, arcgis_table, arcgis_field_type, gtech_attr_type)
                                        Util.log(msg, Util.LOG_LEVEL_WARNING, False, True)
                                else:
                                    msg = "Cannot change data type of %s in %s with domain '%s' assigned from %s to %s " %(arcgis_attr_name, arcgis_table, field_domain, arcgis_field_type, gtech_attr_type)
                                    Util.log(msg, Util.LOG_LEVEL_WARNING, False, True)
                                        # domain is assigned, cannot change the data type
    ##                                if (isSystemField(src_type=network_type, in_table=table_name_full, field_name=arcgis_attr_name) == False):
    ##                                    arcpy.DeleteField_management(in_table=table_name_full, drop_field=arcgis_attr_name)
    ##                                    addField(table_name=table_name_full, field_name=arcgis_attr_name, field_alias = arcgis_attr, field_type=gtech_attr_type, field_length=gtech_attr_length, domain_name=field_arcgis_attr.domain.strip())
                        else:
                            # field type matches, make sure length matches for text field
                            if ( arcgis_field_type == "TEXT" and field_arcgis_attr.length < attr_length):
                                msg = "Field length %d for %s in ArcGIS Feature '%s' does not match with GTech field length %d for '%s'" %(field_arcgis_attr.length,  arcgis_attr_name, arcgis_table, attr_length, gtech_attr_name )
                                Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
                                arcpy.AlterField_management(in_table=table_name_full, field=arcgis_attr_name, field_length=attr_length)


    # special relationships
    if (network_type == "dist"):
        try:
            parentFC = os.path.join(dataset_name_full, "DistributionTransformer")
            childFC = os.path.join(par_local_workspace, "DistributionTransformerUnit")
            relFC = os.path.join(par_local_workspace, "DistTransformer_DistTransformerUnit")
            if (arcpy.Exists(relFC)):
                arcpy.Delete_management(in_data = relFC)
            if (arcpy.Exists(parentFC) and arcpy.Exists(childFC)):
                arcpy.CreateRelationshipClass_management(origin_table=parentFC, destination_table=childFC, out_relationship_class=relFC, relationship_type="COMPOSITE", forward_label="DistributionTransformerUnit", backward_label="DistributionTransformer", message_direction="NONE", cardinality="ONE_TO_MANY", attributed="NONE", origin_primary_key="G3E_FID", origin_foreign_key="G3E_FID", destination_primary_key="", destination_foreign_key="")
            createTable(in_workspace=par_local_workspace, table_name=TABLE_GTECH_LAST_MODIFIED)
            tableLastMod = os.path.join(par_local_workspace, TABLE_GTECH_LAST_MODIFIED)
            if (hasField(in_table=tableLastMod, field_name=FIELD_LAST_MODIFIED_NUMBER) == False):
                addField(table_name=tableLastMod, field_name=FIELD_LAST_MODIFIED_NUMBER, field_alias=FIELD_LAST_MODIFIED_NUMBER.title(), field_type="DOUBLE")


        except Exception as e:
            msg = "Error creating relationship '%s': %s" %(relFC, e.message)
            Util.log(msg, Util.LOG_LEVEL_ERROR, True, True)

    if (network_type == "comms"):
        try:
            msg = "Dropping all relationships from Comms model..."
            Util.log(msg, Util.LOG_LEVEL_DEBUG, False, True)
            #drop relationships as they are not maintained in ArcGIS
            Util.deleteAllRelationships(workspace=par_local_workspace)
            msg = "Dropped all relationships from Comms model."
            Util.log(msg, Util.LOG_LEVEL_INFO, False, True)
        except Exception as e:
            msg = "Error dropping relationships from comms model: %s" %(e.message)
            Util.log(msg, Util.LOG_LEVEL_ERROR, True, True)




    # All done, now export the schema

    if (os.path.exists(par_geodatabase_schema)):
        os.remove(par_geodatabase_schema)

    arcpy.ExportXMLWorkspaceDocument_management (in_data=par_local_workspace, out_file=par_geodatabase_schema, export_type="SCHEMA_ONLY", export_metadata="METADATA")

if __name__ == '__main__':
    main()
