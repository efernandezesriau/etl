-- Name: ETLViewsStatementsGen.sql
-- Script to construct view creation statements for GComms schema
-- Script must be run as owner of the G/comms database e.g. AEGCOMM
-- Vish Apte 07-Oct-2016 Initial Version


Set Serveroutput On;

-- Create a function to extract angle in deg from the oriented point in SDO
-- This angle is then used to set SymbolRotation field in ArcGIS
Create Or Replace Function FN_POINT_ANGLE (
  origGeom MDSYS.SDO_GEOMETRY)
Return Number As
  angleDeg    number := 0;
  numVertices number;
  dim         number;
  xVector     number;
  yVector     number;
Begin
  If (origGeom Is Not Null) Then
    dim := origGeom.GET_DIMS();
    numVertices := MDSYS.SDO_UTIL.GETNUMVERTICES(origGeom);
    If (numVertices = 2 And origGeom.SDO_POINT Is Null And (MOD(origGeom.SDO_GTYPE,1000) = 1) ) Then
      -- it is oriented point geometry
      xVector := origGeom.SDO_ORDINATES(origGeom.SDO_ELEM_INFO(4));
      yVector := origGeom.SDO_ORDINATES(origGeom.SDO_ELEM_INFO(4)+1);
      angleDeg := atan2(yVector, xVector) * 180 / atan2(0, -1);
      angleDeg := Round(angleDeg, 3);
    End If;
  End If;
  return angleDeg;
Exception
  When Others Then Return angleDeg;
End;
/

Drop Table GTECH_TO_ARCGIS_VIEWS;

Create Table GTECH_TO_ARCGIS_VIEWS (FEATURE_MAP_ID Varchar2(5), 
                                    VIEW_NAME Varchar2(100), 
                                    VIEW_SOURCE Varchar2(4000), 
                                    VIEW_STATUS Number(1));


-- Create views
Declare
  strSQL Varchar2(4000);
  Type TABLE_LIST is TABLE Of Varchar2(100);
  arrTables TABLE_LIST := TABLE_LIST();
  intColCount Number;  
  intCounter Number;
  intCounter2 Number;
  boolPrimary boolean := False;
  strPrimaryTable Varchar2(30);
  strPredicateWord Varchar2(30) := 'Where';
  lttClause boolean := True;
  srcSchemaPrefix Varchar2(30) := 'AEGCOMM.';
  srcViewPrefix Varchar2(30) := 'APP_ARCGIS_FM_GC.';
  
Begin
  Delete From GTECH_TO_ARCGIS_VIEWS;
  For i in (Select distinct ATTRMAP.FEATURE_MAP_ID, FEATMAP.GTECH_FEATURE_ID, FEATMAP.GTECH_FEATURE_VIEW, FEATMAP.GTECH_DATA_FILTER, ATTRMAP.ARCGIS_FEATURE, FEATMAP.ARCGIS_SUBTYPE_VALUE 
             From APP_ARCGIS_FM_GC.GCOMM_TO_ARCGIS_ATTR ATTRMAP, APP_ARCGIS_FM_GC.GCOMM_TO_ARCGIS_FEATURES FEATMAP
             Where ATTRMAP.FEATURE_MAP_ID=FEATMAP.RECORD_ID And ATTRMAP.ARCGIS_ATTRIBUTE_NAME Is Not Null Order By ATTRMAP.FEATURE_MAP_ID)
  Loop
    lttClause := True;  
    strPredicateWord := CHR(13) || CHR(10)|| '  Where';
    strSQL := 'Create Or Replace Force View '||NVL(srcViewPrefix,'')||i.GTECH_FEATURE_VIEW || CHR(13) || CHR(10) || ' As '|| CHR(13) || CHR(10) ||'  Select ';
    intColCount := 0;
    If (i.ARCGIS_SUBTYPE_VALUE Is Not Null) Then
      strSQL := strSQL || i.ARCGIS_SUBTYPE_VALUE || ' SubtypeCode ';
      intColCount := intColCount + 1;
    End If;
    arrTables := TABLE_LIST();
    
    boolPrimary := False;
    for j in (Select Distinct ATTRMAPPING.GTECH_COMPONENT_TABLE, FEATATTR.PRIMARY_CNO, FEATATTR.G3E_CNO  From APP_ARCGIS_FM_GC.GCOMM_TO_ARCGIS_ATTR ATTRMAPPING, AEGCOMM.VW_FEAT_ATTR FEATATTR  Where ATTRMAPPING.GTECH_FEATURE_ID=FEATATTR.G3E_FNO And ATTRMAPPING.GTECH_COMPONENT_TABLE= FEATATTR.COMPONENT_TABLE And FEATURE_MAP_ID=i.FEATURE_MAP_ID And GTECH_FEATURE_VIEW=i.GTECH_FEATURE_VIEW And ARCGIS_ATTRIBUTE_NAME Is Not Null)
    Loop
      If (j.PRIMARY_CNO = j.G3E_CNO) Then
        boolPrimary := True;
      Else 
        boolPrimary := False;
      End If;
      arrTables.Extend();
      arrTables(arrTables.Count) :=  Upper(j.GTECH_COMPONENT_TABLE);
      for k in (Select Distinct Upper(GTECH_FIELD_NAME) GTECH_FIELD_NAME, Upper(GTECH_FIELD_TYPE) GTECH_FIELD_TYPE, Upper(ARCGIS_ATTRIBUTE_NAME) ARCGIS_ATTRIBUTE_NAME From APP_ARCGIS_FM_GC.GCOMM_TO_ARCGIS_ATTR 
               Where FEATURE_MAP_ID=i.FEATURE_MAP_ID And GTECH_FEATURE_VIEW=i.GTECH_FEATURE_VIEW And GTECH_COMPONENT_TABLE=j.GTECH_COMPONENT_TABLE And ARCGIS_ATTRIBUTE_NAME Is Not Null)
      Loop
        If (intColCount > 0) Then
          strSQL := strSQL || ', ' || CHR(13) || CHR(10)||'  ';
        End If;
        If (k.GTECH_FIELD_NAME = 'SHAPE' And k.GTECH_FIELD_TYPE = 'SDO_GEOMETRY') Then
          -- GTech geometry field, field name in table is G3E_GEOMETRY, field name in mapping is SHAPE
          strSQL := strSQL || j.GTECH_COMPONENT_TABLE ||'.G3E_GEOMETRY SHAPE';
        ElsIf (k.GTECH_FIELD_NAME = 'LENGTH' And j.GTECH_COMPONENT_TABLE = 'CONNECTIVITY_N') Then
          -- Alias length field in connectivity table
          strSQL := strSQL || j.GTECH_COMPONENT_TABLE ||'.'||k.GTECH_FIELD_NAME || ' MeasuredLength';
        ElsIf (k.GTECH_FIELD_NAME = 'ALIGNMENT' And k.ARCGIS_ATTRIBUTE_NAME = 'SYMBOLROTATION') Then
          -- Convert alignment to SDO_ORIENTATION if a point angle
          strSQL := strSQL || 'FN_POINT_ANGLE(' || j.GTECH_COMPONENT_TABLE || '.G3E_GEOMETRY) ALIGNMENT'; 
        ElsIf (k.GTECH_FIELD_NAME = 'STATE' And j.GTECH_COMPONENT_TABLE Like '%NMI_N') Then
          -- Convert alignment to SDO_ORIENTATION if a point angle
          strSQL := strSQL || j.GTECH_COMPONENT_TABLE ||'.'||k.GTECH_FIELD_NAME || ' StateAbbrev';
        Else
          strSQL := strSQL || j.GTECH_COMPONENT_TABLE ||'.'||k.GTECH_FIELD_NAME;
          
        End If;
        
        intColCount := intColCount + 1;          
      End Loop;
      --If (intPrimary > 0) Then
      --  If (intColCount > 0) Then
      --    strSQL := strSQL || ', ';
      --  End If;  
      --  strSQL := strSQL || j.GTECH_COMPONENT_TABLE ||'.'||'G3E_GEOMETRY ';
      --  intColCount := intColCount + 1;
      --End If;            
    End Loop;
    strSQL := strSQL || CHR(13) || CHR(10) || ' From ';
    For intCounter in 1..arrTables.COUNT
    Loop
      If (intCounter > 1) Then
        strSQL := strSQL || ', ' || CHR(13) || CHR(10)||'    ';
      End If;  
      strSQL := strSQL || NVL(srcSchemaPrefix,'') || 'B$' || arrTables(intCounter) ||' '|| arrTables(intCounter);
    End Loop;
    -- Add G3E_FNO clause
    strSQL := strSQL || '  '|| strPredicateWord ||' '|| arrTables(1)||'.G3E_FNO='||i.GTECH_FEATURE_ID;
    strPredicateWord := '  And';
    If (lttClause = True) Then
      strSQL := strSQL || CHR(13) || CHR(10) ||  strPredicateWord ||' NVL('|| arrTables(1)||'.LTT_ID,0)=0 ';
    End If;   
    If (arrTables.COUNT > 1) Then
        For intCounter in 2..(arrTables.COUNT)
        Loop
          
          strSQL := strSQL ||' '||  CHR(13) || CHR(10) || strPredicateWord ||' NVL('|| arrTables(intCounter)||'.LTT_ID,0)=0 ';
          strPredicateWord := '  And';  
          strSQL := strSQL ||' '||  CHR(13) || CHR(10) || strPredicateWord ||' '|| arrTables(intCounter)||'.G3E_FID='||arrTables(1)||'.G3E_FID ';
          
          strPredicateWord := '  And';  
        End Loop;
        -- G3E_CID condition on SDOGEOM columns
        For intCounter in 1..(arrTables.COUNT)
        Loop
          For intCounter2 in 1..(arrTables.COUNT)
          Loop
            If (arrTables(intCounter) = (arrTables(intCounter2)||'_SDOGEOM')) Then 
              -- Geometry table, match by CID as well
              strSQL := strSQL ||' '|| strPredicateWord ||' '|| arrTables(intCounter)||'.G3E_CID='||arrTables(intCounter2)||'.G3E_CID ';
              strPredicateWord := '  And';  
            End If;
          End Loop;  
        End Loop;
    End If;
    
    DBMS_OUTPUT.PUT_LINE('Filter = '||i.GTECH_DATA_FILTER); 
    If (i.GTECH_DATA_FILTER Is Null Or i.GTECH_DATA_FILTER = '') Then
      Null;
    Else
      strSQL := strSQL || CHR(13) || CHR(10) ||' And ' || i.GTECH_DATA_FILTER;
    End If;
   
    --DBMS_OUTPUT.PUT_LINE(strSQL);
    
    Begin
      Execute Immediate strSQL;
      --DBMS_OUTPUT.PUT_LINE('Success:'||strSQL);
      Insert Into GTECH_TO_ARCGIS_VIEWS (FEATURE_MAP_ID,VIEW_NAME, VIEW_SOURCE, VIEW_STATUS) Values (TO_NUMBER(i.FEATURE_MAP_ID), i.GTECH_FEATURE_VIEW, strSQL, 1);
    Exception 
      When Others Then
        Insert Into GTECH_TO_ARCGIS_VIEWS (FEATURE_MAP_ID,VIEW_NAME, VIEW_SOURCE, VIEW_STATUS) Values (TO_NUMBER(i.FEATURE_MAP_ID), i.GTECH_FEATURE_VIEW, strSQL, 0);
        --DBMS_OUTPUT.PUT_LINE('Error:'||strSQL);
    End;
            
  End Loop;
  Commit;
End;
/

Select RTrim(LTrim(VIEW_SOURCE||';')) From GTECH_TO_ARCGIS_VIEWS Where VIEW_STATUS=0; 

Select RTrim(LTrim(VIEW_SOURCE||';')) From GTECH_TO_ARCGIS_VIEWS Where VIEW_STATUS=1; 

Select RTrim(LTrim(VIEW_SOURCE||';')) From GTECH_TO_ARCGIS_VIEWS Order By FEATURE_MAP_ID;

