-- Script to created index on G3E_FID column of B$ tables 
-- This should be set-up already by G/Tech.
-- Vish Apte 05-Dec-2016 Initial Version

Set Serveroutput On;

-- create indexes on G3E_FID column of B$ tables
Declare
  idx_name VARCHAR2(30);
  idx_count Numeric;
  sql_stmt Varchar2(1000);
Begin
  For i in (Select tname from tab where tname like 'B$%' and tname not like '%_TMP')
  Loop
    Select count(*) into idx_count From user_indexes a, user_ind_columns b where b.column_name='G3E_FID' and a.table_name=b.table_name and a.table_name=i.tname ;
    If (idx_count = 0) Then
      idx_name := 'IDX_FID_'||Replace(i.tname,'B$','');
      If (length(idx_name) > 30) Then
        idx_name := substr(idx_name, 1, 30);
      End If;
      sql_stmt := 'Create index '||idx_name||' on '||i.tname||'(G3E_FID)';
      dbms_output.put_line('Creating index on ' || sql_stmt);
      execute immediate sql_stmt;
      dbms_output.put_line('index created');
    End If;
  End Loop;
End;
/



