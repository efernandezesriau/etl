alter session set "_ORACLE_SCRIPT"=true; 

Drop User AEGCOMM CASCADE;

Create Tablespace TBLSP_GCOMM Datafile 'E:\Oracle\oradata\orcl\TasNet\TASNET_GCOMM1.DBF' Size 1000M Autoextend On Next 10M MaxSize 16000M;

Create User AEGCOMM identified by AE Default Tablespace TBLSP_GCOMM Temporary Tablespace TEMP Quota Unlimited On TBLSP_GCOMM Profile Default Account Unlock;

Grant Connect, Resource to AEGCOMM;

Create User AEELECTRIC identified by AE Default Tablespace TBLSP_GCOMM Temporary Tablespace TEMP Quota Unlimited On TBLSP_GCOMM Profile Default Account Unlock;

Grant Connect, Resource to AEELECTRIC;

Create Role Marketing;

Create Role Designer;

Create Role Supervisor;
  
Create Role Finance;

Create Role OMSPUBLISH;

Create Role FULLPUBLISH;

Create Role Administrator;

CREATE OR REPLACE DIRECTORY GCOMM_DUMP_DIR AS 'E:\Projects\TasNetworks\DATA';

Grant Read,Write on Directory GCOMM_DUMP_DIR to AEGCOMM;