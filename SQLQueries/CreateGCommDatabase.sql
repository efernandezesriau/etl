-- Script to build GCOMM empty schema

alter session set "_ORACLE_SCRIPT"=true; 

Drop User AEGCOMM CASCADE;

Create Tablespace TBLSP_GCOMM Datafile 'B:\app\orcl\oradata\TasNet\TASNET_GCOMM1.DBF' Size 1000M Autoextend On Next 10M MaxSize 16000M;

Create User AEGCOMM identified by AE Default Tablespace TBLSP_GCOMM Temporary Tablespace TEMP Quota Unlimited On TBLSP_GCOMM Profile Default Account Unlock;

CREATE USER APP_ARCGIS_FM_GC IDENTIFIED BY AE Default Tablespace TBLSP_GCOMM Temporary Tablespace TEMP Quota Unlimited On TBLSP_GCOMM Profile Default Account Unlock;

Grant Connect, Resource to AE, APP_ARCGIS_FM_GC;

Grant Select Any Table to APP_ARCGIS_FM_GC;

Grant Create View to APP_ARCGIS_FM_GC;

GRANT EXECUTE ANY PROCEDURE TO APP_ARCGIS_FM_GC;

Grant Connect, Resource to AEGCOMM;

Create User AEELECTRIC identified by AE Default Tablespace TBLSP_GCOMM Temporary Tablespace TEMP Quota Unlimited On TBLSP_GCOMM Profile Default Account Unlock;

Grant Connect, Resource to AEELECTRIC;

Create Role Marketing;

Create Role Designer;

Create Role Supervisor;
  
Create Role Finance;

Create Role OMSPUBLISH;

Create Role FULLPUBLISH;

Create Role Administrator;

CREATE OR REPLACE DIRECTORY GCOMM_DUMP_DIR AS 'B:\Datasets\TasNetworks\GCommsData';

Grant Read,Write on Directory GCOMM_DUMP_DIR to AEGCOMM;