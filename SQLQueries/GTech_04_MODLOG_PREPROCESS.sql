-- Name: MODLOG_PREPROCESS.sql
-- Script to build purged Modification Log table for GTech to ArcGIS 
--   and procedure to populate it.
-- This script must be executed as a schema that owns VW_* views e.g. APP_ARCGIS_FM
-- Vish Apte 25-Oct-2016 Initial Version
-- Vish Apte 30-Nov-2016 Added index creation statement

Spool GTech_04_MODLOG_PREPROCESS.LOG;

-- Drop table MODLOG_GTECH_TO_ARCGIS is it exists already
Drop Table APP_ARCGIS_FM.MODLOG_GTECH_TO_ARCGIS;

-- Create a table to hold modifications meaning to ArcGIS ETL process
CREATE TABLE APP_ARCGIS_FM.MODLOG_GTECH_TO_ARCGIS
(
  G3E_FID        NUMBER(10),
  MOD_NUMBER     NUMBER(38,4),
  GTECH_FEATURE  VARCHAR2(100 BYTE),
  MOD_TYPE       VARCHAR2(10 BYTE),
  STATUS         VARCHAR2(100 BYTE),
  ERROR_DESCRIP  VARCHAR2(3000 BYTE)
)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

CREATE INDEX IDX_MOD_LOG_FID ON APP_ARCGIS_FM.MODLOG_GTECH_TO_ARCGIS(G3E_FID);

-- Create a stored procedure that builds MODLOG_GTECH_TO_ARCGIS table
Create Or Replace Procedure APP_ARCGIS_FM.P_MODLOG_ARCGIS (modNumber In Number Default 0) As
    strCurrAGSTable Varchar2(100);
    strCurrGTechView Varchar2(100);
    strSQL Varchar2(8000);
    strTemp Varchar2(8000);
    intCount int;
    intField int := 0;
    intFNO int;
    modType Varchar2(10);
    intModType int;
    strCurrAGSView Varchar2(100);
    currModNumber Number;
    
Begin
    
    Delete From MODLOG_GTECH_TO_ARCGIS Where STATUS='PROCESSED';
    -- Select records from MODIFICATIONLOG (view that links to MODIFICATIONLOG@GIS.WORLD)
    for i in (Select Distinct G3E_FID From AE.MODIFICATIONLOG Where LTT_ID=0 And MODIFICATIONNUMBER > modNumber Order By G3E_FID)
    Loop
      -- get newly created FIDs 
      -- TYPE 3 is INSERT. Except AG_N all features have mandatory common_n attributes
      Select Count(*) into intCount From AE.MODIFICATIONLOG
        Where LTT_ID=0 And MODIFICATIONNUMBER > modNumber And G3E_FID=i.G3E_FID And TYPE=3 And TABLENAME in ('COMMON_N', 'AG_N');
      If (intCount > 0) Then
        -- record is inserted
        -- Check if this is deleted later.
        Select Floor(Max(MODIFICATIONNUMBER)), Max(G3E_FNO) Into currModNumber, intFNO From AE.MODIFICATIONLOG
          Where LTT_ID=0 And MODIFICATIONNUMBER > modNumber And G3E_FID=i.G3E_FID And TYPE=3 And TABLENAME in ('COMMON_N', 'AG_N');
        Select Count(*) into intCount From AE.MODIFICATIONLOG Where LTT_ID=0 And MODIFICATIONNUMBER > currModNumber And G3E_FID=i.G3E_FID And TYPE=1 And TABLENAME in ('COMMON_N', 'AG_N');
        If (intCount = 0) Then
          -- not deleted later
          modType := 'INSERT';
          For k in (Select Distinct MAPPING.GTECH_FEATURE_VIEW GTECH_VIEW, MODLOG.G3E_FID G3E_FID, Floor(Max(MODIFICATIONNUMBER)) MODNUMBER
                    From GTECH_TO_ARCGIS_ATTR MAPPING, AE.MODIFICATIONLOG MODLOG
                    Where MODLOG.LTT_ID=0 And MODLOG.MODIFICATIONNUMBER >= currModNumber And MODLOG.G3E_FID=i.G3E_FID And
                    ((MAPPING.GTECH_COMPONENT_TABLE=MODLOG.TABLENAME and MODLOG.G3E_FNO Is Null And MAPPING.ARCGIS_ATTRIBUTE_NAME Is Not Null)
                     Or (MAPPING.GTECH_COMPONENT_TABLE=MODLOG.TABLENAME and MAPPING.GTECH_FEATURE_ID=intFNO))
                     Group By MAPPING.GTECH_FEATURE_VIEW, MODLOG.G3E_FID)
          Loop
            strCurrAGSTable := k.GTECH_VIEW;
            
            Delete From  MODLOG_GTECH_TO_ARCGIS Where G3E_FID=i.G3E_FID;

            Insert Into MODLOG_GTECH_TO_ARCGIS (G3E_FID, GTECH_FEATURE, MOD_NUMBER, MOD_TYPE, STATUS)
              Values (i.G3E_FID, strCurrAGSTable, k.MODNUMBER, modType, 'PENDING');
            -- GTECH_TO_ARCGIS mapping table has entries for VW_* feature classes, so need to add them explicitly
            --P_MODLOG_VIEWENTRIES(strCurrAGSTable, i.G3E_FID, modType, k.G3E_CID);

          End Loop;
        End If;
        
      Else
        Select Count(*) into intCount From AE.MODIFICATIONLOG Where LTT_ID=0 And MODIFICATIONNUMBER > modNumber And G3E_FID=i.G3E_FID And TYPE=1 And TABLENAME in ('COMMON_N', 'AG_N');
        If (intCount > 0) Then
          -- this is deleted FID, get FNO
          Select Floor(Max(MODIFICATIONNUMBER)), Max(G3E_FNO) Into currModNumber, intFNO From AE.MODIFICATIONLOG Where LTT_ID=0 And MODIFICATIONNUMBER > modNumber And G3E_FID=i.G3E_FID And TYPE=1 And TABLENAME in ('COMMON_N', 'AG_N');
          modType := 'DELETE';
          For k in (Select Distinct MAPPING.GTECH_FEATURE_VIEW GTECH_VIEW, MODLOG.G3E_FID G3E_FID, Floor(Max(MODIFICATIONNUMBER)) MODNUMBER
                    From GTECH_TO_ARCGIS_ATTR MAPPING, AE.MODIFICATIONLOG MODLOG
                    Where MODLOG.LTT_ID=0 And MODLOG.MODIFICATIONNUMBER >= currModNumber And MODLOG.G3E_FID=i.G3E_FID And
                    ((MAPPING.GTECH_COMPONENT_TABLE=MODLOG.TABLENAME and MODLOG.G3E_FNO Is Null And MAPPING.ARCGIS_ATTRIBUTE_NAME Is Not Null)
                     Or (MAPPING.GTECH_COMPONENT_TABLE=MODLOG.TABLENAME And MAPPING.GTECH_FEATURE_ID=intFNO))
                     Group By MAPPING.GTECH_FEATURE_VIEW, MODLOG.G3E_FID)
          Loop
            strCurrAGSTable := k.GTECH_VIEW;
            
            Delete From  MODLOG_GTECH_TO_ARCGIS Where G3E_FID=i.G3E_FID;

            Insert Into MODLOG_GTECH_TO_ARCGIS (G3E_FID, GTECH_FEATURE, MOD_NUMBER, MOD_TYPE, STATUS)
              Values (i.G3E_FID, strCurrAGSTable, k.MODNUMBER, modType, 'PENDING');   

            -- GTECH_TO_ARCGIS mapping table has entries for VW_* feature classes, so need to add them explicitly
            --P_MODLOG_VIEWENTRIES(strCurrAGSTable, i.G3E_FID, modType, k.G3E_CID);
          
          End Loop;
          
        Else
          Select Count(*) into intCount From AE.MODIFICATIONLOG Where LTT_ID=0 And MODIFICATIONNUMBER > modNumber And G3E_FID=i.G3E_FID And TYPE=2;
          If (intCount > 0) Then
            Select Floor(Max(MODIFICATIONNUMBER)), Max(G3E_FNO) Into currModNumber, intFNO From AE.MODIFICATIONLOG Where LTT_ID=0 And MODIFICATIONNUMBER > modNumber And G3E_FID=i.G3E_FID And TYPE=2;
            modType := 'UPDATE';
            For k in (Select Distinct MAPPING.GTECH_FEATURE_VIEW GTECH_VIEW, MODLOG.G3E_FID G3E_FID, Floor(Max(MODIFICATIONNUMBER)) MODNUMBER
                      From GTECH_TO_ARCGIS_ATTR MAPPING, AE.MODIFICATIONLOG MODLOG
                      Where MODLOG.LTT_ID=0 And MODLOG.MODIFICATIONNUMBER >= currModNumber And MODLOG.G3E_FID=i.G3E_FID And
                      ((MAPPING.GTECH_COMPONENT_TABLE=MODLOG.TABLENAME and MODLOG.G3E_FNO Is Null And MAPPING.ARCGIS_ATTRIBUTE_NAME Is Not Null)
                       Or (MAPPING.GTECH_COMPONENT_TABLE=MODLOG.TABLENAME And MAPPING.GTECH_FEATURE_ID=intFNO))
                       Group By MAPPING.GTECH_FEATURE_VIEW, MODLOG.G3E_FID)
            Loop
              strCurrAGSTable := k.GTECH_VIEW;
              Delete From  MODLOG_GTECH_TO_ARCGIS Where G3E_FID=i.G3E_FID;
              Insert Into MODLOG_GTECH_TO_ARCGIS (G3E_FID, GTECH_FEATURE, MOD_NUMBER, MOD_TYPE, STATUS)
                                  Values (i.G3E_FID, strCurrAGSTable, k.MODNUMBER, modType, 'PENDING');   
              
            
            End Loop;
          End If;
        
        End If; -- Delete or Update
      End If; -- Insert
    End Loop; -- Loop for G3E_FID
    Commit;
    
    -- At the end remove duplicates from the MODLOG_GTECH_TO_ARCGIS table
    
    Execute Immediate ' DELETE FROM MODLOG_GTECH_TO_ARCGIS '||
                      ' WHERE ROWID IN ( '||
                      ' SELECT rid '||
                      ' FROM (SELECT ROWID rid, '||
                      '       ROW_NUMBER () OVER (PARTITION BY G3E_FID, GTECH_FEATURE, MOD_NUMBER, MOD_TYPE, STATUS, ERROR_DESCRIP ORDER BY ROWID) rn '||
                      '       FROM MODLOG_GTECH_TO_ARCGIS) '||
                      '       WHERE rn <> 1)'; 
    
    Commit;

End P_MODLOG_ARCGIS;
/

Spool Off;
