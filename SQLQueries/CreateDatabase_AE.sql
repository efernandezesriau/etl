alter session set "_ORACLE_SCRIPT"=true; 

Drop User AE CASCADE;

Create Tablespace TBLSP_GTECH Datafile 'E:\Oracle\oradata\orcl\TasNet\TASNET_GTECH1.DBF' Size 1000M Autoextend On Next 10M MaxSize 16000M;

Create User AE identified by AE Default Tablespace TBLSP_GTECH Temporary Tablespace TEMP Quota Unlimited On TBLSP_GTECH Profile Default Account Unlock;

Grant Connect, Resource to AE;

Create User AEELECTRIC identified by AE Default Tablespace TBLSP_GTECH Temporary Tablespace TEMP Quota Unlimited On TBLSP_GTECH Profile Default Account Unlock;

Grant Connect, Resource to AEELECTRIC;

Create Role Marketing;

Create Role Designer;

Create Role Supervisor;
  
Create Role Finance;

Create Role OMSPUBLISH;

Create Role FULLPUBLISH;

Create Role Administrator;

CREATE OR REPLACE DIRECTORY GCOMM_DUMP_DIR AS 'E:\Projects\TasNetworks\DATA';

Grant Read,Write on Directory GCOMM_DUMP_DIR to AE;