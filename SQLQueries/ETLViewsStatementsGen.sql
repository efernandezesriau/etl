-- Name: ETLViewsStatementsGen.sql
-- Script to construct view creation statements for schema
-- Script must be run as owner of the G/Tech database
-- Vish Apte 30-Jun-2017 Initial Version
-- Script must be run as APP_ARCGIS_FM

Set Serveroutput On;


-- Function to convert voltage to a numeric value
Create Or Replace Function FN_VOLTAGE_TO_NUMBER(textVoltage Varchar2) Return Numeric As
  voltage Varchar2(1000);
  numericVoltage Number(10,3) := Null;
Begin
  voltage := Ltrim(Rtrim(textVoltage));
   If (length(voltage) > 0) Then
    voltage := Upper(voltage);
    numericVoltage := to_number(regexp_substr(voltage,'^[0-9]+(\.[0-9]{1,2})?' ));
    If (numericVoltage Is Not Null) Then
      If (regexp_count(voltage,'KV', 1, 'i') > 0) Then
        numericVoltage := numericVoltage * 1000.0;
      ElsIf (regexp_count(voltage,'MV', 1, 'i') > 0) Then
        numericVoltage := numericVoltage * 1000000.0;
      End If;
    End If;
  End If;
  Return numericVoltage;
End;
/

-- Create a function to extract angle in deg from the oriented point in SDO
-- This angle is then used to set SymbolRotation field in ArcGIS
Create Or Replace Function FN_POINT_ANGLE (
  origGeom MDSYS.SDO_GEOMETRY)
Return Number As
  angleDeg    number := 0;
  numVertices number;
  dim         number;
  xVector     number;
  yVector     number;
Begin
  If (origGeom Is Not Null) Then
    dim := origGeom.GET_DIMS();
    numVertices := MDSYS.SDO_UTIL.GETNUMVERTICES(origGeom);
    If (numVertices = 2 And origGeom.SDO_POINT Is Null And (MOD(origGeom.SDO_GTYPE,1000) = 1) ) Then
      -- it is oriented point geometry
      xVector := origGeom.SDO_ORDINATES(origGeom.SDO_ELEM_INFO(4));
      yVector := origGeom.SDO_ORDINATES(origGeom.SDO_ELEM_INFO(4)+1);
      angleDeg := atan2(yVector, xVector) * 180 / atan2(0, -1);
      angleDeg := Round(angleDeg, 3);
    End If;
  End If;
  return angleDeg;
Exception
  When Others Then Return angleDeg;
End;
/

Drop Table GTECH_VIEWS;

Drop Table GTECH_TO_ARCGIS_VIEWS;

Create Table GTECH_TO_ARCGIS_VIEWS (FEATURE_MAP_ID Varchar2(5), 
                                    VIEW_NAME Varchar2(100), 
                                    VIEW_SOURCE Varchar2(4000), 
                                    VIEW_STATUS Number(1));


-- Create views
Declare
  strSQL Varchar2(4000);
  Type TABLE_LIST is TABLE Of Varchar2(100);
  arrTables TABLE_LIST := TABLE_LIST();
  intColCount Number;  
  intCounter Number;
  intCounter2 Number;
  boolPrimary boolean := False;
  strPrimaryTable Varchar2(30);
  strPredicateWord Varchar2(30) := 'Where';
  lttClause boolean := True;
  srcSchemaPrefix Varchar2(30) := 'AE.';
  srcViewPrefix Varchar2(30) := 'APP_ARCGIS_FM.';
Begin
  Delete From APP_ARCGIS_FM.GTECH_TO_ARCGIS_VIEWS;
  For i in (Select distinct ATTRMAP.FEATURE_MAP_ID, FEATMAP.GTECH_FEATURE_ID, ATTRMAP.GTECH_FEATURE_VIEW, FEATMAP.GTECH_DATA_FILTER, ATTRMAP.ARCGIS_FEATURE, FEATMAP.ARCGIS_SUBTYPE_VALUE 
             From APP_ARCGIS_FM.GTECH_TO_ARCGIS_ATTR ATTRMAP, APP_ARCGIS_FM.GTECH_TO_ARCGIS_FEATURES FEATMAP
             Where ATTRMAP.FEATURE_MAP_ID=FEATMAP.RECORD_ID And ATTRMAP.ARCGIS_ATTRIBUTE_NAME Is Not Null Order By ATTRMAP.FEATURE_MAP_ID)
  Loop
    If (Upper(SUBSTR(i.ARCGIS_FEATURE, 1, 3)) = 'OMS') Then
        lttClause := False;
    Else
        lttClause := True;
    End If;
    intColCount := 0;
    strPredicateWord := CHR(13) || CHR(10)|| '  Where';
    strSQL := 'Create Or Replace Force View '||NVL(srcViewPrefix,'')||i.GTECH_FEATURE_VIEW || CHR(13) || CHR(10) || ' As '|| CHR(13) || CHR(10) ||'  Select ';
    If (i.ARCGIS_SUBTYPE_VALUE Is Not Null) Then
      If (i.GTECH_FEATURE_VIEW = 'VW_POLE_PT_4') Then
        -- special case for pole, material drives subtype
        strSQL := strSQL || '    Decode(InStr(Upper(POLE_N.POLE_MATERIAL), ''WOOD''), 0, 2, 3) SubtypeCD ';
      Else
        strSQL := strSQL || '    '|| i.ARCGIS_SUBTYPE_VALUE || ' SubtypeCD ';
      End If;
      intColCount := intColCount + 1;
    End If;
    arrTables := TABLE_LIST();
    
    boolPrimary := False;
    for j in (Select Distinct ATTRMAPPING.GTECH_COMPONENT_TABLE, FEATATTR.PRIMARY_CNO, FEATATTR.G3E_CNO  From 
               APP_ARCGIS_FM.GTECH_TO_ARCGIS_ATTR ATTRMAPPING, AE.VW_FEAT_ATTR FEATATTR  Where ATTRMAPPING.GTECH_FEATURE_ID=FEATATTR.G3E_FNO And ATTRMAPPING.GTECH_COMPONENT_TABLE= FEATATTR.COMPONENT_TABLE And FEATURE_MAP_ID=i.FEATURE_MAP_ID And GTECH_FEATURE_VIEW=i.GTECH_FEATURE_VIEW And ARCGIS_ATTRIBUTE_NAME Is Not Null)
    Loop
      If (j.PRIMARY_CNO = j.G3E_CNO) Then
        boolPrimary := True;
      Else 
        boolPrimary := False;
      End If;
      arrTables.Extend();
      arrTables(arrTables.Count) :=  Upper(j.GTECH_COMPONENT_TABLE);
      for k in (Select Distinct Upper(GTECH_FIELD_NAME) GTECH_FIELD_NAME, Upper(GTECH_FIELD_TYPE) GTECH_FIELD_TYPE, Upper(ARCGIS_ATTRIBUTE_NAME) ARCGIS_ATTRIBUTE_NAME From APP_ARCGIS_FM.GTECH_TO_ARCGIS_ATTR 
               Where FEATURE_MAP_ID=i.FEATURE_MAP_ID And GTECH_FEATURE_VIEW=i.GTECH_FEATURE_VIEW And GTECH_COMPONENT_TABLE=j.GTECH_COMPONENT_TABLE And ARCGIS_ATTRIBUTE_NAME Is Not Null)
      Loop
        If (intColCount > 0) Then
          strSQL := strSQL || ', '|| CHR(13) || CHR(10);
        End If;
        strSQL := strSQL || '    ';
        If (k.GTECH_FIELD_NAME = 'SHAPE' And k.GTECH_FIELD_TYPE = 'SDO_GEOMETRY') Then
          -- GTech geometry field, field name in table is G3E_GEOMETRY, field name in mapping is SHAPE
          strSQL := strSQL || j.GTECH_COMPONENT_TABLE ||'.G3E_GEOMETRY SHAPE';
        ElsIf (k.GTECH_FIELD_NAME = 'LENGTH' And j.GTECH_COMPONENT_TABLE = 'CONNECTIVITY_N') Then
          -- Alias length field in connectivity table
          strSQL := strSQL || j.GTECH_COMPONENT_TABLE ||'.'||k.GTECH_FIELD_NAME || ' MeasuredLength';
        ElsIf (k.GTECH_FIELD_NAME = 'ALIGNMENT' And k.ARCGIS_ATTRIBUTE_NAME = 'SYMBOLROTATION') Then
          -- Convert alignment to SDO_ORIENTATION if a point angle
          strSQL := strSQL || 'FN_POINT_ANGLE(' || j.GTECH_COMPONENT_TABLE || '.G3E_GEOMETRY) ALIGNMENT'; 
        ElsIf (k.GTECH_FIELD_NAME = 'STATE' And j.GTECH_COMPONENT_TABLE Like '%NMI_N') Then
          -- Convert alignment to SDO_ORIENTATION if a point angle
          strSQL := strSQL || j.GTECH_COMPONENT_TABLE ||'.'||k.GTECH_FIELD_NAME || ' StateAbbrev';
        ElsIf (k.GTECH_FIELD_NAME in ('VOLTAGE','BUS_VOLTAGE','PRIM_TAP_VOLTAGE','SEC_TAP_VOLTAGE') 
                 And (k.ARCGIS_ATTRIBUTE_NAME in ('NOMINALVOLTAGE','OPERATINGVOLTAGE','LOWSIDEVOLTAGE','PRIMARYTAPVOLTAGE','SECONDARYTAPVOLTAGE')) 
                 And k.GTECH_FIELD_TYPE = 'TEXT') Then
          -- Convert textual voltage to numeric value e.g. 11kV to 11000
          strSQL := strSQL || 'FN_VOLTAGE_TO_NUMBER(' || j.GTECH_COMPONENT_TABLE || '.'|| k.GTECH_FIELD_NAME || ') '|| k.GTECH_FIELD_NAME;   
        Else
          strSQL := strSQL || j.GTECH_COMPONENT_TABLE ||'.'||k.GTECH_FIELD_NAME;
        End If;
        
        intColCount := intColCount + 1;          
      End Loop;
      --If (intPrimary > 0) Then
      --  If (intColCount > 0) Then
      --    strSQL := strSQL || ', ';
      --  End If;  
      --  strSQL := strSQL || j.GTECH_COMPONENT_TABLE ||'.'||'G3E_GEOMETRY ';
      --  intColCount := intColCount + 1;
      --End If;            
    End Loop;
    strSQL := strSQL || CHR(13) || CHR(10) || ' From ';
    For intCounter in 1..arrTables.COUNT
    Loop
      If (intCounter > 1) Then
        strSQL := strSQL || ', '|| CHR(13) || CHR(10)||'    ';
      End If;
      If (Upper(SUBSTR(i.ARCGIS_FEATURE, 1, 3)) = 'OMS') Then
          strSQL := strSQL || 'OMSPUBLISH.'|| arrTables(intCounter) ||' '|| arrTables(intCounter);
      Else
         strSQL := strSQL || NVL(srcSchemaPrefix,'') || 'B$'|| arrTables(intCounter) ||' '|| arrTables(intCounter);
      End If;   
    End Loop;
    -- Add G3E_FNO clause
    strSQL := strSQL || ' '|| strPredicateWord ||' '|| arrTables(1)||'.G3E_FNO='||i.GTECH_FEATURE_ID;
    strPredicateWord := '  And';
    If (lttClause = True) Then
      strSQL := strSQL || CHR(13) || CHR(10) || strPredicateWord ||' NVL('|| arrTables(1)||'.LTT_ID,0)=0 ';
    End If;   
    If (arrTables.COUNT > 1) Then
        For intCounter in 2..(arrTables.COUNT)
        Loop
          If (lttClause = True) Then
            strSQL := strSQL ||' '|| CHR(13) || CHR(10) || strPredicateWord ||' NVL('|| arrTables(intCounter)||'.LTT_ID,0)=0 ';
          End If;  
          strPredicateWord := '  And';  
          strSQL := strSQL ||' '|| CHR(13) || CHR(10) || strPredicateWord ||' '|| arrTables(intCounter)||'.G3E_FID='||arrTables(1)||'.G3E_FID ';
        End Loop;
        -- G3E_CID condition on SDOGEOM columns
        For intCounter in 1..(arrTables.COUNT)
        Loop
          For intCounter2 in 1..(arrTables.COUNT)
          Loop
            If (arrTables(intCounter) = (arrTables(intCounter2)||'_SDOGEOM')) Then 
              -- Geometry table, match by CID as well
              strSQL := strSQL ||' '|| CHR(13) || CHR(10) || strPredicateWord ||' '|| CHR(13) || CHR(10) || arrTables(intCounter)||'.G3E_CID='||arrTables(intCounter2)||'.G3E_CID ';
              strPredicateWord := CHR(13) || CHR(10)||'  And';  
            End If;
          End Loop;  
        End Loop;
    End If;
    
    --DBMS_OUTPUT.PUT_LINE('Filter = '||i.GTECH_DATA_FILTER); 
    
    If (i.GTECH_DATA_FILTER Is Null Or i.GTECH_DATA_FILTER = '') Then
      Null;
    Else
      strSQL := strSQL || CHR(13) || CHR(10) ||' And ' || i.GTECH_DATA_FILTER;
    End If;
   
    --DBMS_OUTPUT.PUT_LINE(strSQL);
    
    Begin
      Execute Immediate strSQL;
      --DBMS_OUTPUT.PUT_LINE('Success:'||strSQL);
      Insert Into GTECH_TO_ARCGIS_VIEWS (FEATURE_MAP_ID,VIEW_NAME, VIEW_SOURCE, VIEW_STATUS) Values (i.FEATURE_MAP_ID, i.GTECH_FEATURE_VIEW, strSQL, 1);
    Exception 
      When Others Then
        Insert Into GTECH_TO_ARCGIS_VIEWS (FEATURE_MAP_ID,VIEW_NAME, VIEW_SOURCE, VIEW_STATUS) Values (i.FEATURE_MAP_ID, i.GTECH_FEATURE_VIEW, strSQL, 0);
        --DBMS_OUTPUT.PUT_LINE('Error:'||strSQL);
    End;
            
  End Loop;
  Commit;
End;
/

--Select RTrim(LTrim(VIEW_SOURCE||';')) From GTECH_TO_ARCGIS_VIEWS Where VIEW_STATUS=0; 

--Select RTrim(LTrim(VIEW_SOURCE||';')) From GTECH_TO_ARCGIS_VIEWS Where VIEW_STATUS=1; 

Select RTrim(LTrim(VIEW_SOURCE||';')) From GTECH_TO_ARCGIS_VIEWS Order By TO_NUMBER(FEATURE_MAP_ID)