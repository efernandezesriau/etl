alter session set "_ORACLE_SCRIPT"=true; 

Create Tablespace TBLSP_TASNET Datafile 'B:\app\orcl\oradata\TasNet\TASNET01.DBF' Size 1000M Autoextend On Next 10M MaxSize 16000M;

Create User AE identified by AE Default Tablespace TBLSP_TASNET Temporary Tablespace TEMP Quota Unlimited On TBLSP_TASNET Profile Default Account Unlock;

Drop Role OMSPUBLISH;

Create User OMSPUBLISH identified by AE Default Tablespace TBLSP_TASNET Temporary Tablespace TEMP Quota Unlimited On TBLSP_TASNET Profile Default Account Unlock;

Drop User APP_ARCGIS_FM Cascade;

CREATE USER APP_ARCGIS_FM IDENTIFIED BY AE Default Tablespace TBLSP_TASNET Temporary Tablespace TEMP Quota Unlimited On TBLSP_TASNET Profile Default Account Unlock;

Grant Connect, Resource to AE, OMSPUBLISH, APP_ARCGIS_FM;

Grant Select Any Table to APP_ARCGIS_FM;

Grant Create View to APP_ARCGIS_FM;

Create Role Marketing;

Create Role Designer;

Create Role Supervisor;
  
Create Role Finance;

--Create Role OMSPUBLISH;

Create Role FULLPUBLISH;

Create Role Administrator;

Create or Replace Directory GTECH_DUMP_DIR as 'B:\Datasets\TasNetworks\GtechData';

GRANT read, write ON DIRECTORY GTECH_DUMP_DIR TO AE;




